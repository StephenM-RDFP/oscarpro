/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.pageUtil;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;

import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

/**
 * retrieves info to display Disease entries for demographic
 */
public class EctDisplayParcsAction extends EctDisplayAction {

	private static Logger logger = MiscUtils.getLogger();
	
	
	private String cmd = "parcs";

	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		
		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.parcs"));
		Dao.setRightHeadingID(cmd);
		
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_parcs", "r", null)) {
			return true; // PARCS link won't show up on new CME screen.
		} else {

			// set lefthand module heading and link
			String winName = "PARCS" + bean.demographicNo;
			String url = "popupPage(580,900,'" + winName + "','" + request.getContextPath() + "/parcs/ExternalController/LimitedExternalControllerDemo.jsp?demographicNo=" + bean.demographicNo + "')";
			Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.parcs"));
			Dao.setLeftURL(url);

			// set righthand link to same as left so we have visual consistency with other modules
			url += "; return false;";
			Dao.setRightURL(url);
			Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action	
		}
		return true;
	}

	public String getCmd() {
		return cmd;
	}
}
