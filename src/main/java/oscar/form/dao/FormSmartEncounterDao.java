/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.FormStringValue;

import java.util.HashMap;

@Repository
public class FormSmartEncounterDao extends AbstractDao<FormSmartEncounter> {

    public FormSmartEncounterDao() {
        super(FormSmartEncounter.class);
    }

    public FormSmartEncounter find(Integer id) {
        FormSmartEncounter form = super.find(id);
        if (form != null) {
            FormStringValueDao formStringValueDao = SpringUtils.getBean(FormStringValueDao.class);
            HashMap<String, FormStringValue> formValues = formStringValueDao.findAllForForm(form);
            for (String key : formValues.keySet()) {
                if (key.startsWith("image_")) {
                    form.getPlaceholderImageMap().put(key, formValues.get(key));
                } else {
                    form.getPlaceholderValueMap().put(key, formValues.get(key));
                }
            }
        }
        return super.find(id);
    }

    @Override
    public FormSmartEncounter saveEntity(FormSmartEncounter form) {
        // save FormSmartEncounter object
        form = super.saveEntity(form);
        // get string fields and persist them:
        for (FormStringValue value : form.getStringValueMap().values()) {
            value.getId().setFormId(form.getId());
            value.getId().setFormName(FormSmartEncounter.FORM_TABLE);
        }
        FormStringValueDao formStringValueDao = SpringUtils.getBean(FormStringValueDao.class);
        for (FormStringValue value : form.getStringValueMap().values()) {
            formStringValueDao.saveEntity(value);
        }
        
        return form;
    }
}
