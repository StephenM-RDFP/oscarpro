/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form;

import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import oscar.form.dao.FormSmartEncounterDao;
import oscar.form.dao.SmartEncounterTemplateImagesDao;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.FormStringValue;
import oscar.form.model.SmartEncounterTemplateImage;
import oscar.form.model.SmartEncounterTemplatePlaceholder;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class FrmSmartEncounterRecord extends FrmRecord {
    private final Logger logger = LoggerFactory.getLogger(FrmSmartEncounterRecord.class);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private FormSmartEncounterDao formSmartEncounterDao = SpringUtils.getBean(FormSmartEncounterDao.class);
    private SmartEncounterTemplateImagesDao smartEncounterTemplateImagesDao = SpringUtils.getBean(SmartEncounterTemplateImagesDao.class);

    private String graphType;
    
    public Properties getFormRecord(LoggedInInfo loggedInInfo, int demographicNo, int existingID) {
        Properties props = new Properties();
        props.setProperty("demographicNo", String.valueOf(demographicNo));

        if (existingID <= 0) { // new form
            props.setProperty("formCreated", dateFormat.format(new Date()));
        } else {
            FormSmartEncounter existingForm = formSmartEncounterDao.find(existingID);
            props.setProperty("formId", String.valueOf(existingForm.getId()));
            props.setProperty("providerNo", existingForm.getProviderNo());
            props.setProperty("formCreated", dateFormat.format(existingForm.getFormCreated()));
            props.setProperty("formEdited", dateFormat.format(existingForm.getFormEdited()));
            props.setProperty("documentName", existingForm.getDocumentName());
            props.setProperty("deltaText", existingForm.getDeltaText());
            props.setProperty("plainText", existingForm.getPlainText());
            props.setProperty("templateUsed", String.valueOf(existingForm.getTemplateUsed()));
            JSONArray resultArray = new JSONArray();
            for (FormStringValue formStringValue : existingForm.getPlaceholderValueMap().values()) {
                JSONObject result = new JSONObject();
                result.put("marker", formStringValue.getId().getFieldName());
                result.put("value", Encode.forJavaScriptBlock(formStringValue.getValue()));
                result.put("isBlock", String.valueOf(SmartEncounterTemplatePlaceholder.isBlockPlaceholder(formStringValue.getId().getFieldName())));
                resultArray.add(result);
            }
            props.setProperty("resolvedPlaceholders", resultArray.toString());

            for (Map.Entry<String, FormStringValue> entry : existingForm.getPlaceholderImageMap().entrySet()) {
                props.setProperty(entry.getKey(), entry.getValue().getValue());
            }
            if (existingForm.getTemplateUsed() != null) {
                List<SmartEncounterTemplateImage> templateImages = smartEncounterTemplateImagesDao.findAllForTemplate(existingForm.getTemplateUsed());
                for (SmartEncounterTemplateImage templateImage : templateImages) {
                    props.setProperty(templateImage.getName(), templateImage.getValue());
                }
            }
        }
        
        return props;
    }

    public int saveFormRecord(Properties props) throws SQLException {
        FrmRecordHelp frmRec = new FrmRecordHelp();
        frmRec.setDateFormat("dd/MM/yyyy");
        

        return 0; // newForm.getId();
    }

    @Override
    public String findActionValue(String submit) throws SQLException {
        return ((new FrmRecordHelp()).findActionValue(submit));
    }
    
    @Override
    public String createActionURL(String where, String action, String demoId, String formId) throws SQLException {
        return ((new FrmRecordHelp()).createActionURL(where, action, demoId, formId));
    }
}
