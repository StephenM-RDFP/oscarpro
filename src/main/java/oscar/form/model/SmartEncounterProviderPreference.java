/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.model;

import org.oscarehr.common.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "smart_encounter_provider_preference")
public class SmartEncounterProviderPreference extends AbstractModel<Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "provider_no")
    private String providerNo;
    @Column(name = "header_delta_text", columnDefinition = "TEXT")
    private String headerDeltaText;
    @Column(name = "header_html_text", columnDefinition = "TEXT")
    private String headerHtmlText;
    @Column(name = "footer_delta_text", columnDefinition = "TEXT")
    private String footerDeltaText;
    @Column(name = "footer_html_text", columnDefinition = "TEXT")
    private String footerHtmlText;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public String getHeaderDeltaText() {
        return headerDeltaText;
    }

    public void setHeaderDeltaText(String headerDeltaText) {
        this.headerDeltaText = headerDeltaText;
    }

    public String getHeaderHtmlText() {
        return headerHtmlText;
    }

    public void setHeaderHtmlText(String headerHtmlText) {
        this.headerHtmlText = headerHtmlText;
    }

    public String getFooterDeltaText() {
        return footerDeltaText;
    }

    public void setFooterDeltaText(String footerDeltaText) {
        this.footerDeltaText = footerDeltaText;
    }

    public String getFooterHtmlText() {
        return footerHtmlText;
    }

    public void setFooterHtmlText(String footerHtmlText) {
        this.footerHtmlText = footerHtmlText;
    }
}