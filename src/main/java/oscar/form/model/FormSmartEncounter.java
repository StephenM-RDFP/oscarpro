/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.model;

import org.apache.commons.io.FileUtils;
import org.oscarehr.common.model.AbstractModel;
import oscar.OscarProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "form_smart_encounter")
public class FormSmartEncounter extends AbstractModel<Integer> implements Serializable, StringValueForm {
    public static final String FORM_TABLE = "formSmartEncounter";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;
    @Column(name="demographic_no")
    private Integer demographicNo;
    @Column(name="provider_no")
    private String providerNo;
    @Column(name="appointment_no")
    private Integer appointmentNo;
    @Column(name="document_name")
    private String documentName;
    @Temporal(TemporalType.DATE)
    @Column(name="formCreated")
    private Date formCreated;
    @Column(name="formEdited")
    private Timestamp formEdited;
    /**
     * Canonical note written and exported from quilljs as a delta, with placeholder elements included 
     */
    @Column(name="delta_text", columnDefinition = "TEXT")
    private String deltaText;
    /**
     * Mote exported from quilljs as a html, with placeholder ( ${placeholderName} ) text included 
     */
    @Column(name="html_text", columnDefinition = "TEXT")
    private String htmlText;
    /**
     * Plain, no markup note exported from quilljs as a html, with placeholder ( ${placeholderName} ) text included 
     */
    @Column(name="plain_text", columnDefinition = "TEXT")
    private String plainText;
    @Column(name="template_used")
    private Integer templateUsed;

    @Transient
    private Map<String, FormStringValue> placeholderValueMap = new HashMap<String, FormStringValue>();
    @Transient
    private Map<String, FormStringValue> placeholderImageMap = new HashMap<String, FormStringValue>();

    public FormSmartEncounter() { }

    @Override
    public String getFormTable() {
        return FORM_TABLE;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public Map<String, FormStringValue> getStringValueMap() { 
        HashMap<String, FormStringValue> map = new HashMap<String, FormStringValue>();
        for (Map.Entry<String, FormStringValue> entry : placeholderValueMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
        for (Map.Entry<String, FormStringValue> entry : placeholderImageMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    @Override
    public void setStringValueMap(Map<String, FormStringValue> stringValueMap) {
        this.placeholderValueMap = stringValueMap;
    }

    public Map<String, FormStringValue> getPlaceholderValueMap() {
        return placeholderValueMap;
    }

    public void setPlaceholderValueMap(Map<String, FormStringValue> placeholderValueMap) {
        this.placeholderValueMap = placeholderValueMap;
    }

    public Map<String, FormStringValue> getPlaceholderImageMap() {
        return placeholderImageMap;
    }

    public void setPlaceholderImageMap(Map<String, FormStringValue> placeholderImageMap) {
        this.placeholderImageMap = placeholderImageMap;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Integer appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public Date getFormCreated() {
        return formCreated;
    }

    public void setFormCreated(Date formCreated) {
        this.formCreated = formCreated;
    }

    public Timestamp getFormEdited() {
        return formEdited;
    }

    public void setFormEdited(Timestamp formEdited) {
        this.formEdited = formEdited;
    }

    public String getDeltaText() {
        return deltaText;
    }

    public void setDeltaText(String deltaText) {
        this.deltaText = deltaText;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
    
    public String getHtmlTextWithValuesFilled() {
        String html = getHtmlText();
        
        // replace tab characters to facilitate inline display
        html = html.replaceAll("\t", "<span class=\"tab\">&nbsp;</span>");
        
        // replace string values 
        for (Map.Entry<String, FormStringValue> stringFormStringValueEntry : getPlaceholderValueMap().entrySet()) {
            String key = stringFormStringValueEntry.getKey();
            String value = stringFormStringValueEntry.getValue().getValue();
            if (SmartEncounterTemplatePlaceholder.isBlockPlaceholder(key)) {
                value = value.replaceAll("\n", "<br/>");
            }
            html = html.replaceAll("\\$\\{" + key + "}", value);
        }

        String imageDirectory = OscarProperties.getInstance().getProperty("form_smart_encounter_images_path");
        
        // replace image 
        for (Map.Entry<String, FormStringValue> imageEntry : getPlaceholderImageMap().entrySet()) {
            String key = imageEntry.getKey();
            String value = imageEntry.getValue().getValue();
            File imageFile = new File(imageDirectory + File.separator + value);
            if (imageFile.exists()) {
                try {
                    byte[] fileData = FileUtils.readFileToByteArray(imageFile);
                    String base64Image = DatatypeConverter.printBase64Binary(fileData);
                    html = html.replaceAll("\\$\\{" + key + "}", "<img src=\"data:image/png;base64," + base64Image + "\"/>");
                } catch (IOException e) {
                    e.printStackTrace(); // todo: this
                }
            }
        }
        
        return html;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Integer getTemplateUsed() {
        return templateUsed;
    }

    public void setTemplateUsed(Integer templateUsed) {
        this.templateUsed = templateUsed;
    }
}
