/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import oscar.form.model.FormSmartEncounter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SmartEncounterActionForm extends ActionForm {
    private Integer formId;
    private Integer demographicNo;
    private String providerNo;
    private Integer appointmentNo;
    private String documentName;
    private Date formCreated;
    private Timestamp formEdited;
    private String deltaText;
    private String htmlText;
    private String plainText;
    private Integer templateUsed;
    private String fieldsToPullListString;
    private String[] images = new String[0];

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Integer appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Date getFormCreated() {
        return formCreated;
    }

    public void setFormCreated(Date formCreated) {
        this.formCreated = formCreated;
    }

    public Timestamp getFormEdited() {
        return formEdited;
    }

    public void setFormEdited(Timestamp formEdited) {
        this.formEdited = formEdited;
    }

    public String getDeltaText() {
        return deltaText;
    }

    public void setDeltaText(String deltaText) {
        this.deltaText = deltaText;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Integer getTemplateUsed() {
        return templateUsed;
    }

    public void setTemplateUsed(Integer templateUsed) {
        this.templateUsed = templateUsed;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getFieldsToPullListString() {
        return fieldsToPullListString;
    }
    public List<String> getFieldsToPullList() {
        List<String> fieldsToPullList = new ArrayList<>();
        if (!StringUtils.isEmpty(fieldsToPullListString)) {
            fieldsToPullList.addAll(Arrays.asList(fieldsToPullListString.split(";")));
        }
        return fieldsToPullList;
    }

    public void setFieldsToPullListString(String fieldsToPullListString) {
        this.fieldsToPullListString = fieldsToPullListString;
    }
    
    public FormSmartEncounter toFormSmartEncounter() {
        FormSmartEncounter newForm = new FormSmartEncounter();
        newForm.setId(this.getFormId() != 0 ? this.getFormId() : null);
        newForm.setDemographicNo(this.getDemographicNo());
        newForm.setProviderNo(this.getProviderNo());
        newForm.setAppointmentNo(this.getAppointmentNo());
        newForm.setDocumentName(this.getDocumentName());
        newForm.setFormCreated(this.getFormCreated() != null ? this.getFormCreated() : new Date());
        newForm.setFormEdited(this.getFormEdited());
        String htmlText = this.getHtmlText()
                .replaceAll("<br>", "<br/>"); // add closing tag to br tags
        newForm.setHtmlText(htmlText);
        newForm.setDeltaText(this.getDeltaText());
        newForm.setPlainText(this.getPlainText());
        newForm.setTemplateUsed(this.getTemplateUsed());
        newForm.setPlaceholderValueMap(SmartEncounterPlaceholderResolver.resolveForm(newForm, getFieldsToPullList()));
        return newForm;
    }
}
