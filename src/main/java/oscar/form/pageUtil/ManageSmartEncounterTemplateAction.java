/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.tika.Tika;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oscar.OscarProperties;
import oscar.form.dao.SmartEncounterShortCodeDao;
import oscar.form.dao.SmartEncounterTemplateDao;
import oscar.form.model.SmartEncounterShortCode;
import oscar.form.model.SmartEncounterTemplate;
import oscar.form.model.SmartEncounterTemplateImage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManageSmartEncounterTemplateAction extends DispatchAction {

    private static Logger logger = LoggerFactory.getLogger(ManageSmartEncounterTemplateAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
    private SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
    
    public ActionForward view(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        
        return mapping.findForward("success");
    }
    
    public ActionForward saveTemplate(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm formDto = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterTemplate template = formDto.toSmartEncounterTemplate();
        template.setEditedDate(new Date());
        template.setPlaceholderImageList(saveImagesAndPopulateTemplateImages(formDto));
        smartEncounterTemplateDao.saveEntity(template);

        return mapping.findForward("success");
    }

    public ActionForward deleteTemplate(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer templateId = form.getId();
        if (templateId != null) {
            SmartEncounterTemplate template = smartEncounterTemplateDao.find(templateId);
            if (template != null) {
                template.setArchived(true);
                smartEncounterTemplateDao.saveEntity(template);
            }
        }

        return mapping.findForward("success");
    }

    public ActionForward saveShortCode(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterShortCode shortCode = form.toSmartEncounterShortCode();
        shortCode.setEditedDate(new Date());
        smartEncounterShortCodeDao.saveEntity(shortCode);

        return mapping.findForward("success");
    }
    public ActionForward deleteShortCode(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer shortCodeId = form.getId();
        if (shortCodeId != null) {
            smartEncounterShortCodeDao.remove(shortCodeId);
        }

        return mapping.findForward("success");
    }

    private List<SmartEncounterTemplateImage> saveImagesAndPopulateTemplateImages(ManageSmartEncounterTemplateForm formDto) {
        List<SmartEncounterTemplateImage> placeholderImageList = new ArrayList<SmartEncounterTemplateImage>();
        for (String imageData : formDto.getImages()) {
            String[] imageDataArray = imageData.split(";");
            String identifier = imageDataArray[0];

            // already uploaded images have a url instead of data:image/FORMAT;data. skip saving
            if (imageDataArray.length == 3) {
                String base64String = imageDataArray[2];
                // Save and create accessible url for image

                base64String = base64String.replace("base64,", "");
                byte[] decodedImageData = DatatypeConverter.parseBase64Binary(base64String);
                String fileExtension = new Tika().detect(decodedImageData);
                fileExtension = fileExtension.substring(fileExtension.indexOf('/') + 1);

                String imageFileName = identifier + "." + fileExtension;

                String filepath = OscarProperties.getInstance().getProperty("form_smart_encounter_images_path") + "/" + imageFileName;

                try (FileOutputStream fos = new FileOutputStream(new File(filepath))) {
                    fos.write(decodedImageData);
                } catch (IOException e) {
                    logger.error("Failed to save smart encounter form image", e);
                }

                placeholderImageList.add(new SmartEncounterTemplateImage(identifier, imageFileName));
            }
        }
        return placeholderImageList;
    }
}
