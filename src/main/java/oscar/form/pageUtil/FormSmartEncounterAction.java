/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import com.itextpdf.text.pdf.PdfReader;
import com.lowagie.text.DocumentException;
import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.apache.tika.Tika;
import org.apache.tika.io.IOUtils;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.FaxJobDao;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.common.model.FaxJob;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;
import oscar.OscarProperties;
import oscar.form.dao.FormSmartEncounterDao;
import oscar.form.dao.SmartEncounterProviderPreferenceDao;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.SmartEncounterProviderPreference;
import oscar.form.model.FormStringValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FormSmartEncounterAction extends DispatchAction {

    private static Logger logger = LoggerFactory.getLogger(FormSmartEncounterAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private FormSmartEncounterDao formSmartEncounterDao = SpringUtils.getBean(FormSmartEncounterDao.class);
    
    public ActionForward view(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        return mapping.findForward("success");
    }
    
    public ActionForward save(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));

        return findForward(mapping, "success", saveForm(formDto));
    }

    public ActionForward print(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));
        
        FormSmartEncounter newForm = saveForm(formDto);

        try {
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=\"SmartEncounter.pdf\"");
            OutputStream os = response.getOutputStream();
            generatePdf(newForm, os);
        } catch (DocumentException | IOException e) {
            return handleExceptionAndForwardMessageToUser(mapping, "Error generating smart encounter PDF", e, newForm);
        }
        return null;
    }


    public ActionForward faxAndPrint(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws DocumentException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin.fax", "w", null)) {
            throw new SecurityException("missing required security object (_admin.fax)");
        }

        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));
        FormSmartEncounter form = saveForm(formDto);
        if (form == null) {
            return handleExceptionAndForwardMessageToUser(mapping, "Error faxing smart template form, no form exists", null, form);
        }

        String[] destinationFaxNos = request.getParameterValues("faxRecipient");
        if (destinationFaxNos == null || destinationFaxNos.length == 0) {
            return handleExceptionAndForwardMessageToUser(mapping, "Error generating smart encounter PDF, no fax recipients provided", null, form);
        }

        for (int i = 0; i < destinationFaxNos.length; i++) {
            destinationFaxNos[i] = destinationFaxNos[i].trim().replaceAll("[^0-9]", "");
            if (destinationFaxNos[i].length() < 7) {
                return handleExceptionAndForwardMessageToUser(mapping, "Document target fax number '" + destinationFaxNos[i] + "' is invalid.", null, form);
            }
        }
        // Convert to list and remove duplicate phone numbers.
        ArrayList<String> recipients = new ArrayList<String>(new HashSet<String>(Arrays.asList(destinationFaxNos)));

        String tempPath = System.getProperty("java.io.tmpdir");
        String faxClinicId = OscarProperties.getInstance().getProperty("fax_clinic_id", "1234");
        String faxNumber = "";
        ClinicDAO clinicDAO = SpringUtils.getBean(ClinicDAO.class);
        if (!faxClinicId.equals("") && clinicDAO.find(Integer.parseInt(faxClinicId)) != null) {
            faxNumber = clinicDAO.find(Integer.parseInt(faxClinicId)).getClinicFax();
            faxNumber = faxNumber.replaceAll("[^0-9]", "");
        }

        // Generate PDF
        int numPages = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            generatePdf(form, bos);
            PdfReader pdfReader = new PdfReader(bos.toByteArray());
            numPages = pdfReader.getNumberOfPages();
            pdfReader.close();
        } catch (IOException e) {
            return handleExceptionAndForwardMessageToUser(mapping, "Exception encountered when generating PDF, please contact support", e, form);
        }


        FaxJobDao faxJobDao = SpringUtils.getBean(FaxJobDao.class);
        FaxConfigDao faxConfigDao = SpringUtils.getBean(FaxConfigDao.class);
        List<FaxConfig> faxConfigs = faxConfigDao.findAll(null, null);

        // Create files in temp directory and create fax entries
        for (int i = 0; i < recipients.size(); i++) {
            String recipientFaxNo = recipients.get(i);

            String tempName = "DOC-" + faxClinicId + form.getId() + "." + i + "." + System.currentTimeMillis();

            String tempPdf = String.format("%s%s%s.pdf", tempPath, File.separator, tempName);
            String tempTxt = String.format("%s%s%s.txt", tempPath, File.separator, tempName);

            // Write the PDF
            try (OutputStream outputStream = new FileOutputStream(tempPdf)) {
                bos.writeTo(outputStream);
                // Creating text file with the specialists fax number
                PrintWriter pw = null;
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(tempTxt);
                    pw = new PrintWriter(fos);
                    pw.println(recipientFaxNo);
                } finally {
                    IOUtils.closeQuietly(pw);
                    IOUtils.closeQuietly(fos);
                }
    
    
                // A little sanity check to ensure both files exist.
                if (!new File(tempPdf).exists() || !new File(tempTxt).exists()) {
                    throw new DocumentException("Unable to create file for fax of document " + form.getId() + ".");
                }
    
                boolean validFaxNumber = false;
    
                FaxJob faxJob = new FaxJob();
                faxJob.setDestination(recipientFaxNo);
                faxJob.setFile_name(tempName + ".pdf");
                faxJob.setNumPages(numPages);
                faxJob.setFax_line(faxNumber);
                faxJob.setStamp(new Date());
                faxJob.setOscarUser(loggedInInfo.getLoggedInProviderNo());
                faxJob.setDemographicNo(form.getDemographicNo());
    
                for (FaxConfig faxConfig : faxConfigs) {
                    if (faxConfig.getFaxNumber().equals(faxNumber)) {
                        faxJob.setStatus(FaxJob.STATUS.SENT);
                        faxJob.setUser(faxConfig.getFaxUser());
                        validFaxNumber = true;
                        break;
                    }
                }
    
                if (!validFaxNumber && faxConfigs.size() > 0) {
                    faxJob.setStatus(FaxJob.STATUS.SENT);
                    faxJob.setUser(faxConfigs.get(0).getFaxUser());
                    validFaxNumber = true;
                }
    
                if (!validFaxNumber) {
                    faxJob.setStatus(FaxJob.STATUS.ERROR);
                    log.error("PROBLEM CREATING FAX JOB", new DocumentException("There are no fax configurations setup for this clinic."));
                } else {
                    faxJob.setStatus(FaxJob.STATUS.SENT);
                }
    
                faxJobDao.persist(faxJob);
            } catch(IOException e) {
                logger.error("Exception encountered when gernerating faxJob", e);
            }
        }

        try {
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=\"SmartEncounter.pdf\"");
            OutputStream os = response.getOutputStream();
            bos.writeTo(os);
        } catch (IOException e) {
            return handleExceptionAndForwardMessageToUser(mapping, "Exception encountered when generating PDF, please contact support", e, form);
        }
        return null;
    }
    
    public void getTemplatePlaceHolderValues(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }

        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        
        Map<String, String> stringValueMap = SmartEncounterPlaceholderResolver.resolve(formDto.getFieldsToPullList(), formDto.getDemographicNo(), formDto.getProviderNo());
        try {
            JSONArray resultArray = new JSONArray();
            for (Map.Entry<String, String> stringEntry : stringValueMap.entrySet()) {
                JSONObject result = new JSONObject();
                result.put("marker", stringEntry.getKey());
                result.put("value", Encode.forJavaScriptSource(stringEntry.getValue()));
                result.put("isBlock", stringValueMap.get(stringEntry.getKey() + "_isBlock"));
                resultArray.add(result);
            }
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("resolvedPlaceholders", resultArray);
            jsonResponse.put("success", true);

            response.getOutputStream().write(jsonResponse.toString().getBytes());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        } catch (IOException e) { logger.error("Exception when loading placeholder values", e); }
    }

    public ActionForward saveProviderHeaderFooter(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        
        String headerHtmlText = request.getParameter("headerHtmlText");
        String headerDeltaText = request.getParameter("headerDeltaText");
        String footerHtmlText = request.getParameter("footerHtmlText");
        String footerDeltaText = request.getParameter("footerDeltaText");

        SmartEncounterProviderPreferenceDao smartEncounterProviderPreferenceDao = SpringUtils.getBean(SmartEncounterProviderPreferenceDao.class);
        SmartEncounterProviderPreference smartEncounterProviderPreference = smartEncounterProviderPreferenceDao.findByProviderNo(loggedInInfo.getLoggedInProviderNo());
        if (smartEncounterProviderPreference == null) {
            smartEncounterProviderPreference = new SmartEncounterProviderPreference();
            smartEncounterProviderPreference.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        }
        smartEncounterProviderPreference.setHeaderHtmlText(headerHtmlText);
        smartEncounterProviderPreference.setHeaderDeltaText(headerDeltaText);
        smartEncounterProviderPreference.setFooterHtmlText(footerHtmlText);
        smartEncounterProviderPreference.setFooterDeltaText(footerDeltaText);
        smartEncounterProviderPreferenceDao.saveEntity(smartEncounterProviderPreference);
        
        return mapping.findForward("preferenceSuccess");
    }
    
    private ActionForward handleExceptionAndForwardMessageToUser(ActionMapping mapping, String message, Throwable e, FormSmartEncounter form) {
        if (e != null) {
            logger.error(message, e);
        }
        ActionRedirect redirect = new ActionRedirect(mapping.findForward("success"));
        redirect.addParameter("formId", form.getId());
        redirect.addParameter("demographic_no", form.getDemographicNo());
        redirect.addParameter("appointment_no", StringUtils.trimToEmpty(String.valueOf(form.getAppointmentNo())));
        redirect.addParameter("error_message", message);
        return redirect;
    }
    
    private ActionForward findForward(ActionMapping mapping, String forwardName, FormSmartEncounter form) {
        ActionRedirect redirect = new ActionRedirect(mapping.findForward(forwardName));
        redirect.addParameter("formId", form.getId());
        redirect.addParameter("demographic_no", form.getDemographicNo());
        redirect.addParameter("appointment_no", StringUtils.trimToEmpty(String.valueOf(form.getAppointmentNo())));
        return redirect;
    }
    
    private FormSmartEncounter saveForm(SmartEncounterActionForm formDto) {
        FormSmartEncounter newForm = formDto.toFormSmartEncounter();
        // Save images locally
        newForm.setPlaceholderImageMap(saveImagesAndPopulatePlaceholderImageUrlMap(formDto, newForm));
        return formSmartEncounterDao.saveEntity(newForm);
    }
    
    private Map<String, FormStringValue> saveImagesAndPopulatePlaceholderImageUrlMap(SmartEncounterActionForm formDto, FormSmartEncounter newForm) {
        Map<String, FormStringValue> placeholderImageUrlMap = new HashMap<String, FormStringValue>();
        for (String imageData : formDto.getImages()) {
            String[] imageDataArray = imageData.split(";");
            String identifier = imageDataArray[0];

            // already uploaded images have a url instead of data:image/FORMAT;data. skip saving
            if (imageDataArray.length == 3) {
                String base64String = imageDataArray[2];
                // Save and create accessible url for image

                base64String = base64String.replace("base64,", "");
                byte[] decodedImageData = DatatypeConverter.parseBase64Binary(base64String);
                String fileExtension = new Tika().detect(decodedImageData);
                fileExtension = fileExtension.substring(fileExtension.indexOf('/') + 1);

                String imageFileName = identifier + "." + fileExtension;

                String filepath = OscarProperties.getInstance().getProperty("form_smart_encounter_images_path") + "/" + imageFileName;

                try (FileOutputStream fos = new FileOutputStream(new File(filepath))) {
                    fos.write(decodedImageData);
                } catch (IOException e) {
                    logger.error("Failed to save smart encounter form image", e);
                }

                placeholderImageUrlMap.put(identifier, new FormStringValue(newForm.getFormTable(), newForm.getId(), identifier, imageFileName));
            } else if (imageDataArray.length == 2) {
                String imageFileName = imageDataArray[1].substring(imageDataArray[1].indexOf("imagefile=") + 10); // TODO: better way of this
                placeholderImageUrlMap.put(identifier, new FormStringValue(newForm.getFormTable(), newForm.getId(), identifier, imageFileName));
            }
        }
        return placeholderImageUrlMap;
    }
    
    private void generatePdf(FormSmartEncounter form, OutputStream os) throws DocumentException {
        String htmlData = form.getHtmlTextWithValuesFilled();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(htmlData);
        renderer.layout();
        renderer.createPDF(os);
    }
}
