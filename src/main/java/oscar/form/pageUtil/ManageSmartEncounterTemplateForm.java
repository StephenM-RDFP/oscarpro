/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import org.apache.struts.action.ActionForm;
import oscar.form.model.SmartEncounterShortCode;
import oscar.form.model.SmartEncounterTemplate;

import java.util.Date;

public class ManageSmartEncounterTemplateForm extends ActionForm {

    private Integer id;
    private String templateName;
    private String shortCodeName;
    private Long createDateLong;
    private Date editedDate;
    private String template;
    private String shortCodeText;
    private String[] images = new String[0];

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getShortCodeName() {
        return shortCodeName;
    }

    public void setShortCodeName(String shortCodeName) {
        this.shortCodeName = shortCodeName;
    }

    public Long getCreateDateLong() {
        return createDateLong;
    }

    public void setCreateDateLong(Long createDateLong) {
        this.createDateLong = createDateLong;
    }

    public Date getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(Date editedDate) {
        this.editedDate = editedDate;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getShortCodeText() {
        return shortCodeText;
    }

    public void setShortCodeText(String shortCodeText) {
        this.shortCodeText = shortCodeText;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public SmartEncounterTemplate toSmartEncounterTemplate() {
        SmartEncounterTemplate newTemplate = new SmartEncounterTemplate();
        newTemplate.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newTemplate.setName(this.getTemplateName());
        newTemplate.setCreateDate((this.getCreateDateLong() != null && 0L != this.getCreateDateLong()) ? new Date(this.getCreateDateLong()) : new Date());
        newTemplate.setEditedDate(this.getEditedDate());
        newTemplate.setTemplate(this.getTemplate());
        newTemplate.setArchived(false);
        return newTemplate;
    }

    public SmartEncounterShortCode toSmartEncounterShortCode() {
        SmartEncounterShortCode newShortCode = new SmartEncounterShortCode();
        newShortCode.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newShortCode.setName(this.getShortCodeName());
        newShortCode.setCreateDate((this.getCreateDateLong() != null && 0L != this.getCreateDateLong()) ? new Date(this.getCreateDateLong()) : new Date());
        newShortCode.setEditedDate(this.getEditedDate());
        newShortCode.setText(this.getShortCodeText());
        return newShortCode;
    }
}
