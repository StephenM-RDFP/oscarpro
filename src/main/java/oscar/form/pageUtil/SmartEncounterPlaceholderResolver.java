/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;


import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.eform.APExecute;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.FormStringValue;
import oscar.form.model.SmartEncounterTemplatePlaceholder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for accepting SmartEncounterForms and resolves the template values with the relevant demographic, provider, and specialist values
 */
public class SmartEncounterPlaceholderResolver {
    
    private static Logger logger = MiscUtils.getLogger();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public static Map<String, FormStringValue> resolveForm(FormSmartEncounter form, List<String> fieldsToPullList) {
        Map<String, String> stringValueMap = resolve(fieldsToPullList, form.getDemographicNo(), form.getProviderNo());
        HashMap<String, FormStringValue> formStringValueMap = new HashMap<String, FormStringValue>();
        for (Map.Entry<String, String> entry : stringValueMap.entrySet()) {
            formStringValueMap.put(entry.getKey(),  new FormStringValue(form.getFormTable(), form.getId(), entry.getKey(), entry.getValue()));
        }
        return formStringValueMap;
    }
    
    public static Map<String, String> resolve(List<String> fieldsToPullList, Integer demographicNo, String providerNo) {
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
        DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
        Provider formProvider = providerDao.getProvider(providerNo);
        Demographic formDemographic = demographicDao.getDemographic(demographicNo);
        ProfessionalSpecialist familyDoctor = getDemographicLinkedSpecialist(formDemographic, ProfessionalSpecialist.DemographicRelationship.FAMILY_DOCTOR);
        ProfessionalSpecialist referralDoctor = getDemographicLinkedSpecialist(formDemographic, ProfessionalSpecialist.DemographicRelationship.REFERRAL_DOCTOR);
        
        HashMap<String, String> stringValueMap = new HashMap<String, String>();
        for (String subjectAndField : fieldsToPullList) {
            if (!stringValueMap.containsKey(subjectAndField)) {
                SmartEncounterTemplatePlaceholder placeholder = SmartEncounterTemplatePlaceholder.getPlaceholderSubjectAndField(subjectAndField);
                Method methodToInvoke =  SmartEncounterTemplatePlaceholder.getMethodBySubjectAndField(subjectAndField);
                if (placeholder != null && methodToInvoke != null) {
                    String value = null;
                    try {
                        ProfessionalSpecialist specialist = getRelevantSpecialist(subjectAndField, familyDoctor, referralDoctor);
                        if (methodToInvoke.getDeclaringClass() == APExecute.class) {
                            value = invokeEformApMethod(methodToInvoke, formDemographic, subjectAndField);
                        } else {
                            value = invokeClassMethod(methodToInvoke, formDemographic, formProvider, specialist);
                        }
                    } catch (InvocationTargetException | IllegalAccessException e) {
                        logger.error("Exception encountered when attempting to resolve smart template placeholder method: " + methodToInvoke.toGenericString());
                    }
                    
                    if (value != null) {
                        if (placeholder.isTextBlock()) {
                            value += "\n";
                        }
                        if (value.length() > 255) {
                            value = value.substring(0, 255);
                        }
                        stringValueMap.put(subjectAndField, value);
                        stringValueMap.put(subjectAndField + "_isBlock", String.valueOf(placeholder.isTextBlock()));
                        
                    }
                }
            }
        }
        
        return stringValueMap;
    }

    /**
     * Invokes the provided method with the corresponding provided demographic/provider/specialist object
     */
    private static String invokeClassMethod(Method methodToInvoke, Demographic demographic, Provider provider, 
                                      ProfessionalSpecialist professionalSpecialist) throws InvocationTargetException, IllegalAccessException {
        if (methodToInvoke.getDeclaringClass() == Demographic.class && demographic != null) {
            return String.valueOf(methodToInvoke.invoke(demographic));
        } else if (methodToInvoke.getDeclaringClass() == Provider.class && provider != null) {
            return String.valueOf(methodToInvoke.invoke(provider));
        } else if (methodToInvoke.getDeclaringClass() == ProfessionalSpecialist.class && professionalSpecialist != null) {
            return String.valueOf(methodToInvoke.invoke(professionalSpecialist));
        } else if (methodToInvoke.getDeclaringClass() == DateFormat.class) {
            return String.valueOf(methodToInvoke.invoke(dateFormat, new Date()));
        }
        return null;
    }
    /**
     * Invokes the provided method with the eform AP service
     */
    private static String invokeEformApMethod(Method methodToInvoke, Demographic demographic, String subjectAndField) throws InvocationTargetException, IllegalAccessException {
        APExecute apExecute = new APExecute();
        String field = subjectAndField.replace("eformAp.", "");
        if (demographic != null) {
            return String.valueOf(methodToInvoke.invoke(apExecute, field, demographic.getDemographicNo().toString()));
        }
        return null;
    }

    private static ProfessionalSpecialist getDemographicLinkedSpecialist(Demographic demographic, ProfessionalSpecialist.DemographicRelationship demographicRelationship) {
        ProfessionalSpecialistDao professionalSpecialistDao = SpringUtils.getBean(ProfessionalSpecialistDao.class);
        DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
        DemographicExt demographicExt = demographicExtDao.getDemographicExt(demographic.getDemographicNo(), demographicRelationship.getDemographicExtKeyVal());
        ProfessionalSpecialist professionalSpecialist = null;
        if (demographicExt != null) {
            try {
                professionalSpecialist = professionalSpecialistDao.find(Integer.valueOf(demographicExt.getValue()));
            } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + demographicExt.getValue()); }
        }
        return professionalSpecialist;
    }

    /**
     * Determines what professional specialist object should be passed to the invokeMethod() function
     * @param subjectAndField The subject and field mapped string to use
     * @param familyDoctor The demographic's family doctor
     * @param referralDoctor The demographic's referral doctor
     * @return
     */
    private static ProfessionalSpecialist getRelevantSpecialist(String subjectAndField, ProfessionalSpecialist familyDoctor, ProfessionalSpecialist referralDoctor) {
        if (subjectAndField.startsWith("demographic.familyDoctor")) {
            return familyDoctor;
        } else if (subjectAndField.startsWith("demographic.referringPhysician")) {
            return referralDoctor;
        }
        return null;
    }
}
