package oscar.form.util;

import org.oscarehr.util.LoggedInInfo;

import java.io.OutputStream;
import java.util.List;

public interface JasperReportPdfPrint {
    
    void PrintJasperPdf(OutputStream os, LoggedInInfo loggedInInfo, Integer demographicNo, Integer formId, List<Integer> pagesToPrint);
}
