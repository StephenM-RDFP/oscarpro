
package oscar.oscarBilling.ca.bc.MSP;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.MiscUtils;

import org.oscarehr.util.SpringUtils;
import oscar.OscarAction;
import oscar.OscarDocumentCreator;
import oscar.entities.MSPBill;
import oscar.oscarBilling.ca.bc.MSP.MSPReconcile.BillSearch;
import oscar.oscarBilling.ca.bc.data.PayRefSummary;

/**
 *
 * <p>Title: CreateBillingReportAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Joel Legris
 * @version 1.0
 */
public class CreateBillingReportAction extends OscarAction {

    private Properties reportCfg = new Properties();
    private OscarDocumentCreator osc = new OscarDocumentCreator();
    private boolean showICBC;
    private boolean showMSP;
    private boolean showPriv;
    private boolean showWCB;
    private static final String REPORTS_PATH = "oscar/oscarBilling/ca/bc/reports/";
    
    private SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    private PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);


    public CreateBillingReportAction() {
        this.cfgReports();

    }

    /**
     * Performs Report Generation Logic based on the supplied parameters form the submitted form
     */
    public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        request.getSession().getServletContext().getServletContextName();
        if (!System.getProperties().containsKey("jasper.reports.compile.class.path")) {
            String classpath = (String)getServlet().getServletContext().getAttribute("org.apache.catalina.jsp_classpath");
            if (classpath==null) classpath = (String)request.getSession().getServletContext().getAttribute("com.ibm.websphere.servlet.application.classpath");
            System.setProperty("jasper.reports.compile.class.path", classpath);
        }
        if (!System.getProperties().containsKey("java.awt.headless")) {
            System.setProperty("java.awt.headless", "true");
        }

        CreateBillingReportActionForm frm = (CreateBillingReportActionForm)actionForm;

        //get form field data
        String docFmt = frm.getDocFormat();
        String repType = frm.getRepType();
        String account = frm.getSelAccount();
        String payee = frm.getSelPayee();
        String provider = frm.getSelProv();
        String startDate = frm.getXml_vdate();
        String endDate = frm.getXml_appointment_date();
        String repDef = docFmt + "_" + this.reportCfg.getProperty(repType);
        showICBC = new Boolean(frm.getShowICBC()).booleanValue();
        showMSP = new Boolean(frm.getShowMSP()).booleanValue();
        showPriv = new Boolean(frm.getShowPRIV()).booleanValue();
        showWCB = new Boolean(frm.getShowWCB()).booleanValue();
        String insurers = createInsurerList();

        //Map of insurer types to be used in bill search criteria
        HashMap reportParams = new HashMap();
        reportParams.put("startDate", startDate);
        reportParams.put("endDate", endDate);
        reportParams.put("insurers", insurers);
        ServletOutputStream outputStream = this.getServletOstream(response);
        MSPReconcile msp = new MSPReconcile();
        BillSearch billSearch = null;

        //open corresponding Jasper Report Definition
        InputStream reportInstream = osc.getDocumentStream(REPORTS_PATH + repDef);
        try {
	        //COnfigure Reponse Header
	        cfgHeader(response, repType, docFmt);
	        //select appropriate report retrieval method
	        if (repType.equals(MSPReconcile.REP_ACCOUNT_REC) || repType.equals(MSPReconcile.REP_INVOICE) || repType.equals(MSPReconcile.REP_WO)) {
	
	            billSearch = msp.getBillsByType(account, payee, provider, startDate, endDate, !showWCB, !showMSP, !showPriv, !showICBC, repType);
	            String billCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "billing_no"));
	            String demNoCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "demoNo"));
	            if (repType.equals(MSPReconcile.REP_ACCOUNT_REC)) {
	                reportParams.put("amtSubmitted", msp.getTotalPaidByStatus(billSearch.list, MSPReconcile.SUBMITTED));
	            }
	            else if (repType.equals(MSPReconcile.REP_WO)) {
	                oscar.entities.Provider payeeProv = msp.getProvider(payee, 1);
	                oscar.entities.Provider provProv = msp.getProvider(provider, 0);
	                reportParams.put("provider", provider.equals("ALL")?"ALL":payeeProv.getFullName());
	                reportParams.put("payee", payee.equals("ALL")?"ALL":provProv.getFullName());
	            }
	
	            oscar.entities.Provider acctProv = msp.getProvider(account, 0);
	            reportParams.put("billCnt", billCnt);
	            reportParams.put("demNoCnt", demNoCnt);
	            reportParams.put("account", account.equals("ALL")?"ALL":acctProv.getFullName());
	
	            //Fill document with report parameter data
	            osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, billSearch.list);
	
	        }
	        else if (repType.equals(MSPReconcile.REP_MSPREM)) {
	            oscar.entities.Provider payeeProv = msp.getProvider(payee, 1);
	            reportParams.put("payee", payeeProv.getFullName());
	            reportParams.put("payeeno", payee);
	            String s21id = request.getParameter("rano");
	            osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, msp.getMSPRemittanceQuery(payee, s21id));
	        }
			else if (repType.equals(MSPReconcile.REP_MSPREM_NEW)) {
				oscar.entities.Provider payeeProv = msp.getProvider(payee, 1);
				reportParams.put("payee", payeeProv.getFullName());
				reportParams.put("payeeno", payee);
				String s21id = request.getParameter("rano");

				SimpleDateFormat preFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat postFormat = new SimpleDateFormat("yyyy-MM-dd");
				ResultSet rs = msp.getAdjustmentsMSPRemittanceQuery(payee, s21id);
				// Add up the Adjustment totals and check for accurate billing matches
				double adjustmentTotal = 0.00;
				String explanatoryCodes = "";
				ArrayList unmatchedInvoices = new ArrayList();
				try {
					while (rs.next()) {
						// Get the received date and compare to the submission date
						String billingmasterNo = rs.getString("billingmaster_no");
						String formattedDate = "";
						String dataCenterNo = rs.getString("t_datacenter");
						try {
							String receivedDate = rs.getString("t_msprcddate");
							if (StringUtils.isNotEmpty(receivedDate)) {
								Date parsedDate = preFormat.parse(receivedDate);
								formattedDate = postFormat.format(parsedDate);
							}
						} catch (java.text.ParseException pe) {
							MiscUtils.getLogger().error("Error", pe);
						}
						formattedDate += "%";
						if (!msp.hasMatchingBillingRecord(billingmasterNo, formattedDate, dataCenterNo)) {
							unmatchedInvoices.add(billingmasterNo);
						}
						// Get the total of all adjustments
						adjustmentTotal += (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja1")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja2")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja3")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja4")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja5")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja6")))) + (Double.parseDouble(MSPReconcile.convCurValue(rs.getString("t_aja7"))));
					}
					rs.beforeFirst();
				} catch (SQLException e) {
					MiscUtils.getLogger().error("Error", e);
				}
				reportParams.put("unmatchedInvoices", unmatchedInvoices);
				reportParams.put("adjustmentTotal", adjustmentTotal);
				osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, rs);
			}
	        else if (repType.equals(MSPReconcile.REP_MSPREMSUM)) {
	            String s21id = request.getParameter("rano");
	            oscar.entities.S21 s21 = msp.getS21Record(s21id);
	
	            oscar.entities.Provider payeeProv = msp.getProvider(provider, 1);
	            reportParams.put("mspBean", msp);
	            //set parameters for payee of provider
	            reportParams.put("provider", payeeProv.equals("ALL")?"ALL":payeeProv.getInitials());
	            reportParams.put("providerNo", payeeProv.getProviderNo());
	            //set parameters for S21 report header
	            reportParams.put("payeeName", s21.getPayeeName());
	            reportParams.put("amtBilled", s21.getAmtBilled());
	            reportParams.put("amtPaid", s21.getAmtPaid());
	            reportParams.put("cheque", s21.getCheque());
	            reportParams.put("s21id", s21id);
	            reportParams.put("paymentDate", s21.getPaymentDate());
	            reportParams.put("payeeNo", s21.getPayeeNo());
	
	            //This is the practitioner summary subreport stream
	            InputStream subPractSum = osc.getDocumentStream(REPORTS_PATH + this.reportCfg.getProperty("REP_MSPREMSUM_PRACTSUM"));
				try {
					reportParams.put("practSum", osc.getJasperReport(subPractSum));
				} 
				finally {
					IOUtils.closeQuietly(subPractSum);
				}

	
	            //This is the S23 summary subreport stream
	            InputStream subS23 = osc.getDocumentStream(REPORTS_PATH + this.reportCfg.getProperty("REP_MSPREMSUM_S23"));
				try {
		            reportParams.put("adj", osc.getJasperReport(subS23));
				} 
				finally {
					IOUtils.closeQuietly(subS23);
				}
				
	            InputStream subS23orphan = osc.getDocumentStream(REPORTS_PATH + this.reportCfg.getProperty("REP_MSPREMSUM_S23_ORPHAN"));
				try {
		            reportParams.put("orphanAdj", osc.getJasperReport(subS23orphan));
				} 
				finally {
					IOUtils.closeQuietly(subS23orphan);
				}
	           
	            //This is the broadcast messages subreport stream
	            InputStream msgs = osc.getDocumentStream(REPORTS_PATH + this.reportCfg.getProperty("MSGS"));
				try {
		            reportParams.put("msgs", osc.getJasperReport(msgs));
				} 
				finally {
					IOUtils.closeQuietly(msgs);
				}
	
	            try {
	                osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, DbConnectionFilter.getThreadLocalDbConnection());
	            }
	            catch (SQLException e) {
	                MiscUtils.getLogger().error("Error", e);
	            }
	
	        }
	
	        else if (repType.equals(MSPReconcile.REP_PAYREF) || repType.equals(MSPReconcile.REP_PAYREF_SUM) ||  repType.equals(MSPReconcile.REP_PAYREF_SUM_GST) ||  repType.equals(MSPReconcile.REP_PAYREF_GST)) {
	            billSearch = msp.getPayments(account, payee, provider, startDate, endDate, !showWCB, !showMSP, !showPriv, !showICBC);
	            oscar.entities.Provider payeeProv = msp.getProvider(payee, 1);
	            oscar.entities.Provider acctProv = msp.getProvider(account, 0);
	            oscar.entities.Provider provProv = msp.getProvider(provider, 0);
	            PayRefSummary sumPayed = new PayRefSummary();
	            PayRefSummary sumRefunded = new PayRefSummary();
	            double gstBillings = 0.00;
	            double nonGstBillings = 0.00;
				double gstTotal = 0.00;
	            for (Iterator iter = billSearch.list.iterator(); iter.hasNext();) {
	
	                MSPBill item = (MSPBill)iter.next();
	                double dblValue = Double.parseDouble(item.getAmount());
	                if (dblValue < 0) {
	                    sumRefunded.addIncValue(item.getPaymentMethod(), dblValue);
	                }
	                sumPayed.addIncValue(item.getPaymentMethod(), dblValue);
	                sumPayed.addAdjustmentAmount(item.getAdjustmentCodeAmt());
	                
	                double gst = Double.parseDouble(item.getGst());
	                gstTotal += gst;
	                if(gst == 0){
	                	nonGstBillings += dblValue;
					} else {
	                	gstBillings += dblValue - gst;
					}
	            }
	            reportParams.put("sumPayed", sumPayed);
	            reportParams.put("sumRefunded", sumRefunded);
	
	            reportParams.put("account", account.equals("ALL")?"ALL":acctProv.getFullName());
	            reportParams.put("gstNumber", account.equals("ALL")?"":findGstNoForProvider(acctProv.getProviderNo()));
	
	            reportParams.put("provider", provider.equals("ALL")?"ALL":provProv.getInitials());
	            reportParams.put("payee", payee.equals("ALL")?"ALL":payeeProv.getInitials());
	            reportParams.put("startDate", startDate);
	            reportParams.put("endDate", endDate);
	            reportParams.put("totalGST", "$" + new BigDecimal(gstTotal).setScale(2, RoundingMode.HALF_UP));
	            reportParams.put("totalNoGSTBillings", "$" + new BigDecimal(nonGstBillings).setScale(2, RoundingMode.HALF_UP));
	            reportParams.put("totalGSTBillings", "$" + new BigDecimal(gstBillings).setScale(2, RoundingMode.HALF_UP));
	
	            //Fill document with report parameter data
	            osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, billSearch.list);
	
	        } else if(repType.equals(MSPReconcile.REP_GST_SUM)){

				billSearch = msp.getPayments(account, payee, provider, startDate, endDate, !showWCB, !showMSP, !showPriv, !showICBC);

				String billCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "billing_no"));
				String demNoCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "demoNo"));
				
				Map<String, Map<String, String>> providerReports = new HashMap<>();
				PayRefSummary sumPayed = new PayRefSummary();
				PayRefSummary sumRefunded = new PayRefSummary();
				double gstBillings = 0.00;
				double nonGstBillings = 0.00;
				double gstTotal = 0.00;

				for(Object element : billSearch.list){
					MSPBill billItem = (MSPBill) element;
					
					double payment = Double.parseDouble(billItem.getAmount());
					double gst = Double.parseDouble(billItem.getGst());

					Map<String, String> providerReport = providerReports.get(billItem.userno);
					
					if(providerReport == null){
						providerReport = new HashMap<>();
						providerReports.put(billItem.userno, providerReport);
						providerReport.put("provTotalNoGST", "0.00");
						providerReport.put("provTotalGSTApplicable", "0.00");
						providerReport.put("provTotalGST", "0.00");
						providerReport.put("provTotal", "0.00");
						providerReport.put("provName", billItem.accountName);
					}
					
					double provTotalNoGST = Double.parseDouble(providerReport.get("provTotalNoGST"));
					double provTotalGSTApplicable = Double.parseDouble(providerReport.get("provTotalGSTApplicable"));
					double provTotalGST = Double.parseDouble(providerReport.get("provTotalGST"));
					double provTotal = Double.parseDouble(providerReport.get("provTotal"));
					
					providerReport.put("provTotal", new BigDecimal(provTotal + payment).setScale(2, RoundingMode.HALF_UP).toString());
					providerReport.put("provGSTNumber", findGstNoForProvider(billItem.userno));
					
					sumPayed.addIncValue(billItem.getPaymentMethod(), payment);
					sumPayed.addAdjustmentAmount(billItem.getAdjustmentCodeAmt());

					gstTotal += gst;
					providerReport.put("provTotalGST", new BigDecimal(provTotalGST + gst).setScale(2, RoundingMode.HALF_UP).toString());
					if(gst == 0){
						nonGstBillings += payment;
						providerReport.put("provTotalNoGST", new BigDecimal(provTotalNoGST + payment).setScale(2, RoundingMode.HALF_UP).toString());
					} else {
						gstBillings += payment - gst;
						providerReport.put("provTotalGSTApplicable", new BigDecimal(provTotalGSTApplicable + payment - gst).setScale(2, RoundingMode.HALF_UP).toString());
					}
				}
				
				reportParams.put("provider", "ALL");
				reportParams.put("payee", "ALL");
				reportParams.put("billCnt", billCnt);
				reportParams.put("demNoCnt", demNoCnt);
				reportParams.put("account", "ALL");
				reportParams.put("startDate", startDate);
				reportParams.put("endDate", endDate);
				reportParams.put("sumPayed", sumPayed);
				reportParams.put("sumRefunded", sumRefunded);
				reportParams.put("totalGST", "$" + new BigDecimal(gstTotal).setScale(2, RoundingMode.HALF_UP));
				reportParams.put("totalNoGSTBillings", "$" + new BigDecimal(nonGstBillings).setScale(2, RoundingMode.HALF_UP));
				reportParams.put("totalGSTBillings", "$" + new BigDecimal(gstBillings).setScale(2, RoundingMode.HALF_UP));

				List<Map<String, String>> list = new ArrayList<>(providerReports.values());
				
				osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, list);

			} else if (repType.equals(MSPReconcile.REP_GST_SUM_SPLIT)) {

				billSearch = msp.getPayments(account, payee, provider, startDate, endDate, !showWCB, !showMSP, !showPriv, !showICBC);

				String billCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "billing_no"));
				String demNoCnt = String.valueOf(msp.getDistinctFieldCount(billSearch.list, "demoNo"));

				Map<String, Map<String, String>> providerReports = new HashMap<>();
				PayRefSummary sumPayed = new PayRefSummary();
				PayRefSummary sumRefunded = new PayRefSummary();
				BigDecimal gstBillings = new BigDecimal("0.00");
				BigDecimal gstBillingsProvider = new BigDecimal("0.00");
				BigDecimal gstBillingsClinic = new BigDecimal("0.00");
				BigDecimal nonGstBillings = new BigDecimal("0.00");
				BigDecimal nonGstBillingsProvider = new BigDecimal("0.00");
				BigDecimal nonGstBillingsClinic = new BigDecimal("0.00");
				BigDecimal total = new BigDecimal("0.00");
				BigDecimal totalProvider = new BigDecimal("0.00");
				BigDecimal totalClinic = new BigDecimal("0.00");
				BigDecimal gstTotal = new BigDecimal("0.00");
				BigDecimal gstTotalProvider = new BigDecimal("0.00");
				BigDecimal gstTotalClinic = new BigDecimal("0.00");

				for(Object element : billSearch.list){
					MSPBill billItem = (MSPBill) element;

					BigDecimal provFeeSplit = findFeeSplitForProvider(billItem.userno);
					BigDecimal payment = new BigDecimal(billItem.getAmount());
					//cents will get rounded off once it's sent to the jasper report
					BigDecimal provPay = payment.multiply(provFeeSplit).divide(new BigDecimal("100"), 100, RoundingMode.HALF_UP);
					BigDecimal clinicPay = payment.subtract(provPay);
					BigDecimal gst = new BigDecimal(billItem.getGst());
					BigDecimal provGst = gst.multiply(provFeeSplit).divide( new BigDecimal("100"),100,  RoundingMode.HALF_UP);
					BigDecimal clinicGst = gst.subtract(provGst);

					Map<String, String> providerReport = providerReports.get(billItem.userno);

					if(providerReport == null){
						providerReport = new HashMap<>();
						providerReports.put(billItem.userno, providerReport);
						providerReport.put("provTotalNoGSTProvider", "0.00");
						providerReport.put("provTotalNoGSTClinic", "0.00");
						providerReport.put("provTotalGSTApplicableProvider", "0.00");
						providerReport.put("provTotalGSTApplicableClinic", "0.00");
						providerReport.put("provTotalGSTProvider", "0.00");
						providerReport.put("provTotalGSTClinic", "0.00");
						providerReport.put("provTotalProvider", "0.00");
						providerReport.put("provTotalClinic", "0.00");
						providerReport.put("provName", billItem.accountName);
						providerReport.put("provFeeSplit", new BigDecimal("100").subtract(provFeeSplit).setScale(0, RoundingMode.HALF_DOWN).toString() + "/" + provFeeSplit.setScale(0, RoundingMode.HALF_UP).toString());
					}

					BigDecimal provTotalNoGSTProvider = new BigDecimal(providerReport.get("provTotalNoGSTProvider"));
					BigDecimal provTotalNoGSTClinic = new BigDecimal(providerReport.get("provTotalNoGSTClinic"));
					BigDecimal provTotalGSTApplicableProvider = new BigDecimal(providerReport.get("provTotalGSTApplicableProvider"));
					BigDecimal provTotalGSTApplicableClinic = new BigDecimal(providerReport.get("provTotalGSTApplicableClinic"));
					BigDecimal provTotalGSTProvider = new BigDecimal(providerReport.get("provTotalGSTProvider"));
					BigDecimal provTotalGSTClinic = new BigDecimal(providerReport.get("provTotalGSTClinic"));
					BigDecimal provTotalProvider = new BigDecimal(providerReport.get("provTotalProvider"));
					BigDecimal provTotalClinic = new BigDecimal(providerReport.get("provTotalClinic"));

					providerReport.put("provTotalProvider", provTotalProvider.add(provPay).toString());
					providerReport.put("provTotalClinic", provTotalClinic.add(clinicPay).toString());
					providerReport.put("provGSTNumber", findGstNoForProvider(billItem.userno));

					sumPayed.addIncValue(billItem.getPaymentMethod(), payment.doubleValue());
					sumPayed.addAdjustmentAmount(billItem.getAdjustmentCodeAmt());

					total = total.add(payment);
					totalProvider = totalProvider.add(provPay);
					totalClinic = totalClinic.add(clinicPay);
					gstTotal = gstTotal.add(gst);
					gstTotalProvider = gstTotalProvider.add(provGst);
					gstTotalClinic = gstTotalClinic.add(clinicGst);
					providerReport.put("provTotalGSTProvider", provTotalGSTProvider.add(provGst).toString());
					providerReport.put("provTotalGSTClinic", provTotalGSTClinic.add(clinicGst).toString());
					
					if(gst.compareTo(BigDecimal.ZERO) == 0){
						nonGstBillings = nonGstBillings.add(payment);
						nonGstBillingsProvider = nonGstBillingsProvider.add(provPay);
						nonGstBillingsClinic = nonGstBillingsClinic.add(clinicPay);
						providerReport.put("provTotalNoGSTProvider", provTotalNoGSTProvider.add(provPay).toString());
						providerReport.put("provTotalNoGSTClinic", provTotalNoGSTClinic.add(clinicPay).toString());
					} else {
						gstBillings = gstBillings.add(payment.subtract(gst));
						gstBillingsProvider = gstBillingsProvider.add(provPay.subtract(provGst));
						gstBillingsClinic = gstBillingsClinic.add(clinicPay.subtract(clinicGst));
						providerReport.put("provTotalGSTApplicableProvider", provTotalGSTApplicableProvider.add(provPay).subtract(provGst).toString());
						providerReport.put("provTotalGSTApplicableClinic", provTotalGSTApplicableClinic.add(clinicPay).subtract(clinicGst).toString());
					}
				}

				reportParams.put("provider", "ALL");
				reportParams.put("payee", "ALL");
				reportParams.put("billCnt", billCnt);
				reportParams.put("demNoCnt", demNoCnt);
				reportParams.put("account", "ALL");
				reportParams.put("startDate", startDate);
				reportParams.put("endDate", endDate);
				reportParams.put("sumPayed", sumPayed);
				reportParams.put("sumRefunded", sumRefunded);
				reportParams.put("total", NumberFormat.getCurrencyInstance().format(total.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalProvider", NumberFormat.getCurrencyInstance().format(totalProvider.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalClinic", NumberFormat.getCurrencyInstance().format(totalClinic.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGST", NumberFormat.getCurrencyInstance().format(gstTotal.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGSTProvider", NumberFormat.getCurrencyInstance().format(gstTotalProvider.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGSTClinic", NumberFormat.getCurrencyInstance().format(gstTotalClinic.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalNoGSTBillings", NumberFormat.getCurrencyInstance().format(nonGstBillings.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalNoGSTBillingsProvider", NumberFormat.getCurrencyInstance().format(nonGstBillingsProvider.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalNoGSTBillingsClinic", NumberFormat.getCurrencyInstance().format(nonGstBillingsClinic.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGSTBillings", NumberFormat.getCurrencyInstance().format(gstBillings.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGSTBillingsProvider", NumberFormat.getCurrencyInstance().format(gstBillingsProvider.setScale(2,  RoundingMode.HALF_UP)));
				reportParams.put("totalGSTBillingsClinic", NumberFormat.getCurrencyInstance().format(gstBillingsClinic.setScale(2,  RoundingMode.HALF_UP)));

				List<Map<String, String>> list = new ArrayList<>(providerReports.values());

				osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, list);
				
			} else if (repType.equals(MSPReconcile.REP_REJ)) {
	            billSearch = msp.getBillsByType(account, payee, provider, startDate, endDate, !showWCB, !showMSP, !showPriv, !showICBC, repType);
	
	            
	            oscar.entities.Provider payProv = msp.getProvider(payee, 1);
	            reportParams.put("account", account.equals("ALL")?"ALL":payProv.getFullName());
	            //Fill document with report parameter data
	            osc.fillDocumentStream(reportParams, outputStream, docFmt, reportInstream, billSearch.list);
	        }
	   }
	   finally {
		   IOUtils.closeQuietly(reportInstream);
	   }

        return actionMapping.findForward(this.target);
    }

    /**
     * A convenience method that returns a concatenated list of insurer types
     * to be passed into a report
     * @return String
     */
    private String createInsurerList() {
        String insurers = "";

        if (showICBC && showMSP && showPriv && showWCB) {
            insurers = "ALL";
        }
        else {
            if (showICBC) {
                insurers += "ICBC,";
            }
            if (showMSP) {
                insurers += "MSP,";
            }
            if (showPriv) {
                insurers += "Private,";
            }
            if (showWCB) {
                insurers += "WCB,";
            }
        }
        return insurers;
    }

    /**
     * Configures document association settings. Associates a selected report with
     * a specific jasper report definition
     */
    public void cfgReports() {
        this.reportCfg.setProperty(MSPReconcile.REP_INVOICE, "rep_invoice.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_PAYREF, "rep_payref.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_PAYREF_SUM, "rep_payref_sum.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_PAYREF_GST, "rep_payref_gst.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_PAYREF_SUM_GST, "rep_payref_sum_gst.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_GST_SUM, "rep_gst_sum.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_GST_SUM_SPLIT, "rep_gst_sum_split.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_ACCOUNT_REC, "rep_account_rec.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_REJ, "rep_rej.jrxml");

        this.reportCfg.setProperty(MSPReconcile.REP_WO, "rep_wo.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_MSPREM, "rep_msprem.jrxml");
		this.reportCfg.setProperty(MSPReconcile.REP_MSPREM_NEW, "rep_msprem_new.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_MSPREMSUM, "rep_mspremsum.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_MSPREMSUM_PRACTSUM, "msppremsum.practsum.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_MSPREMSUM_S23, "msppremsum.s23.jrxml");
        this.reportCfg.setProperty(MSPReconcile.REP_MSPREMSUM_S23_ORPHAN, "msppremsum.s23_orphan.jrxml");
        this.reportCfg.setProperty("MSGS", "broadcastmessages.jrxml");
    }
	private String findGstNoForProvider(String billingProvider) {
		List<Property> props = propertyDao.findByNameAndProvider("gst_number", billingProvider);
		String gstNo = props.isEmpty() ? null : props.get(0).getValue();
		if(gstNo == null){
			SystemPreferences gstNoPref = systemPreferencesDao.findPreferenceByName("clinic_gst_number");
			gstNo = gstNoPref != null? gstNoPref.getValue() : null;
		}

		return gstNo;
	}
	private BigDecimal findFeeSplitForProvider(String billingProvider) {
        BigDecimal feeSplit;
        try {
			List<Property> props = propertyDao.findByNameAndProvider("fee_split", billingProvider);
			String feeSplitString = props.isEmpty()? null : props.get(0).getValue();
			feeSplit = feeSplitString == null ? new BigDecimal("100") : new BigDecimal(feeSplitString);
			feeSplit = feeSplit.doubleValue() > 100? new BigDecimal("100") : feeSplit.doubleValue() < 0 ?  new BigDecimal("0") : feeSplit;
		} catch (NumberFormatException nfe) {
			List<Property> props = propertyDao.findByNameAndProvider("fee_split", billingProvider);
			String feeSplitString = props.isEmpty()? null : props.get(0).getValue();
        	MiscUtils.getLogger().warn("Invalid 'fee_split' stored for providerNo: '" + billingProvider + "', value: '" + feeSplitString + "'", nfe);
        	feeSplit =  new BigDecimal("100") ;
		}

        return feeSplit;
    }

}
