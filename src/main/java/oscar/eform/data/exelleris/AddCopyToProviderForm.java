/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.eform.data.exelleris;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public final class AddCopyToProviderForm extends ActionForm {
//	String firstName;
//	String lastName;
//	String address1;
//	String city;
//	String country;
//	String postalCode;
//	String province;

	private Integer id;
	private String ontarioLifeLabsId;
	private String roverId;
	private String onProvincialGovtId;
	private String pathnetCode;
	private String salutation;
	private String lastName;
	private String firstName;
	private String middleName;
	private String address1;
	private String address2;
	private String city;
	private String stateOrProvince;
	private String zipOrPostal;
	private String country;
	private String region;
	private String specialty;
	private boolean deleted;
	private boolean visible;

	public AddCopyToProviderForm() {

	}


	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getAddress1() {
		return address1;
	}



	public void setAddress1(String address1) {
		this.address1 = address1;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOntarioLifeLabsId() {
		return ontarioLifeLabsId;
	}

	public void setOntarioLifeLabsId(String ontarioLifeLabsId) {
		this.ontarioLifeLabsId = ontarioLifeLabsId;
	}

	public String getRoverId() {
		return roverId;
	}

	public void setRoverId(String roverId) {
		this.roverId = roverId;
	}

	public String getOnProvincialGovtId() {
		return onProvincialGovtId;
	}

	public void setOnProvincialGovtId(String onProvincialGovtId) {
		this.onProvincialGovtId = onProvincialGovtId;
	}

	public String getPathnetCode() {
		return pathnetCode;
	}

	public void setPathnetCode(String pathnetCode) {
		this.pathnetCode = pathnetCode;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getStateOrProvince() {
		return stateOrProvince;
	}

	public void setStateOrProvince(String stateOrProvince) {
		this.stateOrProvince = stateOrProvince;
	}

	public String getZipOrPostal() {
		return zipOrPostal;
	}

	public void setZipOrPostal(String zipOrPostal) {
		this.zipOrPostal = zipOrPostal;
	}

	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}

	public String getSpecialty() {
		return specialty;
	}


	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public boolean isDeleted() {
		return deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isVisible() {
		return visible;
	}


	public void setVisible(boolean visible) {
		this.visible = visible;
	}


	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if (firstName == null || firstName.length() == 0)
			errors.add("firstName", new ActionMessage("Errors.Firstname"));
		if (lastName == null || lastName.length() == 0)
			errors.add("lastName", new ActionMessage("Errors.Lastname"));
		if (city == null || city.length() == 0)
			errors.add("city", new ActionMessage("Errors.City"));
		if (address1 == null || address1.length() == 0)
			errors.add("address", new ActionMessage("Errors.Address"));		
		if (stateOrProvince == null || stateOrProvince.length() == 0)
			errors.add("stateOrProvince", new ActionMessage("Errors.StateOrProvince"));
		if (zipOrPostal == null || zipOrPostal.length() == 0)
			errors.add("zipOrPostal", new ActionMessage("Errors.ZipOrPostal"));

		return errors;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		resetForm();
	}

	public void resetForm() {

		id = null;
		firstName = null;
		lastName = null;
		address1 = null;
		city = null;
		stateOrProvince = null;
		country = null;
		stateOrProvince = null;
		ontarioLifeLabsId = null;
		roverId = null;
		onProvincialGovtId = null;
		pathnetCode = null;
		salutation = null;
		middleName = null;
		address2 = null;
		region = null;
		specialty = null;
		deleted = false;
		visible = true;

	}

	
	
	
}
