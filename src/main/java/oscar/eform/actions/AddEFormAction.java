/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.eform.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hl7.fhir.instance.formats.JsonParser;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.EFormValueDao;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.dao.OrderLabTestCodeDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.EOrder;
import org.oscarehr.common.model.EOrder.OrderState;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.integration.excelleris.eorder.AuthenticationTokenAndResource;
import org.oscarehr.integration.excelleris.eorder.OrderGateway;
import org.oscarehr.integration.excelleris.eorder.OrderGatewayImpl;
import org.oscarehr.integration.excelleris.eorder.OrderResponseHelper;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Questionnaire;
import org.oscarehr.integration.excelleris.eorder.converter.EFormValueHelper;
import org.oscarehr.integration.excelleris.eorder.converter.QuestionnaireConverter;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.match.IMatchManager;
import org.oscarehr.match.MatchManager;
import org.oscarehr.match.MatchManagerException;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.eform.EFormLoader;
import oscar.eform.EFormUtil;
import oscar.eform.data.DatabaseAP;
import oscar.eform.data.EForm;
import oscar.oscarEncounter.data.EctProgram;
import oscar.util.StringUtils;


public class AddEFormAction extends Action {

	private static final Logger logger=MiscUtils.getLogger();
	
	private static final String EXCELLERIS_EORDER_AUTHENTICATION_TOKEN = "EXCELLERIS_EORDER_AUTHENTICATION_TOKEN";
	private static final String EXCELLERIS_PATIENT_ID = "Excelleris Patient Id";
	
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_eform", "w", null)) {
			throw new SecurityException("missing required security object (_eform)");
		}
		
		logger.debug("==================SAVING ==============");
		HttpSession se = request.getSession();

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		boolean fax = "true".equals(request.getParameter("faxEForm"));
		boolean print = "true".equals(request.getParameter("print"));
		boolean isExcellerisEOrder = "true".equals(request.getParameter("M_IsExcellerisEOrder"));
		
		logger.debug("isExcellerisEOrder : " + isExcellerisEOrder);

		@SuppressWarnings("unchecked")
		Enumeration<String> paramNamesE = request.getParameterNames();
		ArrayList<String> paramNames = new ArrayList<String>();  //holds "fieldname, ...."
		ArrayList<String> paramValues = new ArrayList<String>(); //holds "myval, ...."
		String fid = request.getParameter("efmfid");
		String demographic_no = request.getParameter("efmdemographic_no");
		String eform_link = request.getParameter("eform_link");
		String subject = request.getParameter("subject");
		boolean addDocumentToEchart = Boolean.parseBoolean(request.getParameter("printDocumentToEchart"));

		boolean doDatabaseUpdate = false;

		List<String> oscarUpdateFields = new ArrayList<String>();

		if (request.getParameter("_oscardodatabaseupdate") != null && request.getParameter("_oscardodatabaseupdate").equalsIgnoreCase("on"))
			doDatabaseUpdate = true;

		ActionMessages updateErrors = new ActionMessages();

		// The fields in the _oscarupdatefields parameter are separated by %s.
		if (!print && !fax && doDatabaseUpdate && request.getParameter("_oscarupdatefields") != null) {

			oscarUpdateFields = Arrays.asList(request.getParameter("_oscarupdatefields").split("%"));

			boolean validationError = false;

			for (String field : oscarUpdateFields) {
				EFormLoader.getInstance();
				// Check for existence of appropriate databaseap
				DatabaseAP currentAP = EFormLoader.getAP(field);
				if (currentAP != null) {
					if (!currentAP.isInputField()) {
						// Abort! This field can't be updated
						updateErrors.add(field, new ActionMessage("errors.richeForms.noInputMethodError", field));
						validationError = true;
					}
				} else {
					// Field doesn't exit
					updateErrors.add(field, new ActionMessage("errors.richeForms.noSuchFieldError", field));
					validationError = true;
				}
			}

			if (!validationError) {
				for (String field : oscarUpdateFields) {
					EFormLoader.getInstance();
					DatabaseAP currentAP = EFormLoader.getAP(field);
					// We can add more of these later...
					if (currentAP != null) {
						String inSQL = currentAP.getApInSQL();

						inSQL = DatabaseAP.parserReplace("demographic", demographic_no, inSQL);
						inSQL = DatabaseAP.parserReplace("provider", providerNo, inSQL);
						inSQL = DatabaseAP.parserReplace("fid", fid, inSQL);

						inSQL = DatabaseAP.parserReplace("value", request.getParameter(field), inSQL);

						//if(currentAP.getArchive() != null && currentAP.getArchive().equals("demographic")) {
						//	demographicArchiveDao.archiveRecord(demographicManager.getDemographic(loggedInInfo,demographic_no));
						//}

						// Run the SQL query against the database
						//TODO: do this a different way.
						MiscUtils.getLogger().error("Error",new Exception("EForm is using disabled functionality for updating fields..update not performed"));
					}
				}
			}
		}

		if (subject == null) subject="";
		String curField = "";
		while (paramNamesE.hasMoreElements()) {
			curField = paramNamesE.nextElement();
			if( curField.equalsIgnoreCase("parentAjaxId"))
				continue;
			if(request.getParameter(curField) != null && (!request.getParameter(curField).trim().equals("")) )
			{
                if (curField.equals("faxRecipients") || curField.equals("faxRecipientsName")) {
                    // faxRecipients may have multiple values so save them all
                    String[] values = request.getParameterValues(curField);
                    for (String value : values) {
                        paramNames.add(curField);
                        paramValues.add(value);
                    }
                } else {
                    paramNames.add(curField);
                    paramValues.add(request.getParameter(curField));
                }
			}
			
		}


		EForm curForm = new EForm(fid, demographic_no, providerNo);

		//add eform_link value from session attribute
		ArrayList<String> openerNames = curForm.getOpenerNames();
		ArrayList<String> openerValues = new ArrayList<String>();
		for (String name : openerNames) {
			String lnk = providerNo+"_"+demographic_no+"_"+fid+"_"+name;
			String val = (String)se.getAttribute(lnk);
			openerValues.add(val);
			if (val!=null) se.removeAttribute(lnk);
		}

		//----names parsed
		ActionMessages errors = curForm.setMeasurements(paramNames, paramValues);
		curForm.setFormSubject(subject);
		curForm.setValues(paramNames, paramValues);
		if (!openerNames.isEmpty()) curForm.setOpenerValues(openerNames, openerValues);
		if (eform_link!=null) curForm.setEformLink(eform_link);
		curForm.setImagePath();
		curForm.setAction();
		curForm.setNowDateTime();
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			request.setAttribute("curform", curForm);
			request.setAttribute("page_errors", "true");
			return mapping.getInputForward();
		}

		//Check if eform same as previous, if same -> not saved
		String prev_fdid = (String)se.getAttribute("eform_data_id");

		se.removeAttribute("eform_data_id");
		boolean sameform = false;

		if (isExcellerisEOrder) {
			if (!StringUtils.isNullOrEmpty(request.getParameter("M_existingFdid"))) {
				prev_fdid = (String) request.getParameter("M_existingFdid");
				sameform = true;
			}
		} else if (StringUtils.filled(prev_fdid)) {
			EForm prevForm = new EForm(prev_fdid);
			if (prevForm!=null) {
				sameform = curForm.getFormHtml().equals(prevForm.getFormHtml());
			}

		}

		String fdid = null;
		if (!sameform) { //save eform data
			EFormDataDao eFormDataDao=(EFormDataDao)SpringUtils.getBean("EFormDataDao");
			EFormData eFormData=toEFormData(curForm);
			
			String appointmentNo = request.getParameter("appointment_no");
			// Checks if the appointment number parameter either doesn't exist or is passed as a string of null
			if (appointmentNo != null && !appointmentNo.equals("null")) {
				try {
					// Sets the appointment number to the eform data after parsing it
					eFormData.setAppointmentNo(Integer.parseInt(appointmentNo));
				} catch (NumberFormatException e) {
					logger.error("Could not convert appointment number to an integer. Appointment number: " + appointmentNo);
				}
			}
			
			eFormDataDao.persist(eFormData);

			
			fdid = eFormData.getId().toString();

			EFormUtil.addEFormValues(paramNames, paramValues, new Integer(fdid), new Integer(fid), new Integer(demographic_no)); //adds parsed values

			//post fdid to {eform_link} attribute
			if (eform_link!=null) {
				se.setAttribute(eform_link, fdid);
			}

			if (addDocumentToEchart) {
				String eformPrintUrl = PrintAction.getEformRequestUrl(request) + fdid;
				EFormUtil.printEformToEchart(loggedInInfo, eFormData, eformPrintUrl);
			}
			
			SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
			SystemPreferences systemPreferences = systemPreferencesDao.findPreferenceByName("patient_intake_eform");
			if (systemPreferences != null && systemPreferences.getValue() != null && systemPreferences.getValue().equals(fid)) {
				EFormUtil.createPatientIntakeLetter(eFormData);
			}
			
			// Handle Excelleris eOrder
			if (isExcellerisEOrder) {
				ActionForward actionForward = handleExcellerisEOrder(
						mapping, form, request, response, curForm, fdid, providerNo, demographic_no, eFormData);
				if (actionForward != null) {
					return actionForward;
				}
			}
			
			if (fax) {
				request.setAttribute("fdid", fdid);
				return(mapping.findForward("fax"));
			}
			
			else if (print) {
				request.setAttribute("fdid", fdid);
				return(mapping.findForward("print"));
			}

			else {
				//write template message to echart
				String program_no = new EctProgram(se).getProgram(providerNo);
				String path = request.getRequestURL().toString();
				String uri = request.getRequestURI();
				path = path.substring(0, path.indexOf(uri));
				path += request.getContextPath();
	
				EFormUtil.writeEformTemplate(LoggedInInfo.getLoggedInInfoFromSession(request),paramNames, paramValues, curForm, fdid, program_no, path);
			}
			
		}
		else {

			if (isExcellerisEOrder && !StringUtils.isNullOrEmpty(request.getParameter("M_existingFdid"))) {
				EFormDataDao eFormDataDao = (EFormDataDao) SpringUtils.getBean("EFormDataDao");
				Integer existingFdid = Integer.parseInt(request.getParameter("M_existingFdid"));
				EFormData existingEformData = eFormDataDao.findByFormDataId(existingFdid);

				EFormData savedEformData = updateEFormData(curForm, existingEformData, eFormDataDao, existingFdid, fid,
						demographic_no, paramNames, paramValues);
				request.setAttribute("fdid", savedEformData.getId().toString());

				ActionForward actionForward = handleExcellerisEOrder(mapping, form, request, response, curForm,
						request.getParameter("M_existingFdid"), providerNo, demographic_no, savedEformData);
				if (actionForward != null) {
					return actionForward;
				}
			}

			logger.debug("Warning! Form HTML exactly the same, new form data not saved.");
			if (fax) {
				request.setAttribute("fdid", prev_fdid);
				return(mapping.findForward("fax"));
			}
			
			else if (print) {
				logger.debug("Print.....");
				request.setAttribute("fdid", prev_fdid);
				return(mapping.findForward("print"));
			}
		}
		
		if (demographic_no != null) {
			IMatchManager matchManager = new MatchManager();
			DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
			Demographic client = demographicManager.getDemographic(loggedInInfo,demographic_no);
			try {
	            matchManager.<Demographic>processEvent(client, IMatchManager.Event.CLIENT_CREATED);
            } catch (MatchManagerException e) {
            	MiscUtils.getLogger().error("Error while processing MatchManager.processEvent(Client)",e);
            }
		}
		
		if (isExcellerisEOrder) {
			ActionMessages messages = getMessages(request);
			if (messages == null) {
				messages = new ActionMessages();
			}
			ActionMessage message = new ActionMessage("eform.errors.submit_eorder.success");
			messages.add(ActionMessages.GLOBAL_MESSAGE, message);					
			saveMessages(request, messages);
			request.setAttribute("curform", curForm);
			request.setAttribute("page_messages", "true");
			request.setAttribute("fdid", (fdid == null) ? prev_fdid : fdid);
			
			String str = Globals.MESSAGE_KEY;
			return mapping.getInputForward();
		}
		
		return(mapping.findForward("close"));
	}

	private EFormData toEFormData(EForm eForm) {
		EFormData eFormData=new EFormData();
		eFormData.setFormId(Integer.parseInt(eForm.getFid()));
		eFormData.setFormName(eForm.getFormName());
		eFormData.setSubject(eForm.getFormSubject());
		eFormData.setDemographicId(Integer.parseInt(eForm.getDemographicNo()));
		eFormData.setCurrent(true);
		eFormData.setFormDate(new Date());
		eFormData.setFormTime(eFormData.getFormDate());
		eFormData.setProviderNo(eForm.getProviderNo());
		eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" checked=\"checked\"", ""));
		eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" value=\"X\"", ""));
		eFormData.setFormData(eForm.getFormHtml());
		eFormData.setShowLatestFormOnly(eForm.isShowLatestFormOnly());
		eFormData.setPatientIndependent(eForm.isPatientIndependent());
		eFormData.setRoleType(eForm.getRoleType());

		return(eFormData);
	}
	
	private EFormData updateEFormData(EForm eForm, EFormData eFormData, EFormDataDao eFormDataDao, Integer fdid,
			String fid, String demographic_no, ArrayList<String> paramNames, ArrayList<String> paramValues) {
//		EFormData eFormData=new EFormData();
		eFormData.setFormId(Integer.parseInt(eForm.getFid()));
		eFormData.setFormName(eForm.getFormName());
		eFormData.setSubject(eForm.getFormSubject());
		eFormData.setDemographicId(Integer.parseInt(eForm.getDemographicNo()));
		eFormData.setCurrent(true);
		eFormData.setFormDate(new Date());
		eFormData.setFormTime(eFormData.getFormDate());
		eFormData.setProviderNo(eForm.getProviderNo());
		eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" checked=\"checked\"", ""));
		eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" value=\"X\"", ""));
		eFormData.setFormData(eForm.getFormHtml());
		eFormData.setShowLatestFormOnly(eForm.isShowLatestFormOnly());
		eFormData.setPatientIndependent(eForm.isPatientIndependent());
		eFormData.setRoleType(eForm.getRoleType());

		EFormData savedEformData = eFormDataDao.saveEntity(eFormData);

		EFormValueDao eFormValueDao = (EFormValueDao) SpringUtils.getBean("EFormValueDao");

		List<EFormValue> oldEformValues = eFormValueDao.findByFormDataId(fdid);
		Map<String, EFormValue> oldValuesMap = EFormValueHelper.getValueMap(oldEformValues);

		// adds parsed values

		ArrayList<String> paramNamesToAdd = new ArrayList<String>();
		ArrayList<String> paramValuesToAdd = new ArrayList<String>();
		for (int i = 0; i <= paramNames.size() - 1; i++) {
			EFormValue value = oldValuesMap.get(paramNames.get(i));
			if (value != null) {
				value.setVarValue(paramValues.get(i));
				value.setDemographicId(new Integer(demographic_no));
				eFormValueDao.merge(value);
			} else {
				paramNamesToAdd.add(paramNames.get(i));
				paramValuesToAdd.add(paramValues.get(i));
			}

		}

		EFormUtil.addEFormValues(paramNamesToAdd, paramValuesToAdd, fdid, new Integer(fid),
				new Integer(demographic_no));

		for (int i = 0; i <= oldEformValues.size() - 1; i++) {
			EFormValue oldValue = oldEformValues.get(i);
			if (!paramNames.contains(oldValue.getVarName())) {
				oldValue.setVarValue(null);
				oldValue.setDemographicId(null);
				eFormValueDao.merge(oldValue);
			}
		}

		return savedEformData;
	}

	/**
	 * Handle Excelleris EOrder
	 * 
	 * @param mapping  action mapping
	 * @param form  action form
	 * @param request  servlet request
	 * @param response servlet response
	 * @param curForm  current eForm
	 * @param fdid eform data id
	 * @param providerNo provider id
	 * @param demographicNo patient id
	 * @param eFormData eform data
	 * 
	 * @return  action forward or null
	 */
	private ActionForward handleExcellerisEOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response, EForm curForm, String fdid, String providerNo, String demographicNo, EFormData eFormData) {
		logger.debug("Excelleris eOrder enabled eform.");
		try {

			String existingFdid = (String) request.getParameter("M_existingFdid");
			EOrderDao eOrderDao=(EOrderDao)SpringUtils.getBean("EOrderDao");
			EOrder eOrder;
			Integer fdidInteger;
			if (!StringUtils.isNullOrEmpty(existingFdid)) {
				eOrder = eOrderDao.findByExtEFormDataId(Integer.parseInt(existingFdid));
				fdidInteger = new Integer(existingFdid);
			} else {
				// create eOrder in OSCAR database
				eOrder = new EOrder();
				fdidInteger = new Integer(fdid);
				eOrder.setEFormDataId(new Integer(fdidInteger));
				eOrder.setState(OrderState.NEW);
				eOrderDao.persist(eOrder);
			}
			// get eform values for the current eform data
			EFormValueDao eformValueDao = (EFormValueDao)SpringUtils.getBean("EFormValueDao");
			List<EFormValue> eformValues = eformValueDao.findByFormDataId(fdidInteger);					
						
			// invoke service to submit order to Excelleris eOrder service
			OrderGateway orderGateway = (OrderGateway)SpringUtils.getBean(OrderGatewayImpl.class);
			AuthenticationTokenAndResource authenticationTokenAndParams = orderGateway.createOrder(null, eFormData, eformValues);
			logger.debug("authenticationTokenAndParams: {} " + authenticationTokenAndParams);
			
			if (authenticationTokenAndParams == null) {
				logger.debug("authenticationTokenAndParams is null.");
				ActionMessages submitOrderErrors = new ActionMessages();
				ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", "Received null response when submittion eorder.");
				submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
				saveErrors(request, submitOrderErrors);
				request.setAttribute("curform", curForm);
				request.setAttribute("page_errors", "true");
				return mapping.getInputForward();
			}
						
			// get pdf lab requisition form from response and save with the eorder
			OrderResponseHelper orHelper = new OrderResponseHelper(authenticationTokenAndParams.getResource());
			if (orHelper.getErrors() != null && !orHelper.getErrors().isEmpty()) {
				logger.debug("Response has error(s).");
				ActionMessages submitOrderErrors = new ActionMessages();
				for (String errorText : orHelper.getErrors()) {
					ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", errorText);
					submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
				}
				saveErrors(request, submitOrderErrors);
				request.setAttribute("curform", curForm);
				request.setAttribute("page_errors", "true");
				return mapping.getInputForward();
			}
			
			// handle informational messages from Excelleris eOrder
			
			if (orHelper.getInformationalMessages() != null && !orHelper.getInformationalMessages().isEmpty()) {
				logger.debug("Response has informational message(s).");
				ActionMessages submitOrderInfoMsgs = new ActionMessages();
				for (String msgText : orHelper.getInformationalMessages()) {
					ActionMessage message = new ActionMessage("eform.errors.submit_eorder.info_msg", msgText);
					submitOrderInfoMsgs.add(ActionMessages.GLOBAL_MESSAGE, message);
				}
				saveMessages(request, submitOrderInfoMsgs);
			}
			
			// TODO: add logic to handle questionnaire, for now, just display an error
			if (orHelper.getQuestionnaire() != null) {
				logger.debug("Response has Questionnaire.");

				// save the questionnaire as part of the eOrder
				JsonParser parser = new JsonParser();
				try {
					String defn = parser.composeString(orHelper.getQuestionnaire());
					EOrder savedEorder = eOrderDao.findByExtEFormDataId(new Integer(fdidInteger));
					savedEorder.setQuestionnaire(defn);
					eOrderDao.saveEntity(savedEorder);
				} catch (Exception e) {
					logger.error("Error converting Parameters to JSON.", e);
					throw new ServiceException("Error converting Parameters to JSON.");
				}

				// Convert to api questionnaire
				OrderLabTestCodeDao orderLabTestCodeDao = (OrderLabTestCodeDao) SpringUtils
						.getBean(OrderLabTestCodeDao.class);
				Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

				Questionnaire questionnaire = QuestionnaireConverter.toApiObject(orHelper.getQuestionnaire(), valueMap,
						orderLabTestCodeDao);

				request.setAttribute("questionnaire", questionnaire);
				request.setAttribute("M_existingFdid", fdid);
				// set eorder id
				request.setAttribute("curform", curForm);
				return mapping.getInputForward();
			}

			String pdfForm = orHelper.getPdfLabRequisitionForm();
			String orderId = orHelper.getOrderId();		
			
			// update eorder with pdf lab requisition and external order id
			EOrder savedEOrder = eOrderDao.findByExtEFormDataId(new Integer(fdid));
			logger.debug("Saved EOrder id : " + savedEOrder.getId());
			savedEOrder.setGeneratedPDF(pdfForm);
			savedEOrder.setExternalOrderId(orderId);
			savedEOrder.setState(OrderState.SUBMITTED);
			eOrderDao.merge(savedEOrder);
			
			// save excelleris patient id
			String patientId = orHelper.getPatientId();
			DemographicExtDao demographicExtDao = (DemographicExtDao)SpringUtils.getBean(DemographicExtDao.class);
			
			String excellerisPatientIdInDB = demographicExtDao.getValueForDemoKey(new Integer(demographicNo), EXCELLERIS_PATIENT_ID);
			if (excellerisPatientIdInDB == null) {
				DemographicExt demographicExt = new DemographicExt(
						providerNo, new Integer(demographicNo), EXCELLERIS_PATIENT_ID, patientId);
				demographicExtDao.persist(demographicExt);
			}
			
		} catch (Exception ex) {
			logger.error("Error submitting error", ex);
			ActionMessages submitOrderErrors = new ActionMessages();
			String errorMsg = ex.getMessage();
			ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", errorMsg);
			submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);					
			saveErrors(request, submitOrderErrors);
			request.setAttribute("curform", curForm);
			request.setAttribute("page_errors", "true");
			return mapping.getInputForward();
		}
		
		return null;
	}
}
