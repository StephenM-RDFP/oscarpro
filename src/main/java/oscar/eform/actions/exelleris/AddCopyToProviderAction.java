/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.eform.actions.exelleris;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.ExcellerisCopyToProviderDao;
import org.oscarehr.common.model.ExcellerisCopyToProvider;
import org.oscarehr.util.SpringUtils;

import oscar.eform.data.exelleris.AddCopyToProviderForm;

public class AddCopyToProviderAction extends Action {


	private ExcellerisCopyToProviderDao excellerisCopyToProviderDao = (ExcellerisCopyToProviderDao) SpringUtils
			.getBean("excellerisCopyToProviderDao");

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		
		ExcellerisCopyToProvider copyToProvider = null;
		AddCopyToProviderForm addCopyToProviderForm = (AddCopyToProviderForm) form;

		copyToProvider = new ExcellerisCopyToProvider();
		populateFields(copyToProvider, addCopyToProviderForm);

		excellerisCopyToProviderDao.persist(copyToProvider);

		String added = "" + copyToProvider.getFirstName() + " " + copyToProvider.getLastName();
		request.setAttribute("Added", added);
		request.setAttribute("copyToProvider", copyToProvider);
		return mapping.getInputForward();
	}



	private void populateFields(ExcellerisCopyToProvider copyToProvider, AddCopyToProviderForm addCopyToProviderForm) {
		copyToProvider.setFirstName(addCopyToProviderForm.getFirstName());
		copyToProvider.setLastName(addCopyToProviderForm.getLastName());
		copyToProvider.setAddress1(addCopyToProviderForm.getAddress1());
		copyToProvider.setCity(addCopyToProviderForm.getCity());
		String stateOrProvince = addCopyToProviderForm.getStateOrProvince();
		copyToProvider.setStateOrProvince(stateOrProvince);
		String country = stateOrProvince.startsWith("US") ? "US" : "CA";
		copyToProvider.setCountry(country);
		copyToProvider.setZipOrPostal(addCopyToProviderForm.getZipOrPostal());
		copyToProvider.setDeleted(false);
		copyToProvider.setVisible(true);
	}
}
