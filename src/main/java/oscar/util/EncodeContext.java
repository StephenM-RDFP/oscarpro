package oscar.util;

import org.owasp.encoder.Encode;

/**
 * Enum for passing a desired encoding to a method that returns a string value in different contexts
 * In java 8 this could be replaced with lambda functions
 */
public enum EncodeContext {
    FOR_HTML,
    FOR_HTML_CONTENT,
    FOR_HTML_ATTRIBUTE,
    FOR_CSS_STRING,
    FOR_CSS_URL,
    FOR_JAVASCRIPT,
    FOR_JAVASCRIPT_BLOCK,
    FOR_JAVASCRIPT_SOURCE,
    FOR_JAVASCRIPT_ATTRIBUTE,
    FOR_URI_COMPONENT;

    /**
     * Encodes the provided string using the corresponding Encoder method
     * @param stringToEncode The string to encode
     * @return an encoded string with the provided encoding
     */
    public String encode(String stringToEncode) {
        switch(this) {
            case FOR_HTML:
                return Encode.forHtml(stringToEncode);
            case FOR_HTML_CONTENT:
                return Encode.forHtmlContent(stringToEncode);
            case FOR_HTML_ATTRIBUTE:
                return Encode.forHtmlAttribute(stringToEncode);
            case FOR_CSS_STRING:
                return Encode.forCssString(stringToEncode);
            case FOR_CSS_URL:
                return Encode.forCssUrl(stringToEncode);
            case FOR_JAVASCRIPT:
                return Encode.forJavaScript(stringToEncode);
            case FOR_JAVASCRIPT_BLOCK:
                return Encode.forJavaScriptBlock(stringToEncode);
            case FOR_JAVASCRIPT_SOURCE:
                return Encode.forJavaScriptSource(stringToEncode);
            case FOR_JAVASCRIPT_ATTRIBUTE:
                return Encode.forJavaScriptAttribute(stringToEncode);
            case FOR_URI_COMPONENT:
                return Encode.forUriComponent(stringToEncode);
            default:
                return stringToEncode;
        }
    }
}
