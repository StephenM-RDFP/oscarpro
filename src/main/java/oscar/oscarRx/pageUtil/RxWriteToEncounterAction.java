package oscar.oscarRx.pageUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.PMmodule.service.ProgramManager;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.dao.CaseManagementTmpSaveDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.CaseManagementTmpSave;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import oscar.OscarProperties;
import oscar.oscarEncounter.data.EctProgram;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RxWriteToEncounterAction extends Action {
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    
    private static OscarProperties oscarProperties = OscarProperties.getInstance();
    private static SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    private oscar.oscarRx.pageUtil.RxSessionBean rxSessionBean = null;


    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        checkPrivilege(loggedInInfo, "w");
        
        HttpSession session = request.getSession();
        rxSessionBean = (oscar.oscarRx.pageUtil.RxSessionBean) session.getAttribute("RxSessionBean");
        if (rxSessionBean == null) {
            response.sendRedirect("error.html");
            return null;
        }
        String demographicNo = String.valueOf(rxSessionBean.getDemographicNo());
        String programNo = new EctProgram(session).getProgram(session.getAttribute("user").toString());
        

        
        CaseManagementManager caseManagementMgr = SpringUtils.getBean(CaseManagementManager.class);
        CaseManagementTmpSaveDao caseManagementTmpSaveDao = SpringUtils.getBean(CaseManagementTmpSaveDao.class);
        CaseManagementNote note = getLastSaved(request, demographicNo, loggedInInfo.getLoggedInProviderNo(), caseManagementMgr);
        CaseManagementTmpSave tmpSave = caseManagementMgr.getTmpSave(loggedInInfo.getLoggedInProviderNo(), demographicNo, programNo);
        Date today = new Date();
        if (tmpSave != null) {
            String noteBody = generateNote(loggedInInfo, request.getParameter("body"), request.getParameter("additionalNotes"), request.getParameter("prefPharmacy"), false);
            
            if (tmpSave.getNoteId() > 0) {
                note = caseManagementMgr.getNote(String.valueOf(tmpSave.getNoteId()));
                if (note.getUpdate_date().after(tmpSave.getUpdateDate())) {
                    note.setNote(tmpSave.getNote() + "\n" + noteBody);
                    note.setUpdate_date(today);
                    caseManagementMgr.saveNoteSimple(note);
                } else {
                    createAndSaveNewNote(loggedInInfo, demographicNo, programNo, caseManagementMgr, today, tmpSave.getNote() + "\n" + noteBody);
                }
            } else {
                createAndSaveNewNote(loggedInInfo, demographicNo, programNo, caseManagementMgr, today, tmpSave.getNote() + "\n" + noteBody);
            }
            caseManagementTmpSaveDao.remove(tmpSave.getProviderNo(), tmpSave.getDemographicNo(), tmpSave.getProgramId());
        } else if (note != null) {
            String noteBody = generateNote(loggedInInfo, request.getParameter("body"), request.getParameter("additionalNotes"), request.getParameter("prefPharmacy"), false);
            note.setNote(note.getNote() + "\n" + noteBody);
            note.setUpdate_date(today);
            caseManagementMgr.saveNoteSimple(note);
        } else {
            String noteBody = generateNote(loggedInInfo, request.getParameter("body"), request.getParameter("additionalNotes"), request.getParameter("prefPharmacy"), true);
            createAndSaveNewNote(loggedInInfo, demographicNo, programNo, caseManagementMgr, today, noteBody);
        }
        
        
        return null;
    }
    
    private String generateNote(LoggedInInfo loggedInInfo, String noteBody, String additionalNotes, String prefPharmacy, boolean addDateString) {
        

        GregorianCalendar now=new GregorianCalendar();
        int curYear = now.get(Calendar.YEAR);
        int curMonth = (now.get(Calendar.MONTH)+1);
        int curDay = now.get(Calendar.DAY_OF_MONTH);
        String dateString = "["+curYear+"-"+curMonth+"-"+curDay+" .:]\n\n";

        Boolean pasteAsterisks = oscarProperties.isPropertyActive("rx_paste_asterisk");

        List<SystemPreferences> rxSystemPreferences = systemPreferencesDao.findPreferencesByNames(SystemPreferences.RX_PREFERENCE_KEYS);
        HashMap<String, Boolean> rxPreferences = new HashMap<String, Boolean>();
        for (SystemPreferences preference : rxSystemPreferences) {
            rxPreferences.put(preference.getName(), Boolean.parseBoolean(preference.getValue()));
        }
        
        String note = addDateString ? dateString : "";
        if (pasteAsterisks) {
            note += "**********************************************************************************\n"; 
        }
        
        if (rxPreferences.getOrDefault("rx_paste_provider_to_echart", false)) {
            note += "Prescribed and printed by "+ Encode.forJavaScript(loggedInInfo.getLoggedInProvider().getFormattedName())+"\n";
        }
        
        note += noteBody;
        
        if (StringUtils.isNotEmpty(additionalNotes)) {
            note += additionalNotes + "\n";
        }

        if (pasteAsterisks) {
            if (StringUtils.isNotEmpty(prefPharmacy)) {
                note += prefPharmacy + "\n";
            }
            
            note += "****" + Encode.forJavaScript(oscar.oscarProvider.data.ProviderData.getProviderName(rxSessionBean.getProviderNo())) + "******************************************************************************\n";
        }
        
        return note;
    }

    private void checkPrivilege(LoggedInInfo loggedInInfo, String privilege) {
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_rx", privilege, null)) {
            throw new RuntimeException("missing required security object (_rx)");
        }
    }
    
    public CaseManagementNote getLastSaved(HttpServletRequest request, String demono, String providerNo, CaseManagementManager caseManagementMgr) {
        HttpSession session = request.getSession();
        String programId = (String) session.getAttribute("case_program_id");
        Map unlockedNotesMap = this.getUnlockedNotesMap(request);
        return caseManagementMgr.getLastSaved(programId, demono, providerNo, unlockedNotesMap);
    }

    protected Map getUnlockedNotesMap(HttpServletRequest request) {
        Map<Long, Boolean> map = (Map<Long, Boolean>) request.getSession().getAttribute("unlockedNoteMap");
        if (map == null) {
            map = new HashMap<Long, Boolean>();
        }
        return map;
    }

    private void createAndSaveNewNote(LoggedInInfo loggedInInfo, String demographicNo, String programNo, CaseManagementManager caseManagementMgr, Date today, String noteBody) {
        CaseManagementNote note;
        note = new CaseManagementNote();
        note.setObservation_date(today);
        note.setCreate_date(today);
        note.setDemographic_no(demographicNo);
        note.setProvider(loggedInInfo.getLoggedInProvider());
        note.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        note.setSigned(false);
        note.setSigning_provider_no("");
        note.setProgram_no(programNo);
        note.setNote(noteBody);
        note.setIncludeissue(false);
        ProgramManager programManager = SpringUtils.getBean(ProgramManager.class);
        String role;
        try {
            role = String.valueOf((programManager.getProgramProvider(note.getProviderNo(), note.getProgram_no())).getRole().getId());
        } catch (Exception e) {
            role = "0";
        }
        note.setReporter_caisi_role(role);
        note.setReporter_program_team("0");
        note.setPassword(null);
        note.setLocked(false);
        note.setHistory(noteBody);
        note.setUpdate_date(today);
        caseManagementMgr.saveNoteSimple(note);
    }

}
