/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.oscarLab.tld;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.InboxResultsRepository;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.inbox.InboxResponse;
import org.oscarehr.common.model.inbox.OscarInboxQueryParameters;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarMDS.data.CategoryData;

import java.sql.SQLException;

/**
 *
 * @author Jay Gallagher
 */
public class LabTag extends TagSupport {

	private String providerNo;
	private int numNewLabs;
	
	public LabTag() {
		numNewLabs = 0;
	}

	private static Logger logger=MiscUtils.getLogger();

	public int doStartTag() throws JspException {
		InboxResultsRepository inboxResultsRepository = SpringUtils.getBean(InboxResultsRepository.class);
		ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        boolean documentShowDescription = systemPreferencesDao.isPreferenceValueEquals("document_discipline_column_display", "document_description");

		Provider loggedInProvider = providerDao.getProvider(providerNo);
		OscarInboxQueryParameters inboxQueryParameters = new OscarInboxQueryParameters(loggedInProvider);
		inboxQueryParameters.whereProviderNumber(loggedInProvider.getProviderNo())
				.whereStatus("N").whereAbnormalStatus("L")
				.whereSortBy("dateTime").whereSortOrder("descending")
				.wherePage(0).whereResultsPerPage(0)
				.whereShowDocuments(true).whereShowLabs(true).whereShowHrm(true).whereGetCounts(true).whereGetDemographicCounts(false)
                .whereDocumentShowDescription(documentShowDescription);

		InboxResponse inboxResponseResults = inboxResultsRepository.getInboxItems(inboxQueryParameters);

		numNewLabs = inboxResponseResults.getDocumentCount() + inboxResponseResults.getLabCount() + inboxResponseResults.getHrmCount();
		try {
			JspWriter out = super.pageContext.getOut();
			if (numNewLabs > 0) {
				out.print("<span class='tabalert'>  ");
			}
			else out.print("<span>  ");
		} catch (Exception p) {
			MiscUtils.getLogger().error("Error", p);
		}
		return (EVAL_BODY_INCLUDE);
	}

	public void setProviderNo(String providerNo1) {
		providerNo = providerNo1;
	}

	public String getProviderNo() {
		return providerNo;
	}

	public int doEndTag() throws JspException {
		try {
			JspWriter out = super.pageContext.getOut();
			//ronnie 2007-5-4
			if (numNewLabs > 0) {
				out.print("<sup>" + numNewLabs + "</sup></span>");
			}
			else out.print("</span>");
		} catch (Exception p) {
			MiscUtils.getLogger().error("Error", p);
		}
		return EVAL_PAGE;
	}
}
