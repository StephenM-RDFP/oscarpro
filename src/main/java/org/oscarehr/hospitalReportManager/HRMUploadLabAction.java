/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.hospitalReportManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;
import org.oscarehr.util.LoggedInInfo;

import oscar.oscarLab.ca.all.pageUtil.LabUploadForm;
import oscar.oscarLab.ca.all.util.Utilities;

import java.io.IOException;

public class HRMUploadLabAction extends DispatchAction {

    Logger logger = Logger.getLogger(HRMUploadLabAction.class);
    
	@Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

		LabUploadForm frm = (LabUploadForm) form;
        FormFile importFile = frm.getImportFile();
        boolean isJsonUpload = Boolean.parseBoolean(request.getParameter("jsonUpload"));
        boolean success = true;
        
        try {
	        String filePath = Utilities.saveFile(importFile.getInputStream(), importFile.getFileName());
	        
	        HRMReport report = HRMReportParser.parseReport(loggedInInfo,filePath);
	        if (report != null) {
	            HRMReportParser.addReportToInbox(loggedInInfo,report);
            }
	        
        } catch (Exception e) {
            logger.error("Couldn't handle uploaded HRM lab", e);
            success = false;
        } 
        
        if (isJsonUpload) {
            try {
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("uploadSuccess", success);
                jsonResponse.put("fileName", importFile.getFileName());

                response.getOutputStream().write(jsonResponse.toString().getBytes());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
            } catch (IOException e) { logger.error(e); }
            return null;
        } else {
            request.setAttribute("success", success);
            return mapping.findForward("success");
        }
	}
	
}
