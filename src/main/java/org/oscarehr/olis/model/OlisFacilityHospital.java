package org.oscarehr.olis.model;

public interface OlisFacilityHospital {
    Integer getId();
    String getName();
    String getAddressLine1();
    String getAddressLine2();
    String getCity();
    String getProvince();
    String getPostalCode();
    String getOid();
    String getFullId();
}
