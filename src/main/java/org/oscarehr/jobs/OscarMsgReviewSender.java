/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oscarehr.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.oscarehr.casemgmt.dao.CaseManagementNoteDAO;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.dao.ProviderDataDao;
import org.oscarehr.common.dao.ResidentOscarMsgDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.jobs.OscarRunnable;
import org.oscarehr.common.model.OscarMsgType;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderData;
import org.oscarehr.common.model.ResidentOscarMsg;
import org.oscarehr.common.model.Security;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarMessenger.data.MsgMessageData;
import oscar.oscarMessenger.data.MsgProviderData;
import oscar.oscarMessenger.util.MsgDemoMap;

/**
 *
 * @author rjonasz
 */
public class OscarMsgReviewSender implements OscarRunnable {
    
    private Provider provider = null;
    private Security security = null;
    private static final String MESSAGE = "Hello, the following charts require your attention";
    private static final String SUBJECT = "Chart Review";
    private static final Calendar DEFAULT_TIME = new GregorianCalendar(0, 0, 0, 9, 0);
    private final Logger logger = MiscUtils.getLogger();
    
    @Override
    public void run() {
        ProviderDataDao providerDataDao = SpringUtils.getBean(ProviderDataDao.class);
        UserPropertyDAO propertyDao = SpringUtils.getBean(UserPropertyDAO.class);
        ResidentOscarMsgDao residentOscarMsgDao = SpringUtils.getBean(ResidentOscarMsgDao.class);
        CaseManagementNoteDAO caseManagementNoteDao = SpringUtils.getBean(CaseManagementNoteDAO.class);
        
        String userNo = provider.getProviderNo();
        String userName = "System";
        
        logger.info("Starting to send OSCAR Review Messages");
        Integer defaultHour = DEFAULT_TIME.get(Calendar.HOUR_OF_DAY);
        Integer defaultMin = DEFAULT_TIME.get(Calendar.MINUTE);
        
        // get current hour and minute
        Calendar now = GregorianCalendar.getInstance();
        Integer currentHour = now.get(Calendar.HOUR_OF_DAY);
        Integer currentMinute = now.get(Calendar.MINUTE);
        // 'round' the current hour and minute to the nearest half hour
        currentHour = (currentMinute > 45 ? currentHour + 1 : currentHour);
        currentMinute = (currentMinute > 45 || currentMinute < 15 ? 0 : 30);
        // get half hour in 24 hour string time format, no leading zeros on hour
        String thisHalfHour = String.format("%d:%02d", currentHour, currentMinute);
        
        List<ProviderData> providerList = providerDataDao.findAllBilling("1");
        List<String> providerNosList = new ArrayList<String>();
        for( ProviderData p : providerList ) {
            providerNosList.add(p.getId());
        }
        
        List<UserProperty> properties = propertyDao.getPropValues(UserProperty.OSCAR_MSG_RECVD, thisHalfHour);
        for (UserProperty p : properties) {
            
            if (providerNosList.contains(p.getProviderNo())) {

                List<ResidentOscarMsg> residentOscarMsgList = residentOscarMsgDao.findBySupervisor(p.getProviderNo());
                String typeLinkString = createMessageTypeLinkString(residentOscarMsgList);
                providerNosList.remove(p.getProviderNo());
                
                if (residentOscarMsgList.size() > 0) {
                    Integer messageId = createAndSendMessage(p.getProviderNo(), userNo, userName, typeLinkString);
                    logger.info("SENT Review OSCAR MESSAGE");
                    
                    // Link new message to demographics associated with resident messages
                    List<String> uuidList = new ArrayList<String>();
                    for( ResidentOscarMsg res : residentOscarMsgList ) {
                        CaseManagementNote note = caseManagementNoteDao.getNote(res.getNote_id());
                        if (!uuidList.contains(note.getUuid())) {
                            MsgDemoMap msgDemoMap = new MsgDemoMap();
                            msgDemoMap.linkMsg2Demo(messageId, res.getDemographic_no());
                            uuidList.add(note.getUuid());
                        }
                    }
                }
            }
        }
        
        if( currentHour.equals(defaultHour) && currentMinute.equals(defaultMin) && providerNosList.size() > 0  ) {
            for( String p : providerNosList ) {
                String userProp = propertyDao.getStringValue(p, UserProperty.OSCAR_MSG_RECVD);
                
                if( userProp == null ) {
                    List<ResidentOscarMsg> residentOscarMsgList = residentOscarMsgDao.findBySupervisor(p);
                    String typeLinkString = createMessageTypeLinkString(residentOscarMsgList);
                    
                    if( residentOscarMsgList.size() > 0 ) {
                        Integer messageId = createAndSendMessage(p, userNo, userName, typeLinkString);
                        logger.info("SENT DEFAULT TIME OSCAR Review MESSAGE");

                        // Link new message to demographics associated with resident messages
                        for( ResidentOscarMsg res : residentOscarMsgList ) {
                            MsgDemoMap msgDemoMap = new MsgDemoMap();
                            msgDemoMap.linkMsg2Demo(messageId, res.getDemographic_no());
                        }
                    }
                }
            }
        }
        
        logger.info("Completed Sending OSCAR Review Messages");
    }
    
    
    
    
    @Override
    public void setLoggedInProvider(Provider provider) {
    this.provider = provider;
    }

    @Override
    public void setLoggedInSecurity(Security security) {
        this.security = security;
    }
    
    @Override
    public void setConfig(String string) {
    }
    
    private int createAndSendMessage(String providerNo, String senderUserNo, String senderUserName, String typeLinkString) {
        MsgMessageData msgData = new MsgMessageData();
        
        // variable to hold providerNo in array format
        String[] providers = msgData.getDups4(new String[]{ providerNo });
        ArrayList<MsgProviderData> providerListing = msgData.getProviderStructure(providers);
        String curLoco = msgData.getCurrentLocationId();
        String sentToWho = msgData.createSentToString(msgData.getLocalProvidersStructure());
        
        return msgData.sendMessageReview(MESSAGE, SUBJECT, senderUserNo, sentToWho, senderUserName, providerListing, null, null, OscarMsgType.OSCAR_REVIEW_TYPE, typeLinkString);
    }

    private String createMessageTypeLinkString(List<ResidentOscarMsg> residentOscarMsgList) {
        StringBuilder msgInfo = new StringBuilder();
        int idx = 1;
        for (ResidentOscarMsg r : residentOscarMsgList) {
            msgInfo.append(r.getDemographic_no()).append(":").append((r.getAppointment_no() == null ? "null" : r.getAppointment_no())).append(":").append(r.getId()).append(":").append(r.getNote_id());
            if (idx < residentOscarMsgList.size()) {
                msgInfo.append(",");
            }
            ++idx;
        }
        return msgInfo.toString();
    }
}
