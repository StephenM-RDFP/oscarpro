package org.oscarehr.common.model;

import com.google.common.collect.Maps;

import java.util.Map;

public enum TriggerComparator {
    EQUALS("EQUALS","Equal To"),
    NOT_EQUALS("NOT_EQUALS","Not Equal To"),
    GREATER_THAN("GREATER_THAN","Greater Than"),
    LESSER_THAN("LESSER_THAN","Lesser Than"),
    CONTAINS("CONTAINS","Contains"),
    NOT_CONTAINS("NOT_CONTAINS","Not Contains"),
    IN_LIST("IN_LIST","In List"),
    NOT_IN_LIST("NOT_IN_LIST","Not In List");
    private String key;
    private String displayName;

    private static final Map<String, TriggerComparator> keyMap = Maps.newHashMapWithExpectedSize(TriggerComparator.values().length);
    static {
        for (TriggerComparator keyEnum : TriggerComparator.values()) {
            keyMap.put(keyEnum.getKey(), keyEnum);
        }
    }
    
    TriggerComparator(String key, String displayName) {
        this.key = key;
        this.displayName = displayName;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public static TriggerComparator getByKey(String key) {
        return keyMap.get(key);
    }
}