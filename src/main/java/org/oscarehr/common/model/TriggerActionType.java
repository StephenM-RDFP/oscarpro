package org.oscarehr.common.model;

import com.google.common.collect.Maps;

import java.util.Comparator;
import java.util.Map;

public enum TriggerActionType {
    FLAG_PROVIDER("FLAG_PROVIDER", "Flag Provider", TriggerValueType.PROVIDER),
    CLEAR_FLAGGED_PROVIDERS("CLEAR_FLAGGED_PROVIDERS","Clear Flagged Providers", TriggerValueType.NO_VALUE),
    SEND_LAB_TO_UNCLAIMED_INBOX("SEND_LAB_TO_UNCLAIMED_INBOX", "Send Lab to Unclaimed Inbox", TriggerValueType.NO_VALUE),
    LABEL_AS("LABEL_AS","Label As", TriggerValueType.STRING),
    LABEL_AS_KEY("LABEL_AS_KEY","Label As Key", TriggerValueType.KEY_VALUE);
    private String key;
    private String displayName;
    private TriggerValueType valueType;

    private static final Map<String, TriggerActionType> keyMap = Maps.newHashMapWithExpectedSize(TriggerActionType.values().length);
    static {
        for (TriggerActionType keyEnum : TriggerActionType.values()) {
            keyMap.put(keyEnum.getKey(), keyEnum);
        }
    }

    public static Comparator<TriggerActionType> triggerActionTypeComparator () {
        return new Comparator<TriggerActionType>() {
            public int compare (TriggerActionType o1, TriggerActionType o2){
                return o1.getKey().compareTo(o2.getKey());
            }
        };
    }

    TriggerActionType(String key, String displayName, TriggerValueType type) {
        this.key = key;
        this.displayName = displayName;
        this.valueType = type;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TriggerValueType getValueType() {
        return valueType;
    }

    public static TriggerActionType getByKey(String key) {
        return keyMap.get(key);
    }
}