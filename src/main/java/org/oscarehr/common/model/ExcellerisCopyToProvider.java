/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "excelleris_copy_to_providers")
public class ExcellerisCopyToProvider extends AbstractModel<Integer> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "ontario_lifelabs_id")
	private String ontarioLifeLabsId;

	@Column(name = "rover_id")
	private String roverId;

	@Column(name = "on_provincial_govt_id")
	private String onProvincialGovtId;

	@Column(name = "pathnet_code")
	private String pathnetCode;

	@Column(name = "salutation")
	private String salutation;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "address1")
	private String address1;

	@Column(name = "address2")
	private String address2;

	@Column(name = "city")
	private String city;

	@Column(name = "state_or_province")
	private String stateOrProvince;

	@Column(name = "zip_or_postal")
	private String zipOrPostal;

	@Column(name = "country")
	private String country;

	@Column(name = "region")
	private String region;

	@Column(name = "specialty")
	private String specialty;

	@Column(name = "deleted")
	private Boolean deleted;

	@Column(name = "visible")
	private Boolean visible;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getOntarioLifeLabsId() {
		return ontarioLifeLabsId;
	}


	public void setOntariaLifeLabsId(String ontariaLifeLabsId) {
		this.ontarioLifeLabsId = ontariaLifeLabsId;
	}

	public String getRoverId() {
		return roverId;
	}


	public void setRoverId(String roverId) {
		this.roverId = roverId;
	}

	public String getOnProvincialGovtId() {
		return onProvincialGovtId;
	}

	public void setOnProvincialGovtId(String onProvincialGovtId) {
		this.onProvincialGovtId = onProvincialGovtId;
	}

	public String getPathnetCode() {
		return pathnetCode;
	}

	public void setPathnetCode(String pathnetCode) {
		this.pathnetCode = pathnetCode;
	}




	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getLastName() {
		if (lastName != null) {
			return lastName.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		if (firstName != null) {
			return firstName.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}

	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		if (middleName != null) {
			return middleName.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAddress1() {
		if (address1 != null) {
			return address1.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		if (address2 != null) {
			return address2.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		if (city != null) {
			return city.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateOrProvince() {
		if (stateOrProvince != null) {
			return stateOrProvince.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setStateOrProvince(String stateOrProvince) {
		this.stateOrProvince = stateOrProvince;
	}

	public String getZipOrPostal() {
		if (zipOrPostal != null) {
			return zipOrPostal.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setZipOrPostal(String zipOrPostal) {
		this.zipOrPostal = zipOrPostal;
	}

	public String getCountry() {
		if (country != null) {
			return country.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		if (region != null) {
			return region.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSpecialty() {
		if (specialty != null) {
			return specialty.replaceAll("^\"|\"$", "");
		} else {
			return null;
		}
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public void setOntarioLifeLabsId(String ontarioLifeLabsId) {
		this.ontarioLifeLabsId = ontarioLifeLabsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((deleted == null) ? 0 : deleted.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((onProvincialGovtId == null) ? 0 : onProvincialGovtId.hashCode());
		result = prime * result + ((ontarioLifeLabsId == null) ? 0 : ontarioLifeLabsId.hashCode());
		result = prime * result + ((pathnetCode == null) ? 0 : pathnetCode.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((roverId == null) ? 0 : roverId.hashCode());
		result = prime * result + ((salutation == null) ? 0 : salutation.hashCode());
		result = prime * result + ((specialty == null) ? 0 : specialty.hashCode());
		result = prime * result + ((stateOrProvince == null) ? 0 : stateOrProvince.hashCode());
		result = prime * result + ((visible == null) ? 0 : visible.hashCode());
		result = prime * result + ((zipOrPostal == null) ? 0 : zipOrPostal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExcellerisCopyToProvider other = (ExcellerisCopyToProvider) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (deleted == null) {
			if (other.deleted != null)
				return false;
		} else if (!deleted.equals(other.deleted))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (onProvincialGovtId == null) {
			if (other.onProvincialGovtId != null)
				return false;
		} else if (!onProvincialGovtId.equals(other.onProvincialGovtId))
			return false;
		if (ontarioLifeLabsId == null) {
			if (other.ontarioLifeLabsId != null)
				return false;
		} else if (!ontarioLifeLabsId.equals(other.ontarioLifeLabsId))
			return false;
		if (pathnetCode == null) {
			if (other.pathnetCode != null)
				return false;
		} else if (!pathnetCode.equals(other.pathnetCode))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (roverId == null) {
			if (other.roverId != null)
				return false;
		} else if (!roverId.equals(other.roverId))
			return false;
		if (salutation == null) {
			if (other.salutation != null)
				return false;
		} else if (!salutation.equals(other.salutation))
			return false;
		if (specialty == null) {
			if (other.specialty != null)
				return false;
		} else if (!specialty.equals(other.specialty))
			return false;
		if (stateOrProvince == null) {
			if (other.stateOrProvince != null)
				return false;
		} else if (!stateOrProvince.equals(other.stateOrProvince))
			return false;
		if (visible == null) {
			if (other.visible != null)
				return false;
		} else if (!visible.equals(other.visible))
			return false;
		if (zipOrPostal == null) {
			if (other.zipOrPostal != null)
				return false;
		} else if (!zipOrPostal.equals(other.zipOrPostal))
			return false;
		return true;
	}


	public String getFormattedName() {
		return getLastName() + "," + getFirstName();
	}

}
