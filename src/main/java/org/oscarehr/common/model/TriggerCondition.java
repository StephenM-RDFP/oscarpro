package org.oscarehr.common.model;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.TriggerListDao;
import org.oscarehr.util.SpringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="trigger_condition")
public class TriggerCondition extends AbstractModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @Column(name="key_value")
    private String keyValue;
    
    @Column(name="comparator")
    private String comparator;
    
    @Column(name="value")
    private String value;
    
    @Column(name="trigger_id")
    private Integer triggerId;
    
    @Column(name="order_position")
    private Integer orderPosition;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComparator() {
        return comparator;
    }

    public void setComparator(String condition) {
        this.comparator = condition;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(Integer triggerId) {
        this.triggerId = triggerId;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public Integer getOrderPosition() {
        return orderPosition;
    }

    public void setOrderPosition(Integer order) {
        this.orderPosition = order;
    }
    
    public String getKeyValueDisplayName() {
        TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(this.keyValue);
        return triggerConditionType != null ? triggerConditionType.getDisplayName() : "";
    }
    
    public String getComparatorDisplayName() {
        TriggerComparator triggerComparator = TriggerComparator.getByKey(this.comparator);
        return triggerComparator != null ? triggerComparator.getDisplayName() : "";
    }

    public String toString() {
        String value = this.value;
        if (StringUtils.isNotEmpty(this.value) && TriggerConditionType.getByKey(this.keyValue) != null) {
            if (TriggerValueType.PROVIDER.equals(TriggerConditionType.getByKey(this.keyValue).getValueType())) {
                ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
                Provider provider = providerDao.getProvider(this.value);
                if (provider != null) {
                    value = provider.getFormattedName();
                }
            } else if ((TriggerValueType.KEY_VALUE.equals(TriggerConditionType.getByKey(this.keyValue).getValueType()))) {
                TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(this.value);
                value = triggerConditionType != null ? triggerConditionType.getDisplayName() : value;
            } else if (TriggerComparator.IN_LIST.equals(TriggerComparator.getByKey(this.comparator)) || TriggerComparator.NOT_IN_LIST.equals(TriggerComparator.getByKey(this.comparator))) {
                if (NumberUtils.isParsable(this.value)) {
                    TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
                    TriggerList triggerList = triggerListDao.find(Integer.parseInt(this.value));
                    if (triggerList != null) {
                        value = triggerList.getName();
                    }
                }
            }
        }
        return getKeyValueDisplayName() + " is " + getComparatorDisplayName().toLowerCase() + " " + StringUtils.trimToEmpty(value);
    }
    
    public boolean isConditionMet (String inputValue) {
        return isConditionMet(inputValue, this.value);
    }
    
    public boolean isConditionMet (String inputValueA, String inputValueB) {
        TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(this.keyValue);
        if (triggerConditionType != null && inputValueA != null && inputValueB != null) {
            TriggerComparator triggerComparator = TriggerComparator.getByKey(this.comparator);
            if (triggerConditionType.getValueType().equals(TriggerValueType.NUMBER)) {
                if (NumberUtils.isParsable(inputValueA) && NumberUtils.isParsable(inputValueB)) {
                    Float floatA = Float.parseFloat(inputValueA);
                    Float floatB = Float.parseFloat(inputValueB);
                    switch (triggerComparator) {
                        case EQUALS:
                            return floatA.equals(floatB);
                        case NOT_EQUALS:
                            return !floatA.equals(floatB);
                        case GREATER_THAN:
                            return floatA.compareTo(floatB) > 0;
                        case LESSER_THAN:
                            return floatA.compareTo(floatB) < 0;
                    }
                }
            } else if (triggerConditionType.getValueType().equals(TriggerValueType.DATE)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dateA = sdf.parse(inputValueA);
                    Date dateB = sdf.parse(inputValueB);
                    switch (triggerComparator) {
                        case EQUALS:
                            return dateA.equals(dateB);
                        case NOT_EQUALS:
                            return !dateA.equals(dateB);
                        case GREATER_THAN:
                            return dateA.compareTo(dateB) > 0;
                        case LESSER_THAN:
                            return dateA.compareTo(dateB) < 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (triggerConditionType.equals(TriggerConditionType.OBR_4) ||
                    triggerConditionType.equals(TriggerConditionType.OBR_4_1) ||
                    triggerConditionType.equals(TriggerConditionType.OBR_4_2) ||
                    triggerConditionType.equals(TriggerConditionType.LOINC_CODE)) {
                List<String> valueList = Arrays.asList(inputValueA.split("\\|"));
                switch (triggerComparator) {
                    case EQUALS:
                        return valueList.contains(inputValueB);
                    case NOT_EQUALS:
                        return !valueList.contains(inputValueB);
                    case CONTAINS:
                        for (String value : valueList) {
                            if (value.contains(inputValueB)) {
                                return true;
                            }
                        }
                        return false;
                    case NOT_CONTAINS:
                        for (String value : valueList) {
                            if (value.contains(inputValueB)) {
                                return false;
                            }
                        }
                        return true;
                    case IN_LIST:
                        if (NumberUtils.isParsable(inputValueB)) {
                            TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
                            TriggerList triggerList = triggerListDao.find(Integer.parseInt(inputValueB));
                            for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
                                for (String value : valueList) {
                                    if (triggerListItem.getItemValue().equals(value)) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    case NOT_IN_LIST:
                        if (NumberUtils.isParsable(inputValueB)) {
                            TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
                            TriggerList triggerList = triggerListDao.find(Integer.parseInt(inputValueB));
                            for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
                                for (String value : valueList) {
                                    if (triggerListItem.getItemValue().equals(value)) {
                                        return false;
                                    }
                                }
                            }
                        }
                        return true;
                }
            } else {
                switch (triggerComparator) {
                    case EQUALS:
                        return inputValueA.equals(inputValueB);
                    case NOT_EQUALS:
                        return !inputValueA.equals(inputValueB);
                    case CONTAINS:
                        return inputValueA.contains(inputValueB);
                    case NOT_CONTAINS:
                        return !inputValueA.contains(inputValueB);
                    case IN_LIST:
                        if (NumberUtils.isParsable(inputValueB)) {
                            TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
                            TriggerList triggerList = triggerListDao.find(Integer.parseInt(inputValueB));
                            if (triggerList != null && triggerList.getTriggerListItems() != null) {
                                for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
                                    if (triggerListItem.getItemValue().equals(inputValueA)) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    case NOT_IN_LIST:
                        if (NumberUtils.isParsable(inputValueB)) {
                            TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
                            TriggerList triggerList = triggerListDao.find(Integer.parseInt(inputValueB));
                            if (triggerList != null && triggerList.getTriggerListItems() != null) {
                                for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
                                    if (triggerListItem.getItemValue().equals(inputValueA)) {
                                        return false;
                                    }
                                }
                            }
                        }
                        return true;
                }
            }
        }
        return false;
    }
}