package org.oscarehr.common.model;

public enum TriggerValueType {
    STRING("STRING", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS, TriggerComparator.CONTAINS, TriggerComparator.NOT_CONTAINS, TriggerComparator.IN_LIST, TriggerComparator.NOT_IN_LIST}),
    STRING_LIST("STRING_LIST", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS, TriggerComparator.CONTAINS, TriggerComparator.NOT_CONTAINS, TriggerComparator.IN_LIST, TriggerComparator.NOT_IN_LIST}),
    NUMBER("NUMBER", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS, TriggerComparator.GREATER_THAN, TriggerComparator.LESSER_THAN}),
    DATE("DATE", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS, TriggerComparator.GREATER_THAN, TriggerComparator.LESSER_THAN}),
    PROVIDER("PROVIDER", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS}),
    BOOLEAN("BOOLEAN", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS}),
    KEY_VALUE("KEY_VALUE", new TriggerComparator[]{TriggerComparator.EQUALS, TriggerComparator.NOT_EQUALS}),
    NO_VALUE("NO_VALUE", new TriggerComparator[]{});
    private String key;
    private TriggerComparator[] triggerComparator;
    
    TriggerValueType(String key, TriggerComparator[] triggerComparator) {
        this.key = key;
        this.triggerComparator = triggerComparator;
    }

    public String getKey() {
        return key;
    }

    public TriggerComparator[] getTriggerComparator() {
        return triggerComparator;
    }
}