package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "document_comment")
public class DocumentComment extends AbstractModel<Integer> implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "provider_no")
    private String providerNo;
    @Column(name = "document_no")
    private Integer documentNo;
    @Column(name = "comment")
    private String comment = "";
    @Column(name = "comment_time")
    private Date commentTime;
    @Column(name = "deleted")
    private Boolean deleted = false;

    public DocumentComment() {
    }

    public DocumentComment(String providerNo, Integer documentNo, String comment) {
        this.providerNo = providerNo;
        this.documentNo = documentNo;
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getProviderNo() {
        return providerNo;
    }
    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getDocumentNo() {
        return documentNo;
    }
    public void setDocumentNo(Integer documentNo) {
        this.documentNo = documentNo;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentTime() {
        return commentTime;
    }
    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @PrePersist
    protected void jpaSetDate() {
        this.commentTime = new Date();
    }
}
