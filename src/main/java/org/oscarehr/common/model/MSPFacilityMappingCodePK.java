package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MSPFacilityMappingCodePK implements Serializable {
    @Column(name="billing_code")
    private String billingCode;

    @Column(name="clinic_id")
    private Integer clinicId;

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }
}
