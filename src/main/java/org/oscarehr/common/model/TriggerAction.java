package org.oscarehr.common.model;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.util.SpringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="trigger_action")
public class TriggerAction extends AbstractModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @Column(name="key_value")
    private String keyValue;
    
    @Column(name="value")
    private String value;
    
    @Column(name="trigger_id")
    private Integer triggerId;
    
    @Column(name="order_position")
    private Integer orderPosition;
    
    public TriggerAction (String keyValue, String value) {
        this.keyValue = keyValue;
        this.value = value;
    }

    public TriggerAction () {
        
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String type) {
        this.keyValue = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(Integer triggerId) {
        this.triggerId = triggerId;
    }

    public Integer getOrderPosition() {
        return orderPosition;
    }

    public void setOrderPosition(Integer order) {
        this.orderPosition = order;
    }
    
    public String getDisplayName () {
        TriggerActionType triggerActionType = TriggerActionType.getByKey(this.keyValue);
        return triggerActionType != null ? triggerActionType.getDisplayName() : "";
    }

    public String toString() {
        String value = this.value;
        if (StringUtils.isNotEmpty(this.value) && TriggerActionType.getByKey(this.keyValue) != null) {
            if (TriggerValueType.PROVIDER.equals(TriggerActionType.getByKey(this.keyValue).getValueType())) {
                ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
                Provider provider = providerDao.getProvider(this.value);
                if (provider != null) {
                    value = provider.getFormattedName();
                }
            } else if (TriggerValueType.KEY_VALUE.equals(TriggerActionType.getByKey(this.keyValue).getValueType())) {
                TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(this.value);
                value = triggerConditionType != null ? triggerConditionType.getDisplayName() : value;
            }
        }
        return getDisplayName() + " " + StringUtils.trimToEmpty(value);
    }
}