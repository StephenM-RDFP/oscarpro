package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="trigger_log")
public class TriggerLog extends AbstractModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @Column(name="trigger_id")
    private int triggerId;
    
    @Column(name="trigger_action")
    private String triggerAction;
    
    @Column(name="trigger_action_value")
    private String triggerActionValue;

    @Column(name="trigger_type")
    private String triggerType;

    @Column(name="content_id")
    private String contentId;

    @Column(name="time_stamp")
    private Date timeStamp;
    
    TriggerLog (int triggerId, String triggerAction, String triggerActionValue, String triggerType, String contentId, Date timeStamp) {
        this.triggerId = triggerId;
        this.triggerAction = triggerAction;
        this.triggerActionValue = triggerActionValue;
        this.triggerType = triggerType;
        this.contentId = contentId;
        this.timeStamp = timeStamp;
    }
    
    TriggerLog () {
        
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(int triggerId) {
        this.triggerId = triggerId;
    }

    public String getTriggerAction() {
        return triggerAction;
    }

    public void setTriggerAction(String triggerAction) {
        this.triggerAction = triggerAction;
    }

    public String getTriggerActionValue() {
        return triggerActionValue;
    }

    public void setTriggerActionValue(String triggerActionValue) {
        this.triggerActionValue = triggerActionValue;
    }

    public String getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}