/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.oscarehr.common.model.ExcellerisCopyToProvider;
import org.oscarehr.util.MiscUtils;
import org.springframework.stereotype.Repository;

@Repository
public class ExcellerisCopyToProviderDao extends AbstractDao<ExcellerisCopyToProvider> {

	private static final Logger logger = MiscUtils.getLogger();
	public ExcellerisCopyToProviderDao() {
		super(ExcellerisCopyToProvider.class);
	}

	/**
	 * Sorted by lastname,firstname
	 */
	public List<ExcellerisCopyToProvider> findAll()
	{
		logger.debug("Entering copy to dao find all");
		Query query = entityManager
				.createQuery("select x from " + modelClass.getName()
						+ " x where x.deleted=0 order by x.lastName,x.firstName");

		@SuppressWarnings("unchecked")
		List<ExcellerisCopyToProvider> results = query.getResultList();
		return(results);
	}


	public List<ExcellerisCopyToProvider> findByFullName(String lastName, String firstName) {
		Query query = entityManager.createQuery("select x from " + modelClass.getName()
				+ " x WHERE x.lastName like ? and x.firstName like ? and x.deleted=0 order by x.lastName");
		query.setParameter(1, lastName + "%");
		query.setParameter(2, firstName + "%");

		@SuppressWarnings("unchecked")
		List<ExcellerisCopyToProvider> cList = query.getResultList();
		if (cList != null && cList.size() > 0) {
			return cList;
		}


		return null;
	}

	public List<ExcellerisCopyToProvider> findByLastName(String lastName) {
		return findByFullName(lastName, "");
	}

}
