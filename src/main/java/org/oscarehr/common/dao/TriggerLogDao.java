package org.oscarehr.common.dao;

import org.oscarehr.common.model.TriggerLog;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class TriggerLogDao extends AbstractDao<TriggerLog>
{
    public TriggerLogDao() { super(TriggerLog.class); }

    public List<TriggerLog> findAll () {
        Query query = entityManager.createQuery("SELECT t FROM TriggerLog t ORDER BY t.timeStamp DESC, t.id DESC");
        return query.getResultList();
    }
    
    public List<TriggerLog> findAllByPage (int page) {
        Query query = entityManager.createQuery("SELECT t FROM TriggerLog t ORDER BY t.timeStamp DESC, t.id DESC");
        query.setFirstResult(page*20);
        query.setMaxResults(20);
        return query.getResultList();
    }
    
    public int getCount () {
        Query query = entityManager.createQuery("SELECT COUNT(t) FROM TriggerLog t");
        return ((Long) query.getSingleResult()).intValue();
    }
}