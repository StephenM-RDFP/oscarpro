package org.oscarehr.common.dao;

import org.oscarehr.common.model.Trigger;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class TriggerDao extends AbstractDao<Trigger>
{
    public TriggerDao() { super(Trigger.class); }

    public List<Trigger> findAll () {
        Query query = entityManager.createQuery("SELECT t FROM Trigger t WHERE (t.triggerStatus = 'A' OR t.triggerStatus = 'I') ORDER BY t.triggerStatus ASC, t.internalId DESC, t.name ASC");
        return query.getResultList();
    }
    
    public List<Trigger> findAllActiveByType(String type) {
        Query query = entityManager.createQuery("SELECT t FROM Trigger t WHERE t.triggerStatus = 'A' AND t.type = :type ORDER BY t.name ASC");
        query.setParameter("type", type);
        return query.getResultList();
    }
    
    public Long countAllActive() {
        Query query = entityManager.createQuery("SELECT COUNT(t) FROM Trigger t WHERE t.triggerStatus = 'A'");
        return (Long) query.getSingleResult();
    }
}