package org.oscarehr.common.dao;

import org.oscarehr.common.model.MSPFacilityMapping;
import org.oscarehr.common.model.MSPFacilityMappingBlackWhite;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class MSPFacilityMappingBlackWhiteDao extends AbstractDao<MSPFacilityMappingBlackWhite>
{
    public MSPFacilityMappingBlackWhiteDao() { super(MSPFacilityMappingBlackWhite.class); }

    public List<MSPFacilityMappingBlackWhite> findAll () {
        Query query = entityManager.createNativeQuery("select bw.*" +
                " from billing_msp_facility_mapping_blackwhite bw" +
                " left join provider p on p.provider_no = bw.provider_no" +
                " left join billing_msp_facility_mapping c on c.clinic_id = bw.clinic_id" +
                " order by c.clinic_name, p.last_name");
        List<MSPFacilityMappingBlackWhite> resultList = new ArrayList<>();
        List<Object[]> objectList = query.getResultList();
        for (Object[] object : objectList) {
            MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite = new MSPFacilityMappingBlackWhite();
            mspFacilityMappingBlackWhite.setProviderNo((String)object[0]);
            mspFacilityMappingBlackWhite.setClinicId((Integer)object[1]);
            resultList.add(mspFacilityMappingBlackWhite);
        }
        return resultList;
    }
    
    public void removeByProviderNo (String providerNo) {
        MSPFacilityMappingBlackWhite result = this.find(providerNo);
        entityManager.remove(entityManager.contains(result) ? result : entityManager.merge(result));
    }
    
    public Integer getClinicIdIfValid(String providerNo) {
        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        SystemPreferences systemPreferences = systemPreferencesDao.findPreferenceByName("msp_facility_mapping_black_white");
        String listType = (systemPreferences != null && systemPreferences.getValue() != null)? systemPreferences.getValue() : "black";
        MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite = this.findByProviderNo(providerNo);
        if (listType.equals("black")) {
            MSPFacilityMappingDao mspFacilityMappingDao = SpringUtils.getBean(MSPFacilityMappingDao.class);
            List<MSPFacilityMapping> mspFacilityMappingList = mspFacilityMappingDao.findAll();
            return mspFacilityMappingBlackWhite == null ? (!mspFacilityMappingList.isEmpty() ? mspFacilityMappingList.get(0).getId() : 0) : 0;
        } else {
            return mspFacilityMappingBlackWhite != null ? mspFacilityMappingBlackWhite.getClinicId() : 0;
        }
    }
    
    public MSPFacilityMappingBlackWhite findByProviderNo (String providerNo) {
        Query query = entityManager.createQuery("FROM MSPFacilityMappingBlackWhite b WHERE b.providerNo = :providerNo");
        query.setParameter("providerNo", providerNo);
        List<MSPFacilityMappingBlackWhite> mspFacilityMappingBlackWhiteList = query.getResultList();
        return mspFacilityMappingBlackWhiteList.isEmpty() ? null : mspFacilityMappingBlackWhiteList.get(0);
    }
}
