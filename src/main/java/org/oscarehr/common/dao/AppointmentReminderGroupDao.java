package org.oscarehr.common.dao;

import org.apache.log4j.Logger;
import org.oscarehr.common.model.AppointmentReminder;
import org.oscarehr.common.model.AppointmentReminderGroup;
import org.oscarehr.util.MiscUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
@SuppressWarnings("unchecked")
public class AppointmentReminderGroupDao extends AbstractDao<AppointmentReminderGroup> {

    public AppointmentReminderGroupDao() {
        super(AppointmentReminderGroup.class);
    }

    private Logger logger = MiscUtils.getLogger();

    public AppointmentReminderGroup getByAppointmentNo(Integer appointmentNo) {
        String sql = "select a from AppointmentReminderGroup a where a.appointmentId=?1";
        Query query = entityManager.createQuery(sql);
        query.setParameter(1, appointmentNo);

        try {
            return(AppointmentReminderGroup) query.getSingleResult();
        } catch (NoResultException nre) {
            logger.debug("No appointment reminder group record found! | " + nre);
            return null;
        }
    }
    
    public Integer getMostRecentGroupNo () {
        String sql = "select MAX(a.groupNo) from AppointmentReminderGroup a";
        Query query = entityManager.createQuery(sql);
        
        return query.getSingleResult() == null ? 0 : (Integer) query.getSingleResult();
    }
}
