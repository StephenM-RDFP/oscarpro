package org.oscarehr.common.dao;

import org.oscarehr.common.model.TriggerAction;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.stereotype.Repository;
import oscar.log.LogAction;

@Repository
@SuppressWarnings("unchecked")
public class TriggerActionDao extends AbstractDao<TriggerAction>
{
    public TriggerActionDao() { super(TriggerAction.class); }

    public void removeActionById (TriggerAction triggerAction) {
        try {
            entityManager.remove(entityManager.contains(triggerAction) ? triggerAction : entityManager.merge(triggerAction));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}