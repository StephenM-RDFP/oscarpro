package org.oscarehr.common.dao;

import org.oscarehr.common.model.OscarLog;
import org.oscarehr.common.model.TriggerListItem;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;
import oscar.log.LogAction;

@Repository
@SuppressWarnings("unchecked")
public class TriggerListItemDao extends AbstractDao<TriggerListItem>
{
    public TriggerListItemDao() { super(TriggerListItem.class); }
    
    public void removeTriggerListItem(TriggerListItem item) {
        entityManager.remove(entityManager.contains(item) ? item : entityManager.merge(item));
    }
}