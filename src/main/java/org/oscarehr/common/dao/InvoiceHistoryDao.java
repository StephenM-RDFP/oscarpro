package org.oscarehr.common.dao;

import org.oscarehr.common.model.InvoiceHistory;
import org.springframework.stereotype.Repository;
import oscar.util.StringUtils;

import javax.persistence.Query;
import java.util.List;

@Repository
public class InvoiceHistoryDao extends AbstractDao<InvoiceHistory> {
    public InvoiceHistoryDao() {
        super(InvoiceHistory.class);
    }
    
    public List<InvoiceHistory> findHistory(Integer billingNo, String field) {
        return findHistory(billingNo, 0, field);
    }

    public List<InvoiceHistory> findHistory(Integer billingNo, Integer itemNo, String field) {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("select i from  InvoiceHistory i where i.billingNo = :billingNo and i.itemNo = :itemNo");

        if (StringUtils.filled(field)) {
            queryStr.append(" and i.field = :field");
        }

        queryStr.append(" order by i.timestamp desc");

        Query q = entityManager.createQuery(queryStr.toString());
        q.setParameter("billingNo", billingNo);
        q.setParameter("itemNo", itemNo);

        if (StringUtils.filled(field)) {
            q.setParameter("field", field);
        }

        List<InvoiceHistory> rs = q.getResultList();

        return rs;
    }
}

