package org.oscarehr.common.dao;

import org.oscarehr.common.model.BillingRule;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class BillingRuleDao extends AbstractDao<BillingRule> {

    public BillingRuleDao() {
        super(BillingRule.class);
    }
    
    public List<BillingRule> getAllForBillRegion(String billRegion) {
        String queryStr = "FROM BillingRule b WHERE b.billRegion = :billRegion";

        Query q = entityManager.createQuery(queryStr);
        q.setParameter("billRegion", billRegion);
        
        return q.getResultList();
    }
}
