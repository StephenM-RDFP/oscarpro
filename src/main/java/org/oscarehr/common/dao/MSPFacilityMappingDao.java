package org.oscarehr.common.dao;

import org.oscarehr.common.model.MSPFacilityMapping;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class MSPFacilityMappingDao extends AbstractDao<MSPFacilityMapping>
{
    public MSPFacilityMappingDao() { super(MSPFacilityMapping.class); }

    public List<MSPFacilityMapping> findAll () {
        Query query = entityManager.createQuery("FROM MSPFacilityMapping b ORDER BY b.clinic ASC");
        return query.getResultList();
    }
    
    public MSPFacilityMapping findByClinicName (String clinic) {
        Query query = entityManager.createQuery("FROM MSPFacilityMapping b WHERE b.clinic = :clinic");
        query.setParameter("clinic", clinic);
        try {
            return (MSPFacilityMapping) query.getSingleResult();
        } catch (Exception e) {
            
        }
        return null;
    }
    
    public void removeClinic(Integer clinicId, LoggedInInfo loggedInInfo) {
        try {
            MSPFacilityMapping result = this.find(clinicId);
            
            OscarLogDao oscarLogDao = SpringUtils.getBean(OscarLogDao.class);
            OscarLog oscarLog = new OscarLog();
            oscarLog.setAction("remove");
            oscarLog.setContent("MSPFacilityMapping");
            oscarLog.setContentId("Clinic=" + result.getClinic());
            oscarLog.setIp(loggedInInfo.getIp());
            oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
            oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
            oscarLog.setData("facilityNumber=" + result.getFacilityNumber() + "; "
                    + "subNumber=" + result.getSubNumber() + ";");
            oscarLogDao.persist(oscarLog);
            
            entityManager.remove(entityManager.contains(result) ? result : entityManager.merge(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
