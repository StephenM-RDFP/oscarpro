package org.oscarehr.common.dao;

import org.oscarehr.common.model.DocumentComment;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class DocumentCommentDao extends AbstractDao<DocumentComment> {
	public DocumentCommentDao() {
		super(DocumentComment.class);
	}

	public List<DocumentComment> findBySegmentIdAndProviderNoNotDeleted(Integer segmentID, String providerNo) {
		String sql = "SELECT dc FROM DocumentComment dc WHERE dc.documentNo = :segmentID AND dc.providerNo = :providerNo AND dc.deleted = FALSE";
		Query query = entityManager.createQuery(sql);
		query.setParameter("segmentID", segmentID);
		query.setParameter("providerNo", providerNo);
		return query.getResultList();
	}
}
