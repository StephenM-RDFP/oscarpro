package org.oscarehr.integration.excelleris.eorder.converter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.castor.core.util.StringUtil;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.DiagnosticOrder;
import org.hl7.fhir.instance.model.DiagnosticOrder.DiagnosticOrderItemComponent;
import org.hl7.fhir.instance.model.DiagnosticOrder.DiagnosticOrderStatus;
import org.hl7.fhir.instance.model.Extension;
import org.hl7.fhir.instance.model.Practitioner;
import org.hl7.fhir.instance.model.Questionnaire;
import org.hl7.fhir.instance.model.Questionnaire.QuestionComponent;
import org.hl7.fhir.instance.model.QuestionnaireResponse;
import org.hl7.fhir.instance.model.Reference;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.HttpClientHelper;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

import cdsrourke.PatientDocument.Patient;
import oscar.util.StringUtils;

public class DiagnosticOrderConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	private static final String LAB_TEST_PREFIX = "T_";
	private static final String TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE = "/ExcellerisTestCodeMapping.properties";
	
	private static final String NEONATAL_BILIRUBIN_TEST = "T_neonatal_bilirubin";
	private static final String NEONATAL_BILIRUBIN_CHILD_AGE_DAYS = "EO_child_age_days";
	private static final String NEONATAL_BILIRUBIN_CHILD_AGE_HOURS = "EO_child_age_hours";
	private static final String NEONATAL_BILIRUBIN_PRACTITIONER_PHONE = "EO_practitioner_phone";
	private static final String NEONATAL_BILIRUBIN_PATIENT_PHONE = "EO_patient_phone";
	private static final String ANSWER_UNKNOWN = "UNK";
	
	private static final String TDM_TIME_COLLECTED_1 = "EO_time_collected_1";
	private static final String TDM_TIME_OF_LAST_DOSE_1 = "EO_time_of_last_dose_1";
	private static final String TDM_TIME_OF_NEXT_DOSE_1 = "EO_time_of_next_dose_1";
	private static final String TDM_TIME_COLLECTED_2 = "EO_time_collected_2";
	private static final String TDM_TIME_OF_LAST_DOSE_2 = "EO_time_of_last_dose_2";
	private static final String TDM_TIME_OF_NEXT_DOSE_2 = "EO_time_of_next_dose_2";
	
	private static final String THERAPEUTIC_DRUG_MONITORING = "T_therapeutic_drug_monitor";
	
	private static final String THERAPEUTIC_DRUG_MONITORING_1 = "T_therapeutic_drug_mon_src_1";
	private static final String THERAPEUTIC_DRUG_MONITORING_2 = "T_therapeutic_drug_mon_src_2";
	
	private static final String VAGINAL_RECTAL_GROUP_B_STREP = "T_vaginal_rectal_group_b";
	private static final String VAGINAL_RECTAL_GROUP_B_STREP_SRC = "T_vaginal_rectal_group_b_src";

	private static final String CHLAMYDIA = "T_chlamydia";
	private static final String CHLAMYDIA_SRC = "T_chlamydia_src";

	private static final String GC = "T_gc";
	private static final String GC_SRC = "T_gc_src";

	private static final String WOUND = "T_wound";
	private static final String WOUND_SRC = "T_wound_src";
	
	private static final String OTHER_SWABS_PUS = "T_other_swabs_pus";
	private static final String OTHER_SWABS_PUS_SRC = "T_other_swabs_pus_src";
	
	private static final String  OTHER_TESTS = "T_other_tests_";
	private static final String QUESTION_RESPONSE_PREFIX = "QR_q";


	
	/**
	 * Convert eForm values to a Diagnostic Order
	 * 
	 * @param valueMap               hashmap that contains the key value paires to
	 *                               eForm values
	 * @param questionnaireResponses list of questionnaire responses returned to
	 *                               handle hard-coded tests that requires
	 *                               questionnaires to be answered
	 * @param questionnaires
	 * 
	 * @return converted Diagnostic Order
	 */
	public static DiagnosticOrder toFhirObject(Map<String, EFormValue> valueMap,
			QuestionnaireResponse questionnaireResponse, Questionnaire questionnaire) {
		
		LOG.trace("Calling DiagnosticOrderConverter.toFhirObject");
		
		// Load test code mapping file		
		InputStream inputStream = HttpClientHelper.class.getResourceAsStream(TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE);
		Properties properties = new Properties();
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			LOG.error("Error loading " + TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE, e);
			throw new ServiceException("Error loading " + TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE);
		}		
		
		// eform value to diagnostic order conversion
		DiagnosticOrder order = new DiagnosticOrder();
		// status
		order.setStatus(DiagnosticOrderStatus.REQUESTED);
		

		// Extension
		// ordering subject pregnancy		
		Boolean pregnant = false;
		if (valueMap.containsKey("EO_pregnant")) {
			  pregnant = true;
		}
		Extension extension = FhirResourceHelper.createBooleanExtension(pregnant, ResourceUrlHelper.getSubjectPregancyExtensionUrl());
		order.addExtension(extension);

		
		// Specimen Collection
		
		if (valueMap.containsKey("EO_specimen_col_date")) {
			EFormValue eformValue = valueMap.get("EO_specimen_col_date");
			String specimenCollectionDate = eformValue.getVarValue();  // expect it to be of the form yyyy/mm/dd
			
			// see if a time is specified too
			String specimenCollectionTime = "00:00";
			if (valueMap.containsKey("EO_specimen_col_time")) {
				eformValue = valueMap.get("EO_specimen_col_time");
				specimenCollectionTime = eformValue.getVarValue();  // expect it to be of the form HH:MM 24hr clock
			}
		    specimenCollectionTime = specimenCollectionTime +":01";  // add seconds field
			// find timezone info
			eformValue = valueMap.get("EO_timezone_offset");
			String timeZone = eformValue.getVarValue();
			String specimenCollectionDateTime = specimenCollectionDate.replace("/","-") + "T" + specimenCollectionTime + timeZone;
			
			extension = FhirResourceHelper.createDateTimeExtension(specimenCollectionDateTime, ResourceUrlHelper.getCollectionDateTimeExtensionUrl());
		    order.addExtension(extension);
			
		}
		
		// diagnosis/notes
		if (valueMap.containsKey("EO_Additional_Clinic_Info")) {
			//LOG.info("additional clinical information ");
			 EFormValue eformValue = valueMap.get("EO_Additional_Clinic_Info");
		    String diagnosis = eformValue.getVarValue();
		    //convert to url format - user URI to convert the content to replace space with %20 etc
			try {
				URI uri = new URI("dummy", diagnosis, null);
				//LOG.info("raw url encode: " + uri.getRawSchemeSpecificPart());
			    extension = FhirResourceHelper.createStringExtension(uri.getRawSchemeSpecificPart(), ResourceUrlHelper.getDiagnosisExtensionUrl());
			    order.addExtension(extension);
			} catch (URISyntaxException e) {
				LOG.error("Additional Clinical Information");
				throw new ServiceException("Additional Clinical Information");
			}
		}
		
		// diagnostic code
		extension = FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getDiagnosticCodeExtensionUrl(),
				"Diagnosis", ResourceUrlHelper.getClassificationExtensionUrl());
		order.addExtension(extension);		
		
		// Subject (reference)
		String patientExcellerisId = EFormValueHelper.getValue(valueMap, PatientPropertySet.EXCELLERIS_PATIENT_ID);
		Reference reference = new Reference();
		if (patientExcellerisId == null) {
			// new patient		
			reference.setReference(ResourceUrlHelper.getResourceUrl(Patient.class.getSimpleName(), null));
			order.setSubject(reference);
		} else {
			// existing patient in Excelleris
			reference.setReference(ResourceUrlHelper.getResourceUrl(Patient.class.getSimpleName(), patientExcellerisId));
			order.setSubject(reference);
		}
		
		// Encounter (reference)
		String encounterUrl = EFormValueHelper.getValue(valueMap, EncounterPropertySet.ENCOUNTER_URL);
		if (encounterUrl == null) {
			LOG.error("Missing " + EncounterPropertySet.ENCOUNTER_URL + " eform value.");
			throw new ServiceException("Missing " + EncounterPropertySet.ENCOUNTER_URL + " eform value.");
		} else {
			reference = new Reference();
			reference.setReference(encounterUrl);
			order.setEncounter(reference);
		}
		
		// Orderer (reference)
		String orderingProviderExcellerisId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		if (orderingProviderExcellerisId == null) {
			LOG.error("Orderring provider Excelleris Id");
			throw new ServiceException("This lab requisition will not be submitted as an eOrder because the orderring provider does not have Lifelabs/Excelleris Ids.");
		} else {
			reference = new Reference();
			reference.setReference((ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), orderingProviderExcellerisId)));
			order.setOrderer(reference);
		}
		
		// copy to provider
		// automatically add ordering provider as copy to provider
		extension = FhirResourceHelper.createValueReferenceExtension(
				ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), orderingProviderExcellerisId), 
				ResourceUrlHelper.getCopyToRecipientExtensionUrl());
		order.addExtension(extension);
		
		// other copy to providers
		Integer unknownCopyToProvider = 1;
		for (int ccProviderIndex = 1; ccProviderIndex <= PractitionerPropertyConfig.MAXIMUM_NUM_COPY_TO_PROVIDERS; ccProviderIndex++) {		
			PractitionerPropertyConfig propertyConfig = new PractitionerPropertyConfig(
					PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderLifeLabsIdPropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderTitlePropertyNameByIndex(ccProviderIndex));
			if (PractitionerConverter.hasCopyToProviderData(valueMap, propertyConfig)) {
				String copyToProviderExcellerisId = EFormValueHelper.getValue(valueMap, 
						PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex));
				if (!StringUtils.isNullOrEmpty(copyToProviderExcellerisId)) {
					extension = FhirResourceHelper.createValueReferenceExtension(
							ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), copyToProviderExcellerisId), 
							ResourceUrlHelper.getCopyToRecipientExtensionUrl());					
				} else {
					extension = FhirResourceHelper.createValueReferenceExtension(
							ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), PractitionerConverter.getUnknownProviderId(unknownCopyToProvider)), 
							ResourceUrlHelper.getCopyToRecipientExtensionUrl());
					unknownCopyToProvider++;
				}			
				order.addExtension(extension);
			}
		}		
				
		// Identifier
		
//		testCodeToKeyMap is used to create the questionnarie response
//		the list has the test key at index 0, and test itemId at index 1
		// Items
		// walk through all values with T_ prefix
		Map<String, List<String>> testCodeToKeyAndItemIdMap = new HashMap<String, List<String>>();
		// valueMap contains all eForm Value data belonging to the current eForm Data instance				
		for (String key : valueMap.keySet()) {
			if (key.startsWith(LAB_TEST_PREFIX) && isRequired(key)) {
				EFormValue value = valueMap.get(key);
//				the test has been removed from the order
				if (StringUtils.isNullOrEmpty(value.getVarValue())) {
					continue;
				}

				DiagnosticOrderItemComponent labTest = new DiagnosticOrderItemComponent();
				order.addItem(labTest);
				// id

				String itemUuid = UUID.randomUUID().toString();
				labTest.setId(itemUuid);
				// status
				labTest.setStatus(DiagnosticOrderStatus.REQUESTED);
				// extension
				// payer-type
				String payer = null;
				if (valueMap.containsKey("EO_OHIP")) {
					payer = "OHIP";
				} else if (valueMap.containsKey("EO_WSIB")) {
					payer = "WSIB";
				} else  /*if (valueMap.containsKey("EO_Third_Party"))*/ {
					// not insured so has to be patient pay
					payer = "Patient";
				}
				extension = FhirResourceHelper.createCodeableConceptExtension(
						ResourceUrlHelper.getItemPayerTypeValueSetExtensionUrl(), payer, ResourceUrlHelper.getItemPayerTypeExtensionUrl());
				labTest.addExtension(extension);
				
				// tie breaker
				// TODO: how to set tie breaker code
				String tieBreaker = "0";
//				if (isOtherTests(key)) {
//					// the value depends on the order
//					Integer testPosition = Integer.parseInt(key.replace(OTHER_TESTS, "")) - 1;
//					tieBreaker = testPosition.toString();
//				}
				extension = FhirResourceHelper.createCodeableConceptExtension(
						ResourceUrlHelper.getItemTieBreakerValueSetExtensionUrl(), tieBreaker,
						ResourceUrlHelper.getItemTieBreakerExtensionUrl());
				labTest.addExtension(extension);

				// is hard coded test
				String hardCoded = isHardCodedTest(key) ? "1" : "0";
				// this value apparently takes on other values based on the test - not described in the api doc
				if (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) {
					hardCoded = "2";
				} else  if (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
					hardCoded = "3";
				}
				extension = FhirResourceHelper.createCodeableConceptExtension(
						ResourceUrlHelper.getItemIsHardCodedTestValueSetExtensionUrl(), hardCoded, ResourceUrlHelper.getItemIsHardCodedTestExtensionUrl());
				labTest.addExtension(extension);
				
				// other test - this extension is needed if it is a Other Test
				if (isOtherTests(key)) {
					// the value depends on the order
					String otherTest = key.replace(OTHER_TESTS, "");
//					String otherTest = isOtherTests(key) ? "1" : "0";
					extension = FhirResourceHelper.createCodeableConceptExtension(
							ResourceUrlHelper.getItemOtherTestValueSetExtensionUrl(), otherTest, ResourceUrlHelper.getItemOtherTestExtensionUrl());
					labTest.addExtension(extension);
				}
				
				// is other microbiology
				String otherMicrobiology = isOtherMicrobiology(key) ? "true" : "false";
				extension = FhirResourceHelper.createCodeableConceptExtension(
						ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl(), otherMicrobiology, ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestExtensionUrl());
				labTest.addExtension(extension);
				
				// code
				CodeableConcept code = new CodeableConcept();
				Coding coding = new Coding();
				coding.setSystem(ResourceUrlHelper.getOrderCodeValueSetSystemUrl());
				
				String testCode = getTestCode(valueMap, properties, key, questionnaireResponse, itemUuid,
						questionnaire);

				ArrayList<String> keyItemId = new ArrayList<String>();
				keyItemId.add(0, key);
				keyItemId.add(1, itemUuid);
				testCodeToKeyAndItemIdMap.put(testCode, keyItemId);

				LOG.info("Test code: " + testCode);				
				coding.setCode(testCode);
				code.addCoding(coding);
				labTest.setCode(code);

			}
		}
				
		if (questionnaire != null) {
			questionnaireResponse = createQuestionnaireResponse(valueMap, questionnaireResponse, questionnaire,
					testCodeToKeyAndItemIdMap);

		}

		// make sure that there is at least one test in the order
		if (order.getItem() == null || order.getItem().isEmpty()) {
			LOG.warn("There is no test in the lab order.");
			throw new ServiceException("Your lab order is empty. You need to select one or more tests before submitting the order.");
		}
		return order;
	}
	

	// --------------------------------------------------------------- Private Methods
	
	/**
	 * Get test code for the given eform value key
	 * 
	 * @param valueMap  hashmap for eForm values
	 * @param properties  test name and code mapping properties file
	 * @param key  eform value key to be mapped to test code
	 * 
	 * @return  test code
	 */
	private static String getTestCode(
			Map<String, EFormValue> valueMap, Properties properties, String key,
			QuestionnaireResponse questionnaireResponse, String itemUuid, Questionnaire questionnaire) {
		// using only 1 questionnaire response to store all questions
		// a null will cause a new one to be created. otherwise will add more info to the existing one
//		QuestionnaireResponse questionnaireResponse = null;
//		if (questionnaireResponses.size() > 0) {
//			questionnaireResponse = questionnaireResponses.get(0);  
//		}
		
		/**
		 * TODO: rules
		 * There are 4 codes related to PSA tests. The code to use depend on a combination of 2 fields out of 4. E.g. 
		 * Total PSA + Insured = one code, Total PSA + uninsured = another code.
         * I have also created OEI-18 for a eForm UI issue.  please take a look and let me know if you have a preferred solution.
		 */
		
		/**
		 * TODO:
		 * 
		 * 1. For Therapeutic Drug Monitoring tests, it seems to me that there is a separate lab test code for each drug that can be specified. 
		 *    As such, the EMR has to use the actual drug name to determine which lab test code to send to the eOrder service. Please confirm if this is 
		 *    the correct understanding. If the above is true, I assume the list of codes (for each drug) is in the eOrder Test Catalogue. 
		 *    I may need to come back to you if I have problems finding the required code.
         * 2. For the Chlamydia test, it seems that there are 2 possible lab test codes and if the source is specified as URINE, then we need to use 
         *    TR10690-6K and if the source is specified as something else, then we are supposed to use TR10690-6. Please confirm.
         *    And for both cases, the source info has to be included in a Questionnaire Response. Please confirm.
         * 3. For the GC test, again it seems that there are 2 possible lab test codes and if the source is specified as URINE, then we need to use 
         *    TR10714-4K and for other sources, we are supposed to use TR10714-4. Please confirm.
         *    And for both cases, the source info has to be included in a Questionnaire Response. Please confirm.
		 */
		
		// TODO: handle the rules listed above when mapping test codes

		// Resolving test excelleris code from properties file
		// for most lab tests on the Ontario Lab Req form, we can map the key to a unique code directly.
		// for a few selected tests, we need some additional steps
		
		
		String testCode = null;
		if (isComplexLabCodeMapping(key))
		{
			// complex mapping
			String derivedKey = null;
			// PSA
			if (key.equals("T_total_psa") || (key.equals("T_free_psa"))) {
				// see if corresponding insured/uninsured is selected to determine code
				if (valueMap.containsKey("T_psa_insured")) {
					derivedKey=key+"_insured";
				} else if (valueMap.containsKey("T_psa_uninsured")) {
					derivedKey=key+"_uninsured";
				}
				LOG.info("Key : " + derivedKey);
				testCode =  properties.getProperty(derivedKey);
			}
			// TODO: other complex mapping
			// TODO: handle test with source
			// - depends on source info GC, Chlamydia
			String source = null;
			String sourceKey = null;
			if (key.equals(CHLAMYDIA)) {
				// check the corresponding source
				sourceKey = "T_chlamydia_src";
				if (valueMap.containsKey(sourceKey)) {
					EFormValue eformValue = valueMap.get(sourceKey);
					source = eformValue.getVarValue();
					
					if (source.equals("URINE")) {
						derivedKey = "T_Chlamydia_Investigation_Ur";
						LOG.info("Key : " + derivedKey);
						testCode = properties.getProperty(derivedKey);
					} else {
						derivedKey = "T_Chlamydia_Investigation";
						LOG.info("Key : " + derivedKey);
						testCode = properties.getProperty(derivedKey);
					}
				} else {
					// no source value specified. - ERROR
					LOG.error("No source specified for:  " + key);
					throw new ServiceException("No source specified for:  " + key);
				}

				// set source info
				
				questionnaireResponse = createTestSourceQuestionnaireResponse(valueMap, testCode, CHLAMYDIA_SRC, itemUuid, questionnaireResponse);

			}
			if (key.equals(GC)) {
				// check the corresponding source
				sourceKey = "T_gc_src";
				if (valueMap.containsKey(sourceKey)) {
					EFormValue eformValue = valueMap.get(sourceKey);
					source = eformValue.getVarValue();
					if (source.equals("URINE")) {
						derivedKey = "T_N_gonorrhoeae_Invest_Urine";
						LOG.info("Key : " + derivedKey);
						testCode = properties.getProperty(derivedKey);
					} else {
						derivedKey = "T_N_gonorrhoeae_Investigatio";
						LOG.info("Key : " + derivedKey);
						testCode = properties.getProperty(derivedKey);
					}
				} else {
					// no source value specified. - ERROR
					LOG.error("No source specified for:  " + key);
					throw new ServiceException("No source specified for:  " + key);
				}
				
				// set source info
				questionnaireResponse = createTestSourceQuestionnaireResponse(valueMap, testCode, GC_SRC, itemUuid, questionnaireResponse);
			}
			
			if (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) {
				EFormValue eformValue = valueMap.get(key);
				source = eformValue.getVarValue();
				derivedKey =  convertKey(source);
				LOG.info("Key : " + derivedKey);
				testCode = properties.getProperty(derivedKey);
				// process the source info
				questionnaireResponse = createTDMQuestionnaireResponse(valueMap, testCode, questionnaireResponse, TDM_TIME_COLLECTED_1, TDM_TIME_OF_LAST_DOSE_1, TDM_TIME_OF_NEXT_DOSE_1);
			}
			
			if (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
				EFormValue eformValue = valueMap.get(key);
				source = eformValue.getVarValue();
				derivedKey =  convertKey(source);
				LOG.info("Key : " + derivedKey);
				testCode = properties.getProperty(derivedKey);
				// process the source info
				questionnaireResponse = createTDMQuestionnaireResponse(valueMap, testCode, questionnaireResponse, TDM_TIME_COLLECTED_2, TDM_TIME_OF_LAST_DOSE_2, TDM_TIME_OF_NEXT_DOSE_2);
			}
			
			if (NEONATAL_BILIRUBIN_TEST.equals(key)){				
				testCode = properties.getProperty(key);
				questionnaireResponse = createNeonatalBilirubinQuestionnaireResponse(valueMap, testCode, questionnaireResponse);
			}
			
			if (key.equals(VAGINAL_RECTAL_GROUP_B_STREP)) {
				testCode = properties.getProperty(key);
				questionnaireResponse = createTestSourceQuestionnaireResponse(valueMap, testCode, VAGINAL_RECTAL_GROUP_B_STREP_SRC, itemUuid, questionnaireResponse);
			}

			if (key.equals(WOUND)) {
				testCode = properties.getProperty(key);
				questionnaireResponse = createTestSourceQuestionnaireResponse(valueMap, testCode, WOUND_SRC, itemUuid,
						questionnaireResponse);
			}

			if (key.equals(OTHER_SWABS_PUS)) {
				testCode = properties.getProperty(key);
				questionnaireResponse = createTestSourceQuestionnaireResponse(valueMap, testCode, OTHER_SWABS_PUS_SRC, itemUuid, questionnaireResponse);
			}

            
//			if (questionnaireResponses.size() == 0 && questionnaireResponse != null) {
//				// only 1 for now and not a list of responses
//				questionnaireResponses.add(questionnaireResponse);	
//			}
			
		} else {		
			// simple mapping, just look it up from properties file 
			// TODO: this code only checks Ontario mapping codes - need to review when we add BC mapping codes

			// see if it is one of 'Other Tests'
			if (isOtherTests(key)) {
				// handle other test (find out the name of other tests)
				String derivedKey = null;
				//use naming convention to derive key from names of other tests.
				// for other tests, the test name is stored in the eForm value:value 
				EFormValue eformValue = valueMap.get(key);
				derivedKey = eformValue.getVarValue();
				derivedKey = convertKey(derivedKey);
				LOG.info("Key : " + derivedKey);
				testCode =  properties.getProperty(derivedKey);

			} else {
				LOG.info("Key : " + key);
				//LOG.info(properties);
				//LOG.info("eformValue.getVarValue(): " + eformValue.getVarValue());
				testCode = properties.getProperty(key);
			}			
		}
		
		return testCode;
	}
	
	private static boolean isComplexLabCodeMapping(String key) {
		// code to determine if lab code mapping is complex or not
		if (key.equals("T_total_psa") || 
		   (key.equals("T_free_psa")) ||
		   (key.equals("T_chlamydia")) || 
		   (key.equals("T_gc")) ||
		   (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) ||
		   (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) ||
		   (key.equals(NEONATAL_BILIRUBIN_TEST)) ||
		   (key.equals(VAGINAL_RECTAL_GROUP_B_STREP)) ||
		   (key.equals(WOUND)) ||
		   (key.equals(OTHER_SWABS_PUS))) {
			return true;
		} else {
		    return false;
		}
	}
	
	private static boolean isRequired(String key) {
		// code to determine if a form field needs to be mapped to a lab test code
		if (key.equals("T_psa_insured") || (key.equals("T_psa_uninsured")) || (key.equals(THERAPEUTIC_DRUG_MONITORING)) || isSource(key)) {
			return false;
		} else {
		    return true;
		}
	}
	
	private static boolean isOtherTests(String key) {
		// code to determine if this is one of 'Other Tests;
		if (key.contains(OTHER_TESTS)) {
			return true;
		} else {
		    return false;
		}
	}
	
	private static boolean isHardCodedTest(String key) {
		if (isOtherTests(key)) {
			return false;
		}
		return true;
	}
	
	private static boolean isOtherMicrobiology(String key) {
		// TODO: how to determine other microbiology test
		if (key.equals(OTHER_SWABS_PUS)) {
			return true;
		} else {
		    return false;
		}
	}
	
	private static boolean isSource(String key) {
		if (key.startsWith("T_") && (key.endsWith("_src") || key.contains("_src_")) && !key.equals(THERAPEUTIC_DRUG_MONITORING_1) && !key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
			return true;
		}
		return false;
	}
	
	private static String convertKey(String key) {
		// remove following characters
		String derivedKey = key;
		derivedKey = StringUtil.replaceAll(derivedKey, "(", "");
		derivedKey = StringUtil.replaceAll(derivedKey, ")", "");
		derivedKey = StringUtil.replaceAll(derivedKey, "[", "");
		derivedKey = StringUtil.replaceAll(derivedKey, "]", "");
		derivedKey = StringUtil.replaceAll(derivedKey, "+", "");
		derivedKey = StringUtil.replaceAll(derivedKey, ".", "");
		derivedKey = StringUtil.replaceAll(derivedKey, ",", "");

		// replace following characters
		derivedKey = StringUtil.replaceAll(derivedKey, "-", "_");
		derivedKey = StringUtil.replaceAll(derivedKey, "/", "_");
		derivedKey = StringUtil.replaceAll(derivedKey, " ", "_");
		derivedKey = "T_" + derivedKey;
		if (derivedKey.length() >28) {
		    derivedKey = derivedKey.substring(0, 28);
		}
		if(derivedKey.endsWith("_")) {
			derivedKey = derivedKey.substring(0, derivedKey.length()-1);
		}
		return derivedKey;
	}
	
	/**
	 * Create questionnaire response for neonatal bilirubin test
	 * 
	 * @param valueMap  eform value hash map
	 * @param itemIdentifier
	 * @param testCode
	 * @return  questionnaire response for the specific test
	 */
	private static QuestionnaireResponse createNeonatalBilirubinQuestionnaireResponse(Map<String, EFormValue> valueMap, String testCode, QuestionnaireResponse existingQR) {
		
		// need to send child age days, hours, practitioner phone and patient phone
		// as questionnaire response
		// child age days
		List<String> questions = new ArrayList<>();
		List<String> answers = new ArrayList<>();
		String question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_CHILD_AGE_DAYS);
		questions.add(question);
		String answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_DAYS);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
						
		// child age hours
		question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_CHILD_AGE_HOURS);
		questions.add(question);
		answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_HOURS);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		// practitioner phone
		question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_PRACTITIONER_PHONE_NO);
		questions.add(question);
		answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PRACTITIONER_PHONE);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		// patient phone
		question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_PATIENT_PHONE_NO);
		questions.add(question);
		answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PATIENT_PHONE);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);
		
		return questionnaireResponse;
	}
	
	/**
	 * Create questionnaire response for therapeutic drug monitoring test
	 * 
	 * @param valueMap  eform value hash map
	 * @param itemIdentifier
	 * @param testCode
	 * @return  questionnaire response for the specific test
	 */
	private static QuestionnaireResponse createTDMQuestionnaireResponse(Map<String, EFormValue> valueMap, String testCode, QuestionnaireResponse existingQR, String timeCollected, String timeLastDose, String timeNextDose) {
		
		// need to send time collected, time of last dose, time of next dose
		// as questionnaire response
		// time collected
		List<String> questions = new ArrayList<>();
		List<String> answers = new ArrayList<>();
		String question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_COLLECTED);
		questions.add(question);
		String answerValue = EFormValueHelper.getValue(valueMap, timeCollected);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
						
		// time of last dose
		question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_OF_LAST_DOSE);
		questions.add(question);
		answerValue = EFormValueHelper.getValue(valueMap, timeLastDose);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		// practitioner phone
		question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_OF_NEXT_DOSE);
		questions.add(question);
		answerValue = EFormValueHelper.getValue(valueMap, timeNextDose);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);
		
		return questionnaireResponse;
	}
	
	/**
	 * Create questionnaire response for test requiring source info
	 * 
	 * @param valueMap       eform value hash map
	 * @param itemIdentifier
	 * @param testCode
	 * @return questionnaire response for the specific test
	 */
	private static QuestionnaireResponse createTestSourceQuestionnaireResponse(
			Map<String, 
			EFormValue> valueMap, 
			String testCode, 
			String answerKey, 
			String questionLinkUuid, 
			QuestionnaireResponse existingQR) {
		
		// need to send source information
		// as questionnaire response
		
		// source
		List<String> questions = new ArrayList<>();
		List<String> answers = new ArrayList<>();
		String question = QuestionTextHelper.getSourceQuestionLinkId(testCode, questionLinkUuid);

		questions.add(question);
		String answerValue = EFormValueHelper.getValue(valueMap, answerKey);
		if (StringUtils.empty(answerValue)) {
			answerValue = ANSWER_UNKNOWN;
		}
		answers.add(answerValue);
		
		QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);
		
		return questionnaireResponse;
	}

	private static QuestionnaireResponse createQuestionnaireResponse(Map<String, EFormValue> valueMap,
			QuestionnaireResponse existingQR, Questionnaire questionnaire, Map<String, List<String>> testCodeToKeyAndItemIdMap) {
		if (valueMap == null) {
			LOG.error("Null value map cannot create response");
			throw new ServiceException("Null value map cannot create response");
		}

		if (questionnaire == null) {
			LOG.error("Null questionnaire cannot create response");
			throw new ServiceException("Null questionnaire cannot create response");
		}

		List<String> questions = new ArrayList<String>();
		List<String> answers = new ArrayList<String>();
		List<String> questionTexts = new ArrayList<String>();

		int j = 1;
		for (QuestionComponent question : questionnaire.getGroup().getQuestion()) {
			EFormValue answerValue = valueMap.get(QUESTION_RESPONSE_PREFIX + j);
			String testCode = getTestCodeFromLinkId(question.getLinkId());
			if (testCodeToKeyAndItemIdMap.containsKey(testCode) && testCodeToKeyAndItemIdMap.get(testCode) != null) {
				String key = testCodeToKeyAndItemIdMap.get(testCode).get(0);
				if (answerValue != null) {
					answers.add(answerValue.getVarValue());
					if (isHardCodedTest(key)) {
						String linkId = question.getLinkId().substring(0, question.getLinkId().lastIndexOf("|"));
						linkId = linkId + "|" + testCodeToKeyAndItemIdMap.get(testCode).get(1);
						questions.add(linkId);
					} else {
						questions.add(question.getLinkId());
					}
					questionTexts.add(question.getText());
				}
			}
			j++;
		}
		if (questions.size() > 0 && answers.size() > 0) {
			return FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers, questionTexts);
		} else {
			return existingQR;
		}
	}
	private static String getTestCodeFromLinkId(String linkId) {
		String[] linkIdArray = linkId.split("\\|");
		return linkIdArray[2];
	}
}
