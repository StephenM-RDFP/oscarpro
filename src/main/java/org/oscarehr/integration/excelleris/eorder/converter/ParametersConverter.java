package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Bundle;
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.instance.model.Bundle.BundleType;
import org.hl7.fhir.instance.model.DiagnosticOrder;
import org.hl7.fhir.instance.model.Encounter;
import org.hl7.fhir.instance.model.Location;
import org.hl7.fhir.instance.model.Parameters;
import org.hl7.fhir.instance.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.instance.model.Patient;
import org.hl7.fhir.instance.model.Practitioner;
import org.hl7.fhir.instance.model.Questionnaire;
import org.hl7.fhir.instance.model.QuestionnaireResponse;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class ParametersConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	public static Parameters toFhirObject(Map<String, EFormValue> valueMap, Questionnaire questionnaire) {
	
		Parameters parameters = new Parameters();
		LOG.debug("++++++++++++++++++++++++++++ eformValues BEGIN ++++++++++++++++++++++++");
		for (String key : valueMap.keySet()) {
			LOG.debug("key: " + key + "=" + valueMap.get(key).getVarValue());
		}
		LOG.debug("++++++++++++++++++++++++++++ eformValues END ++++++++++++++++++++++++");
		
		// OrderBundle
		ParametersParameterComponent component = new ParametersParameterComponent();
		// name
		component.setName(FhirParameterNames.ORDER_BUNDLE_NAME);
		parameters.addParameter(component);
		
		// bundle resource
		Bundle bundle = new Bundle();
		component.setResource(bundle);
		bundle.setType(BundleType.DOCUMENT);
		
		// location entry
		Location location = LocationConverter.toFhirObject(valueMap);
		BundleEntryComponent entry = new BundleEntryComponent();
		// Remove randomly generated location url
		String locationUrl = ResourceUrlHelper.getGuidResourceUrl();
		// inject location url into in memory EFormValue
		EFormValue eformValue = EFormValueHelper.createEFormValue(LocationPropertySet.LOCATION_URL, locationUrl);
		// add location_url to in-memory copy
		valueMap.put(LocationPropertySet.LOCATION_URL, eformValue);
		entry.setFullUrl(locationUrl);
		entry.setResource(location);
		bundle.addEntry(entry);
		
		// encounter entry
		Encounter encounter = EncounterConverter.toFhirObject(valueMap);
		entry = new BundleEntryComponent();
		String encounterUrl = ResourceUrlHelper.getGuidResourceUrl();
		// inject location url into in memory EFormValue
		eformValue = EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, encounterUrl);
		// add encounter_url to in-memory copy
		valueMap.put(EncounterPropertySet.ENCOUNTER_URL, eformValue);		
		entry.setFullUrl(encounterUrl);
		entry.setResource(encounter);
		bundle.addEntry(entry);
		
		// diagnostic order
		QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse();
		DiagnosticOrder diagnosticOrder = DiagnosticOrderConverter.toFhirObject(valueMap, questionnaireResponse,
				questionnaire);
		entry = new BundleEntryComponent();
		entry.setFullUrl(ResourceUrlHelper.getResourceUrl(DiagnosticOrder.class.getSimpleName(), null));
		entry.setResource(diagnosticOrder);
		bundle.addEntry(entry);
		
		// practitioner
		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap);
		entry = new BundleEntryComponent();
		eformValue = valueMap.get(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		if (eformValue == null) {
			LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
			throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
		}
		entry.setFullUrl(ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), eformValue.getVarValue()));		
		entry.setResource(practitioner);
		bundle.addEntry(entry);
		
		// automatically add orderer as copy to provider
		practitioner = PractitionerConverter.toFhirObject(valueMap);
		entry = new BundleEntryComponent();
		eformValue = valueMap.get(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		if (eformValue == null) {
			LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
			throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
		}
		entry.setFullUrl(ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), eformValue.getVarValue()));		
		entry.setResource(practitioner);
		bundle.addEntry(entry);
		
		// more copy to practitioners
		Integer unknownProviderIndex = 1;
		for (int ccProviderIndex = 1; ccProviderIndex <= PractitionerPropertyConfig.MAXIMUM_NUM_COPY_TO_PROVIDERS; ccProviderIndex++) {		
			PractitionerPropertyConfig propertyConfig = new PractitionerPropertyConfig(
					PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderLifeLabsIdPropertyNameByIndex(ccProviderIndex),
					PractitionerPropertySet.getCopyToProviderTitlePropertyNameByIndex(ccProviderIndex));
			if (PractitionerConverter.hasCopyToProviderData(valueMap, propertyConfig)) {
				// provider known by Excelleris
				String execellerisId = EFormValueHelper.getValue(valueMap, 
						PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex));
				if (execellerisId != null) {									
					Practitioner copyToPractitioner = PractitionerConverter.toFhirObject(valueMap, unknownProviderIndex, propertyConfig);
					entry = new BundleEntryComponent();
					entry.setFullUrl(ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), execellerisId));
					entry.setResource(copyToPractitioner);
					bundle.addEntry(entry);
				// provider unknown by excelleris
				} else {
					Practitioner copyToPractitioner = PractitionerConverter.toFhirObject(valueMap, unknownProviderIndex, propertyConfig);
					entry = new BundleEntryComponent();
					entry.setFullUrl(ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), PractitionerConverter.getUnknownProviderId(unknownProviderIndex)));
					entry.setResource(copyToPractitioner);
					bundle.addEntry(entry);
					unknownProviderIndex++;
				}
			}
		}		
		
		// patient
		Patient patient = PatientConverter.toFhirObject(valueMap);
		entry = new BundleEntryComponent();
		String patientExcellerisId = null;
		eformValue = valueMap.get(PatientPropertySet.EXCELLERIS_PATIENT_ID);		
		if (eformValue == null) {
			LOG.warn(PatientPropertySet.EXCELLERIS_PATIENT_ID + " is missing from the form.");
		} else {
			patientExcellerisId = eformValue.getVarValue();
		}
		entry.setFullUrl(ResourceUrlHelper.getResourceUrl(Patient.class.getSimpleName(), patientExcellerisId));
		entry.setResource(patient);
		bundle.addEntry(entry);
		
		// add questionnaire responses if available
		if (questionnaireResponse != null && questionnaireResponse.getGroup() != null
				&& questionnaireResponse.getGroup().getQuestion().size() > 0) {
				ParametersParameterComponent questionnaireComponent = new ParametersParameterComponent();
				questionnaireComponent.setName(FhirParameterNames.QUESTIONNAIRE_RESOURCE_NAME);
				questionnaireComponent.setResource(questionnaireResponse);
				parameters.addParameter(questionnaireComponent);
		}
		
		return parameters;
	}	
}
