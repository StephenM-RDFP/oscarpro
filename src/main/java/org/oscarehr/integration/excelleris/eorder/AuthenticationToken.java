package org.oscarehr.integration.excelleris.eorder;

import java.io.Serializable;

public class AuthenticationToken implements Serializable {
	
	private String lastMRHSession;
	private String mrhSession;
	private String f5ST;
	private String token;
	
	public AuthenticationToken() {
		super();
	}

	public AuthenticationToken(String lastMRHSession, String mrhSession, String f5st, String token) {
		super();
		this.lastMRHSession = lastMRHSession;
		this.mrhSession = mrhSession;
		f5ST = f5st;
		this.token = token;
	}

	public String getLastMRHSession() {
		return lastMRHSession;
	}

	public void setLastMRHSession(String lastMRHSession) {
		this.lastMRHSession = lastMRHSession;
	}

	public String getMrhSession() {
		return mrhSession;
	}

	public void setMrhSession(String mrhSession) {
		this.mrhSession = mrhSession;
	}

	public String getF5ST() {
		return f5ST;
	}

	public void setF5ST(String f5st) {
		f5ST = f5st;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((f5ST == null) ? 0 : f5ST.hashCode());
		result = prime * result + ((lastMRHSession == null) ? 0 : lastMRHSession.hashCode());
		result = prime * result + ((mrhSession == null) ? 0 : mrhSession.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		AuthenticationToken other = (AuthenticationToken) obj;
		if (f5ST == null) {
			if (other.f5ST != null) return false;
		} else if (!f5ST.equals(other.f5ST)) return false;
		if (lastMRHSession == null) {
			if (other.lastMRHSession != null) return false;
		} else if (!lastMRHSession.equals(other.lastMRHSession)) return false;
		if (mrhSession == null) {
			if (other.mrhSession != null) return false;
		} else if (!mrhSession.equals(other.mrhSession)) return false;
		if (token == null) {
			if (other.token != null) return false;
		} else if (!token.equals(other.token)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthenticationToken [lastMRHSession=" + lastMRHSession + ", mrhSession=" + mrhSession + ", f5ST=" + f5ST + ", token=" + token + "]";
	}
}
