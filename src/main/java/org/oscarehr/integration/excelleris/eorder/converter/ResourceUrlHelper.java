package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.oscarehr.integration.excelleris.eorder.ConfigureConstants;
import org.oscarehr.integration.excelleris.eorder.PropertyDefaultConstants;
import org.oscarehr.util.MiscUtils;

import oscar.OscarProperties;
import oscar.util.StringUtils;

public class ResourceUrlHelper {
	
	private static final Logger LOG = MiscUtils.getLogger();

	private static final String FHIR_NAMESPACE_URL = "http://hl7.org/fhir/v2/0203";
	private static final String RESOURCE_BASE_URL = "https://api.excelleris.com/";
	private static final String EORDER_PATH = "/eorder/";
	
	public static final String getApiVersion() {
		String version = null;
		if (OscarProperties.getInstance() != null) {
			version	= OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_API_VERSION_PARAM_NAME);
		}
		
		if (StringUtils.empty(version)) {
			version = PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION;
		}
		
		return version;
	}
	
	public static final String getFhirNameSpaceUrl() {
		return FHIR_NAMESPACE_URL;
	}
	
	public static String getOrderCodeValueSetSystemUrl() {	
			return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/ordercode";
	}
	
	public static String getResourceUrl(String resourceType, String resourceId) {
		String url = RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH; 
		
		url = url + resourceType;	
		if (resourceId != null) {
			url = url + "/" + resourceId;
		}
		
		return url;
	}
	
	public static final String getSubjectPregancyExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-subjectpregnancy";
	}
	
	public static final String getDiagnosisExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-subjectdiagnosis";
	}
	
	public static final String getDiagnosticCodeExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosis-code";
	}
	
	public static final String getClassificationExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-classification";
	}
	
	public static final String getItemPayerTypeValueSetExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/payer-type";
	}
	
	public static final String getItemPayerTypeExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-item-payertype";
		
	}
	
	public static final String getItemTieBreakerValueSetExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosticorder-item-tiebreaker";
	}
	
	public static final String getItemTieBreakerExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-item-tiebreaker";
	}
	
	public static final String getItemIsHardCodedTestValueSetExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosticorder-item-ishardcodedtest";
	}
	
	public static final String getItemIsHardCodedTestExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-item-ishardcodedtest";
	}
	
	public static final String getItemOtherTestValueSetExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosticorder-item-othertestfield";
	}
	
	public static final String getItemOtherTestExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-item-othertestfield";
	}
	
	public static final String getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosticorder-item-isothermicrobiology";
	}
	
	public static final String getItemOtherIsOtherMicrobilogyTestExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-item-isothermicrobiology";
	}
	
	public static final String getCopyToRecipientExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-copytorecipient";
	}
	
	public static final String getCollectionDateTimeExtensionUrl() {
		return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "diagnosticorder-collectiondatetime";
	}
	
	public static String getGuidResourceUrl() {
		return "urn:uuid:" + UUID.randomUUID();
	}
}
