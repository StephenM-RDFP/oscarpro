package org.oscarehr.integration.excelleris.eorder;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Bundle;
import org.hl7.fhir.instance.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.instance.model.DiagnosticOrder;
import org.hl7.fhir.instance.model.DocumentReference;
import org.hl7.fhir.instance.model.DocumentReference.DocumentReferenceContentComponent;
import org.hl7.fhir.instance.model.OperationOutcome;
import org.hl7.fhir.instance.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.instance.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.hl7.fhir.instance.model.Parameters;
import org.hl7.fhir.instance.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.instance.model.Questionnaire;
import org.hl7.fhir.instance.model.Resource;
import org.oscarehr.util.MiscUtils;

public class OrderResponseHelper {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	private static final String VALIDATION_BUNDLE_NAME = "ValidationBundle";
	private static final String OUTPUT_BUNDLE_NAME = "OutputBundle";
	
	private final Resource resource;
	private String orderId;
	private String patientId;
	/** Base 64 encoded pdf form */
	private String pdfForm;
	private List<String> errors = new ArrayList<>();
	private List<String> informationalMessages = new ArrayList<>();
	private boolean initialized = false;
	private Questionnaire questionnaire;
	
	public OrderResponseHelper(Resource resource) {
		this.resource = resource;
	}
	
	public List<String> getErrors() {
		
		if (!initialized) {
			parse();
		}
		
		return this.errors;
	}
	
	public List<String> getInformationalMessages() {
		
		if (!initialized) {
			parse();
		}
		
		return this.informationalMessages;
	}
	public String getOrderId() {
		
		if (!initialized) {
			parse();
		}
		
		return this.orderId;
	}
	
	public String getPdfLabRequisitionForm() {
		
		if (!initialized) {
			parse();
		}
		
		return this.pdfForm;
	}
	
	public String getPatientId() {
		
		if (!initialized) {
			parse();
		}
		
		return this.patientId;
	}
	
	public Questionnaire getQuestionnaire() {
		
		return this.questionnaire;
	}
	
	// ------------------------------------------------------------------------- Private Methods
	
	private void parse() {
		if (initialized) {
			return;
		}
		
		if (resource instanceof Bundle) {
			parseBundle((Bundle)resource);
		} else if (resource instanceof Parameters) {
			parseParameters((Parameters)resource);
		} else {
			// TODO:
			LOG.warn("Unknown resource type " + resource.getClass().getSimpleName());
		}
		
		this.initialized = true;
	}

	private void parseParameters(Parameters parameters) {

		for (ParametersParameterComponent parameter : parameters.getParameter()) {
			String name = parameter.getName();
			LOG.debug("parameter name " + parameter.getName());
			
			// output bundle
			if (OUTPUT_BUNDLE_NAME.equals(parameter.getName())) {
				Bundle bundle = (Bundle) parameter.getResource();
				for (BundleEntryComponent entry : bundle.getEntry()) {
					Resource entryResource = entry.getResource();
                    // LOG.debug("parseParameters: " + entryResource.getClass().getName());		
					// Questionnaire
					if (entryResource instanceof Questionnaire) {
						this.questionnaire = (Questionnaire) entryResource;
					}
					
					// Diagnostic Order
					if (entryResource instanceof DiagnosticOrder) {
						DiagnosticOrder diagnosticOrder = (DiagnosticOrder)entryResource;
						this.orderId = diagnosticOrder.getId();
						LOG.debug("orderId : " + this.orderId);
						String subjectReference = diagnosticOrder.getSubject().getReference();
						LOG.debug("subjectReference : " + subjectReference);
						int index = subjectReference.lastIndexOf("/");
						this.patientId = subjectReference.substring(index+1);
						LOG.debug("patientId : " + this.patientId);
					}
					
					// Document Reference
					if (entryResource instanceof DocumentReference) {
						DocumentReference documentReference = (DocumentReference) entryResource;
						List<DocumentReferenceContentComponent> content = documentReference.getContent();
						if (content != null && content.size() == 1) {
							this.pdfForm = content.get(0).getAttachment().getDataElement().getValueAsString();
						} else {
							if (content == null) {
								LOG.error("No pdf lab requisition form.");
								throw new ServiceException("No pdf lab requisition form.");
							}
						}
					}
				}
			} else if (VALIDATION_BUNDLE_NAME.equals(name)){
				
				// parameters
				Parameters bundleParameters = (Parameters)parameter.getResource();
				
				for(ParametersParameterComponent bundleParameter: bundleParameters.getParameter()) {
					Resource bundleResouce = bundleParameter.getResource();
					// order bundle
		            // LOG.debug("validation bundle - parameter " + bundleResouce.getClass().getName());
		
					if (bundleResouce instanceof Bundle) {
						parseBundle((Bundle)bundleResouce);
					}
					
					if (bundleResouce instanceof OperationOutcome) {
						parseOperationOutcome((OperationOutcome)bundleResouce);
					}
					// questionnaire
					if (bundleResouce instanceof Questionnaire) {
						this.questionnaire = (Questionnaire) bundleResouce;
					}
				}
			}
		}
	}

	private void parseBundle(Bundle bundle) {
		
		for (BundleEntryComponent entry : bundle.getEntry()) {
			Resource resource = entry.getResource();
            // LOG.debug("parseBundle: " + resource.getClass().getName());

			// Operation Outcome
			if (resource instanceof OperationOutcome) {
				OperationOutcome operationOutcome = (OperationOutcome)resource;
				for (OperationOutcomeIssueComponent issue : operationOutcome.getIssue()) {
					// TODO: do we want to retrieve other error messages
					errors.add(issue.getDetails().getText());
				}
			}
			// Order bundle
		}
	}
	private void parseOperationOutcome(OperationOutcome opOutcome) {
		// Operation Outcome

		for (OperationOutcomeIssueComponent issue : opOutcome.getIssue()) {
			// TODO: do we want to retrieve other error messages
			if (issue.getSeverity().equals(IssueSeverity.INFORMATION))
			{
			    informationalMessages.add(issue.getDetails().getText());
				LOG.debug("informational msg: " + issue.getDetails().getText());

				} else {
					errors.add(issue.getDetails().getText());
				}
				
			}
		}
	}
