package org.oscarehr.integration.excelleris.eorder;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.model.Parameters;
import org.hl7.fhir.instance.model.Questionnaire;
import org.hl7.fhir.instance.model.Resource;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.EOrder;
import org.oscarehr.integration.excelleris.eorder.converter.EFormValueHelper;
import org.oscarehr.integration.excelleris.eorder.converter.ParametersConverter;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import oscar.OscarProperties;

@Component
public class OrderServiceImpl implements OrderService {
	
	private static final Logger LOG = MiscUtils.getLogger();	
	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String HTTP_HEADER_EXCELLERIS_SUBMIT_METHOD = "X-Excelleris-Submit-Method";
	private static final String ORDER_NOW = "ordernow";
	private static final String JSON_FHIR_FORMAT = "application/json+fhir";
	private static final String JSON_FHIR_FORMAT_UTF_8 = "application/json+fhir; charset=utf-8";
	private static final String HTTP_HEADER_COOKIE = "Cookie";
	private static final String HTTP_HEADER_ACCEPT = "Accept";
	private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
	
	@Autowired
	private EOrderDao eOrderDao;

	public OrderServiceImpl() {

	}

	public OrderServiceImpl(EOrderDao eOrderDao) {
		this.eOrderDao = eOrderDao;
	}

	@Override
	public Resource createOrder(AuthenticationToken authenticationToken, EFormData eformData, List<EFormValue> eformValues) throws ServiceException {
		
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		EOrder eOrder = eOrderDao.findByExtEFormDataId(eformData.getId());
		// convert parameters to JSON

		JsonParser parser = new JsonParser();
//				ByteArrayOutputStream writer = new ByteArrayOutputStream();
		Questionnaire questionnaire = null;
		if (eOrder != null && eOrder.getQuestionnaire() != null) {
			try {
				questionnaire = (Questionnaire) parser.parse(eOrder.getQuestionnaire());
			} catch (Exception e) {
				LOG.error("Error converting Parameters to JSON.", e);
				throw new ServiceException("Error converting Parameters to JSON.");
			}
		}
		// convert EFormData to Parameters
		Parameters parameters = ParametersConverter.toFhirObject(valueMap, questionnaire);
		
		// convert parameters to JSON
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		try {
			parser.compose(writer, parameters);
		} catch (Exception e) {
			LOG.error("Error converting Parameters to JSON.", e);
			throw new ServiceException("Error converting Parameters to JSON.");
		}
		
		// invoke create order service
		HttpEntity responseEntity = null;
		HttpPost httpPost = null;
		HttpClient httpClient = null;
		try {
			Map<String, String> cookieMap = new HashMap<>();
			httpClient = HttpClientHelper.createHttpClient(cookieMap);
			
			String orderUrl = getCreateOrderURL();
			LOG.debug("Order URL: " + orderUrl);
			httpPost = new HttpPost(orderUrl);
			httpPost.addHeader(HTTP_HEADER_COOKIE, authenticationToken.getLastMRHSession());
			httpPost.addHeader(HTTP_HEADER_COOKIE, authenticationToken.getMrhSession());
			httpPost.addHeader(HTTP_HEADER_COOKIE, authenticationToken.getF5ST());
			httpPost.setHeader(AUTHORIZATION_HEADER, "Bearer " + authenticationToken.getToken());
			httpPost.setHeader(HTTP_HEADER_EXCELLERIS_SUBMIT_METHOD, ORDER_NOW);
			httpPost.setHeader(HTTP_HEADER_ACCEPT, JSON_FHIR_FORMAT);
			httpPost.setHeader(HTTP_HEADER_CONTENT_TYPE, JSON_FHIR_FORMAT_UTF_8);
			
//			for (Header header : httpPost.getAllHeaders()) {
//				LOG.debug(header.getName() + " : " + header.getValue());
//			}			
			
			String requestString = new String(writer.toByteArray(), "UTF-8");
			LOG.debug("****************** requestString: " + requestString);
			StringEntity entity = new StringEntity(requestString);
			
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			LOG.debug("StatusLine: " + response.getStatusLine());
			
			responseEntity = response.getEntity();
		} catch (ConnectTimeoutException ex) {
			LOG.warn("Connection timeout when connecting to Excelleris eOrder server.", ex);
			throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again or use the Print button to print the Lab Requisition without sending to eOrder.", ex);			
		} catch (Exception ex) {
			LOG.error("Error creating order", ex);
			if (ex.getMessage() != null && ex.getMessage().toLowerCase().contains("read timed out")) {
				throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again or use the Print button to print the Lab Requisition without sending to eOrder.", ex);
			}
			throw new ServiceException("Error creating order " + ex.getMessage());
		}
		
		// convert response to FHIR resource
		String result = null;
		try {
			result = EntityUtils.toString(responseEntity, "UTF-8");
			LOG.debug("~~~~~~~~~~~~~~~~~~~ result \n " + result);
			Resource resource = parser.parse(result);
			return resource;
		} catch (Exception e) {			
			LOG.error("Error parseing result", e);
			throw new ServiceException("Error occured in the server. Please try again later. If the problem persists, please contact your technical support.", e);
		}
	}
	
	
	// ---------------------------------------------------------------------- Private Methods
	
	/**
	 * Get Excelleris eOrder service creating order url
	 * @return
	 */
	private String getCreateOrderURL() {
		if (OscarProperties.getInstance() != null) {
			return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_ORDER_URL_PARAM_NAME);
		} else {
			LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_ORDER_URL");
			return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_ORDER_URL;
		}
	}
}
