package org.oscarehr.integration.excelleris.eorder.converter;

public class EncounterPropertySet {

	// Property names used for exchange information in-memory
	public static final String ENCOUNTER_URL = "encounter_url";
}
