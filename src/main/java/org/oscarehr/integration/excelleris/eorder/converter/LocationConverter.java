package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Location;
import org.oscarehr.common.model.EFormValue;

public class LocationConverter {
	
	public static Location toFhirObject(Map<String, EFormValue> valueMap) {
		// TODO: how to populate location
		// TODO: remove hard-coded identifier
		Location location = new Location();
		Identifier identifier = new Identifier();
		identifier.setValue("501");
		location.addIdentifier(identifier);
		return location;
	}

}
