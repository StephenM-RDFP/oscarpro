package org.oscarehr.integration.excelleris.eorder.converter;

public class QuestionTextHelper {
	
	private static final String LINK_ID_GUI = "GUI";
	private static final String LINK_ID_TEST_CODE = "TestCode";
	private static final String LINK_ID_SEPARATOR = "|";
	private static final String LINK_ID_GLOBAL = "Global";
	
	
	public static final String QUESTION_CHILD_AGE_DAYS = "ChildAgeDays";
	public static final String QUESTION_CHILD_AGE_HOURS = "ChildAgeHours";
	public static final String QUESTION_PRACTITIONER_PHONE_NO = "PractitionerNo";
	public static final String QUESTION_PATIENT_PHONE_NO = "PatientNo";
	
	public static final String QUESTION_TIME_COLLECTED = "Time Collected:";
	public static final String QUESTION_TIME_OF_LAST_DOSE = "Time of Last Dose:";
	public static final String QUESTION_TIME_OF_NEXT_DOSE = "Time of Next Dose:";
	
	
	public static final String QUESTION_SOURCE = "Source";
	
	public static String getQuestionLinkId(String testCode, String question) {
		return LINK_ID_GUI + LINK_ID_SEPARATOR + LINK_ID_TEST_CODE + LINK_ID_SEPARATOR + 
				testCode + LINK_ID_SEPARATOR + LINK_ID_GLOBAL + LINK_ID_SEPARATOR + "1" + LINK_ID_SEPARATOR + question;
	}

	public static String getSourceQuestionLinkId(String testCode, String questionLinkUuid) {
		
		return getQuestionLinkId(testCode, QUESTION_SOURCE + LINK_ID_SEPARATOR + questionLinkUuid);
	}
}
