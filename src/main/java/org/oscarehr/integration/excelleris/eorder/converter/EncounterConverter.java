package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Encounter;
import org.hl7.fhir.instance.model.Encounter.EncounterLocationComponent;
import org.hl7.fhir.instance.model.Encounter.EncounterState;
import org.hl7.fhir.instance.model.Reference;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class EncounterConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();

	public static Encounter toFhirObject(Map<String, EFormValue> valueMap) {
		
		Encounter encounter = new Encounter();
		EncounterLocationComponent location = new EncounterLocationComponent();
		Reference locationReference = new Reference();		
		String locationUrl = EFormValueHelper.getValue(valueMap, LocationPropertySet.LOCATION_URL);
		if (locationUrl == null) {
			LOG.error("Missing " + LocationPropertySet.LOCATION_URL + " eform value");
			throw new ServiceException("Missing " + LocationPropertySet.LOCATION_URL + " eform value");
		}
		locationReference.setReference(locationUrl);
		location.setLocation(locationReference);
		encounter.addLocation(location);
		encounter.setStatus(EncounterState.PLANNED);
		
		return encounter;
	}
}
