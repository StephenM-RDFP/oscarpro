package org.oscarehr.integration.excelleris.eorder.converter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.Questionnaire.QuestionComponent;
import org.oscarehr.common.dao.OrderLabTestCodeDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCode;
import org.oscarehr.integration.excelleris.eorder.HttpClientHelper;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Choice;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.api.QuestionType;
import org.oscarehr.util.MiscUtils;

import oscar.util.StringUtils;

public class QuestionConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();
	private static final String TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE = "/ExcellerisTestCodeMapping.properties";


	public static Question toApiObject(QuestionComponent fhirQuestion, Map<String, EFormValue> valueMap,
			OrderLabTestCodeDao orderLabTestCodeDao) {

		if (fhirQuestion == null) {
			LOG.error("Null Fhir Question");
			throw new ServiceException("Cannot convert null Fhir Question");
		}

		if (valueMap == null) {
			LOG.error("Null value map");
			throw new ServiceException("Cannot convert Fhir Question");
		}

		Question apiQuestion = new Question();
		apiQuestion.setType(QuestionType.valueOf(fhirQuestion.getType().toString()));

		apiQuestion.setQuestionText(fhirQuestion.getText());
		apiQuestion.setLinkId(fhirQuestion.getLinkId());
		ArrayList<Choice> choices = new ArrayList<Choice>();
		for (Coding coding : fhirQuestion.getOption()) {
			choices.add(new Choice(coding.getCode(), coding.getDisplay()));

		}
		apiQuestion.setChoices(choices);

		// Load test code mapping file
		InputStream inputStream = HttpClientHelper.class
				.getResourceAsStream(TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE);
		Properties properties = new Properties();
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			LOG.error("Error loading " + TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE, e);
			throw new ServiceException("Error loading " + TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE);
		}

		for (Entry<Object, Object> entry : properties.entrySet()) {
			if (apiQuestion.getTestCode().equals(entry.getValue())) {
				String hardCodedTestFieldId = (String) entry.getKey();
				if (valueMap.containsKey(hardCodedTestFieldId)) {
					apiQuestion.setHardCodedTestFieldId(hardCodedTestFieldId);
				}
				break;
			}
		}

		OrderLabTestCode orderLabTestCode = orderLabTestCodeDao.findByTestCode(apiQuestion.getTestCode());
		if (orderLabTestCode != null) {
			apiQuestion.setTestName(orderLabTestCode.getTestName());
			if (StringUtils.isNullOrEmpty(apiQuestion.getHardCodedTestFieldId())) {
				apiQuestion.setOtherTestFieldId(orderLabTestCode.getTestName());
			}
		}

		return apiQuestion;
	}
}
