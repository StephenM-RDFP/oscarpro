package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.model.Address;
import org.hl7.fhir.instance.model.ContactPoint;
import org.hl7.fhir.instance.model.DateType;
import org.hl7.fhir.instance.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.instance.model.HumanName;
import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Patient;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class PatientConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	public static Patient toFhirObject(Map<String, EFormValue> valueMap) {
			
		Patient patient = new Patient();
		
		String payer = null;
		if (valueMap.containsKey("EO_OHIP")) {
			payer = "OHIP";
		} else if (valueMap.containsKey("EO_WSIB")) {
			payer = "WSIB";
		} else  /*if (valueMap.containsKey("EO_Third_Party"))*/ {
			// not insured so has to be patient pay
			payer = "Patient";
		}
		
		// Identifier
		// hcn - required if not patient pay		
		String healthInsuranceNumber = EFormValueHelper.getValue(valueMap, PatientPropertySet.PAITENT_HEALTH_INSURANCE_NUMBER);
		if (healthInsuranceNumber == null && !"Patient".equals(payer)) {
			LOG.error("Missing patient's " + PatientPropertySet.PAITENT_HEALTH_INSURANCE_NUMBER);
			throw new ServiceException("Patient's " + PatientPropertySet.PAITENT_HEALTH_INSURANCE_NUMBER + " is required.");
		}
		
		// Health insurance number retrieved from database includes space + version number
		// remove spaces from health insurance number
		if (healthInsuranceNumber != null) {
			healthInsuranceNumber = healthInsuranceNumber.replace(" ", "");
		}
		
		// Excelleris does not accept null property represented as not setting the property as
		// specified by https://www.hl7.org/fhir/DSTU2/json.html
		// Workaround to put a special empty string in there and have the fhir-api-excelleris code
		// to output a ""
		
		if (healthInsuranceNumber == null) {
			healthInsuranceNumber = JsonParser.OUTPUT_NULL_AS_EMPTY_STRING;
		}
		
		if (healthInsuranceNumber != null) {
			Identifier identifier = FhirResourceHelper.createIdentifier(
					"hcn", OidConstants.HEALTH_INSURANCE_NUMBER_OID, healthInsuranceNumber, healthInsuranceNumber, "ON");
			patient.addIdentifier(identifier);
		}
		
		// Name
		String lastName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_LAST_NAME);
		if (StringUtils.isEmpty(lastName)) {
			LOG.warn("Missing patient's last name.");
			throw new ServiceException("Patient's last name is required.");
		}
		String firstName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_FIRST_NAME);
		if (StringUtils.isEmpty(firstName)) {
			LOG.warn("Missing patient's first name.");
			throw new ServiceException("Patient's first name is required.");
		}
		String middleName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_MIDDLE_NAME);
		
		// -- title
		String title = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_TITLE);
		if (StringUtils.isEmpty(title)) {
            // eOrder api requires the field even if it is not set, so we have to put UNK there to workaround
			title = "UNK";
		}
		HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
		patient.addName(name);
		
		// Gender
		String femaleValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER_FEMALE);
		// female
		if (femaleValue != null) {
			patient.setGender(AdministrativeGender.FEMALE);
		}
		// male
		String maleValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER_MALE);
		if (maleValue != null) {
			patient.setGender(AdministrativeGender.MALE);
		}
		if (StringUtils.isEmpty(femaleValue) && StringUtils.isEmpty(maleValue)) {
			String genderValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER);
			if (StringUtils.isEmpty(genderValue)) {
				LOG.warn("Missing patient's gender, mapped to unknown.");
				patient.setGender(AdministrativeGender.UNKNOWN);
			} else {
				if ("Male".equalsIgnoreCase(genderValue)) {
					patient.setGender(AdministrativeGender.MALE);
				} else if ("Female".equalsIgnoreCase(genderValue)) {
					patient.setGender(AdministrativeGender.FEMALE);
				} else {
					patient.setGender(AdministrativeGender.OTHER);
				}
			}
		}
		
		// Dob		
		String dob = EFormValueHelper.getValue (valueMap, PatientPropertySet.PATIENT_DOB);
		if (dob != null) {
			LOG.debug("Patient's DOB: " + dob);
			// dob from eForm is formatted in yyyy/MM/dd
			// Excelleris requires yyyy-MM-ddT00:00:00.000Z format
			String formattedDate = dob.replaceAll("/", "-");
			formattedDate = formattedDate + "T00:00:00.000Z";
			DateType dateType = new DateType();
			dateType.setValueAsString(formattedDate);
			patient.setBirthDateElement(dateType);
		} else {
			LOG.warn("Missing patient's date of birth.");
			throw new ServiceException("Patient's date of birth is required.");
		}
		
		// Address
		Address address = FhirResourceHelper.createAddress(valueMap);
		patient.addAddress(address);
		
		// Telecom
		String phoneNumber = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_PHONE);
		if (StringUtils.isEmpty(phoneNumber)) {
			LOG.warn("Missing patient's phone number.");
			throw new ServiceException("Patient's phone number is required.");
		}
		ContactPoint contactPoint = FhirResourceHelper.createContactPoint(valueMap);
		patient.addTelecom(contactPoint);
		
		// extension (optional)
		
		// Excelleris patient id
		String excellerisPatientId = EFormValueHelper.getValue(valueMap, PatientPropertySet.EXCELLERIS_PATIENT_ID);
		if (excellerisPatientId != null && excellerisPatientId.trim().length() > 0) {
			patient.setId(excellerisPatientId);
		} else {
			// Excelleris does not accept null property represented as not setting the property as
			// specified by https://www.hl7.org/fhir/DSTU2/json.html
			// Workaround to put a special empty string in there and have the fhir-api-excelleris code
			// to output a ""
			patient.setId(JsonParser.OUTPUT_NULL_AS_EMPTY_STRING);
		}
		
		return patient;
	}
}
