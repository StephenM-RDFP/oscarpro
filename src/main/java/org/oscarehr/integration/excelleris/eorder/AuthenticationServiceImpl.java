package org.oscarehr.integration.excelleris.eorder;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import oscar.OscarProperties;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

	private static final Logger LOG = MiscUtils.getLogger();

	private static final String LAST_MRH_SESSION_COOKIE_NAME = "LastMRH_Session";
	private static final String MRH_SESSION_COOKIE_NAME = "MRHSession";
	private static final String F5_ST_COOKIE_NAME = "F5_ST";

	public AuthenticationServiceImpl() {
		super();
	}

	@Override
	public AuthenticationToken authenticate() throws ServiceException {

		// Send request to launchpad with client certification to get cookies to use for login
		LOG.info("\n################################## Send request to LaunchPad ####################################\n");

		try {

			Map<String, String> cookieMap = new HashMap<>();
			HttpClient httpClient = HttpClientHelper.createHttpClient(cookieMap);

			HttpGet httpGet = new HttpGet(this.getLoginURL());
			HttpResponse response = httpClient.execute(httpGet);
			LOG.debug("StatusLine: " + response.getStatusLine());
			LOG.debug("cookieMap: " + cookieMap);

			// Invoke login service
			LOG.info("\n################################## LOGIN ####################################\n");

			String loginUrl = getLoginURL();
			LOG.debug("Login URL: " + loginUrl);
			HttpPost httpPost = new HttpPost(loginUrl);
			for (String key : cookieMap.keySet()) {
				httpPost.setHeader("Cookie", cookieMap.get(key));
			}

			UserCredential userCredential = new UserCredential(getUsername(), getPassword());
			ObjectMapper mapper = new ObjectMapper();
			String content = mapper.writeValueAsString(userCredential);
			//LOG.debug("content: " + content);
			StringEntity entity = new StringEntity(content);
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(entity);
			response = httpClient.execute(httpPost);

			HttpEntity responseEntity = response.getEntity();
			String token = "";
			if (responseEntity != null) {
				token = EntityUtils.toString(responseEntity, "UTF-8");
				LOG.debug("token: " + token);
			}
			LOG.debug("StatusLine: " + response.getStatusLine());
			
			return new AuthenticationToken(cookieMap.get(LAST_MRH_SESSION_COOKIE_NAME), cookieMap.get(MRH_SESSION_COOKIE_NAME), 
					cookieMap.get(F5_ST_COOKIE_NAME), token.replaceAll("\"", ""));

		} catch (ConnectTimeoutException ex) {
			LOG.warn("Connection timeout when connecting to Excelleris authentication server.", ex);
			throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again or use the Print button to print the Lab Requisition without sending to eOrder.", ex);			
		}
		catch (Exception ex) {
			LOG.error("Error login to Excelleris eOrder authentication server.", ex);
			if (ex.getMessage() != null && ex.getMessage().toLowerCase().contains("read timed out")) {
				throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again or use the Print button to print the Lab Requisition without sending to eOrder.", ex);
			}
			throw new ServiceException("Error login to Excelleris eOrder authentication server.", ex);
		}
	}

	// ---------------------------------------------------------------------------- Private Methods

	/**
	 * Get Excelleris eOrder service login url
	 */
	private String getLoginURL() {
		if (OscarProperties.getInstance() != null) {
			return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_LOGIN_URL_PARAM_NAME);
		} else {
			LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_LOGIN_URL");
			return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_LOGIN_URL;
		}
	}

	/**
	 * Get Excelleris eORder login user name
	 */
	private String getUsername() {
		if (OscarProperties.getInstance() != null) {
			return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_USER_PARAM_NAME);
		} else {
			LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_USER");
			return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_USER;
		}
	}

	/**
	 * Get Excelleris eOrder password
	 * @return
	 */
	private String getPassword() {
		if (OscarProperties.getInstance() != null) {
			return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_USER_PASSWORD_PARAM_NAME);
		} else {
			LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_USER_PASSWORD");
			return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_USER_PASSWORD;
		}
	}
}
