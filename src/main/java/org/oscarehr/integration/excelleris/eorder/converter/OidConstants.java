package org.oscarehr.integration.excelleris.eorder.converter;

public class OidConstants {

	public static final String HEALTH_INSURANCE_NUMBER_OID = "2.16.840.1.113883.3.1772.1.10";
	public static final String PRACTITIONER_EXCELLERIS_ID_OID = "2.16.840.1.113883.3.1772.1.0";
	public static final String PRACTITIONER_LIFE_LABS_ID_OID = "2.16.840.1.113883.3.1772.1.150";
}
