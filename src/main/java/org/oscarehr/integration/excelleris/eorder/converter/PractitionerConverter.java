package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.HumanName;
import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Practitioner;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class PractitionerConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	public static Practitioner toFhirObject(Map<String, EFormValue> valueMap) {

		Practitioner practitioner = new Practitioner();
		
		// Id
		String excellerisId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		if (excellerisId == null) {
			LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing.");
			throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is required.");
		}
		practitioner.setId(excellerisId);
		
		// Identifier
		// rover id
		Identifier excellerisIdIdentifier = FhirResourceHelper.createIdentifier(
				ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_EXCELLERIS_ID_OID, "PRN", excellerisId, "Rover");
		LOG.debug("excellerisId : " + excellerisId);
		practitioner.addIdentifier(excellerisIdIdentifier);
		
		// LifeLabs id
		String lifeLabsId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		if (lifeLabsId == null) {
			LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID + " is missing.");
			throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID + " is required.");
		}
		LOG.debug("lifeLabsId : " + lifeLabsId);
		Identifier lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
				ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", lifeLabsId, "Ontario Lifelabs");
		practitioner.addIdentifier(lifelabsIdIdentifier);
		
		// Name
		String lastName = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		if (lastName == null) {
			LOG.error(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is missing.");
			throw new ServiceException(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is required.");
		}		
		String firstName = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		if (firstName == null) {
			LOG.error(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is missing.");
			throw new ServiceException(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is required.");
		}
		
		// TODO: get middle name
		String middleName = null;

		String title = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_TITLE);
		if (title == null) {
			LOG.warn(PractitionerPropertySet.PRACTITIONER_TITLE + " is missing. Default to DR.");
			title = "DR.";
		}
		
		HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
		practitioner.setName(name);
	
		// Telecom (optional)
		// Address (optional)
		// Role (optional)
		
		return practitioner;
	}
	
	public static Practitioner toFhirObject(Map<String, EFormValue> valueMap, Integer unknownProviderIndex, PractitionerPropertyConfig propertyConfig) {

		Practitioner practitioner = new Practitioner();
		
		// Id
		String excellerisId = EFormValueHelper.getValue(valueMap, propertyConfig.getExcellerisIdPropertyName());
		if (excellerisId == null && unknownProviderIndex == null) {
			LOG.error(propertyConfig.getExcellerisIdPropertyName() + " is missing.");
			throw new ServiceException(propertyConfig.getExcellerisIdPropertyName() + " is required.");
		}
		if (excellerisId != null) {
			practitioner.setId(excellerisId);
		} else {
			practitioner.setId(getUnknownProviderId(unknownProviderIndex));
		}
		
		// Identifier
		// rover id
		if (excellerisId != null) {
		Identifier excellerisIdIdentifier = FhirResourceHelper.createIdentifier(
				ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_EXCELLERIS_ID_OID, "PRN", excellerisId, "Rover");
		LOG.debug("excellerisId : " + excellerisId);
		practitioner.addIdentifier(excellerisIdIdentifier);
		}
		
		// LifeLabs id
		String lifeLabsId = EFormValueHelper.getValue(valueMap, propertyConfig.getLifeLabsIdPropertyName());
		if (lifeLabsId == null && unknownProviderIndex == null) {
			LOG.error(propertyConfig.getLifeLabsIdPropertyName() + " is missing.");
			throw new ServiceException(propertyConfig.getLifeLabsIdPropertyName() + " is required.");
		}
		LOG.debug("lifeLabsId : " + lifeLabsId);
		Identifier lifelabsIdIdentifier = null;
		if (lifeLabsId != null) {
			lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
					ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", lifeLabsId, "Ontario Lifelabs");
		} else if (unknownProviderIndex != null) {
			lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
					ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", getUknownProviderCode(unknownProviderIndex), "Ontario Lifelabs");
		}
		practitioner.addIdentifier(lifelabsIdIdentifier);
		
		// Name
		String lastName = EFormValueHelper.getValue(valueMap, propertyConfig.getLastNamePropertyName());
		if (lastName == null) {
			LOG.error(propertyConfig.getLastNamePropertyName() + " is missing.");
			throw new ServiceException(propertyConfig.getLastNamePropertyName() + " is required.");
		}		
		String firstName = EFormValueHelper.getValue(valueMap, propertyConfig.getFirstNamePropertyName());
		if (firstName == null) {
			LOG.warn(propertyConfig.getFirstNamePropertyName() + " is missing. Using default value of UNK");
			firstName = "UNK";
		}
		
		// TODO: get middle name
		String middleName = null;

		String title = EFormValueHelper.getValue(valueMap, propertyConfig.getTitlePropertyName());
		if (title == null) {
			LOG.warn(propertyConfig.getTitlePropertyName() + " is missing. Default to DR.");
			title = "DR.";
		}
		
		HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
		practitioner.setName(name);
	
		// Telecom (optional)
		// Address (optional)
		// Role (optional)
		
		return practitioner;
	}
	
	public static boolean hasCopyToProviderData(Map<String, EFormValue> valueMap, PractitionerPropertyConfig propertyConfig) {

		String excellerisId = EFormValueHelper.getValue(valueMap, propertyConfig.getExcellerisIdPropertyName());
		String lifeLabsId = EFormValueHelper.getValue(valueMap, propertyConfig.getLifeLabsIdPropertyName());
		String lastName = EFormValueHelper.getValue(valueMap, propertyConfig.getLastNamePropertyName());
		String firstName = EFormValueHelper.getValue(valueMap, propertyConfig.getFirstNamePropertyName());
		String title = EFormValueHelper.getValue(valueMap, propertyConfig.getTitlePropertyName());
		
		if (!StringUtils.isEmpty(excellerisId) || !StringUtils.isEmpty(lifeLabsId) ||
			!StringUtils.isEmpty(lastName) || !StringUtils.isEmpty(firstName) ||
			!StringUtils.isEmpty(title)) {
			return true;
		}
				
		return false;
	}
	
	public static String getUnknownProviderId(Integer unknownProviderIndex) {
		return Integer.toString(unknownProviderIndex * 10);
	}
	
	public static String getUknownProviderCode(Integer unknownProviderIndex) {
		return "UNKNOWN" + (unknownProviderIndex - 1);
	}
}
