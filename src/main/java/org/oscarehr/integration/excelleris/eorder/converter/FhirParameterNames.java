package org.oscarehr.integration.excelleris.eorder.converter;

public class FhirParameterNames {
	
	public static final String ORDER_BUNDLE_NAME = "OrderBundle";
	public static final String QUESTIONNAIRE_RESOURCE_NAME = "QuestionnaireResponse";

}
