package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Address;
import org.hl7.fhir.instance.model.Address.AddressType;
import org.hl7.fhir.instance.model.Address.AddressUse;
import org.hl7.fhir.instance.model.BooleanType;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.ContactPoint;
import org.hl7.fhir.instance.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.instance.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.instance.model.DateTimeType;
import org.hl7.fhir.instance.model.Extension;
import org.hl7.fhir.instance.model.HumanName;
import org.hl7.fhir.instance.model.HumanName.NameUse;
import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Identifier.IdentifierUse;
import org.hl7.fhir.instance.model.QuestionnaireResponse;
import org.hl7.fhir.instance.model.QuestionnaireResponse.GroupComponent;
import org.hl7.fhir.instance.model.QuestionnaireResponse.QuestionAnswerComponent;
import org.hl7.fhir.instance.model.QuestionnaireResponse.QuestionComponent;
import org.hl7.fhir.instance.model.QuestionnaireResponse.QuestionnaireResponseStatus;
import org.hl7.fhir.instance.model.Reference;
import org.hl7.fhir.instance.model.StringType;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class FhirResourceHelper {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	public static Identifier createIdentifier(
			String codingSystem, String identifierSystem, String code, String value, String assignerValue) {
		Identifier identifier = new Identifier();
		// use
		identifier.setUse(IdentifierUse.OFFICIAL);
		// type		
		CodeableConcept typeValue = new CodeableConcept();
		Coding coding = new Coding();
		coding.setSystem(codingSystem);
		coding.setCode(code);
		typeValue.addCoding(coding);
		identifier.setType(typeValue);
		// system
		identifier.setSystem(identifierSystem);
		// value
		identifier.setValue(value);
		// assigner
		Reference assigner = new Reference();
		assigner.setDisplay(assignerValue);
		identifier.setAssigner(assigner);
		
		return identifier;
	}
	
	public static HumanName createHumanName(String firstName, String lastName, String middleName, String prefix) {
		
		HumanName name = new HumanName();
		name.setUse(NameUse.OFFICIAL);
		// family name
		name.addFamily(lastName);		
		// first name
		name.addGiven(firstName);
		// prefix
		name.addPrefix(prefix);
		
		// TODO: how to map middle name
		return name;
		
	}
	
	public static Address createAddress(Map<String, EFormValue> valueMap) {
		
		Address address = new Address();		
		// use
		address.setUse(AddressUse.HOME);
		// TODO: set type properly
		// type
		address.setType(AddressType.BOTH);
		// line
		String addressLine = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_LINE);
		if (StringUtils.isEmpty(addressLine)) {
			LOG.warn("Missing patient address line");
			throw new ServiceException("Patient address line is required.");
		}
		address.addLine(addressLine);
		// city
		String city = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_CITY);
		if (StringUtils.isEmpty(city)) {
			LOG.warn("Missing patient city");
			throw new ServiceException("Patient city required.");
		}
		address.setCity(city);
		// state
		String state = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
		address.setState(state);
		
		// use the state info to determine the country and postal code formatting
		
		// map country code based on the following rule: 
		// this needs to use CAN for Canada and USA for US) If they use the full name, it will take only 5 characters 
		// (this truncates the canadian orders that creates new patients to CANAD). This is because HL7 standards follow the ITU standard 
		// (http://hl7.org/fhir/extension-contactpoint-country.html
		// for now, we just need to handle Canada and US
		// country
		String country = null;
		if (state == null) {
			LOG.error("no province/state info");
			throw new ServiceException("no province/state info");
		}
		
		if (state.startsWith("US")) {
			country = "US";
		} else if (StringUtils.equalsIgnoreCase(state, "OT")) {
			country = "UNK"; // cannot derive country info
		} else {
			country = "CAN";
		}
		// postal code
		String postalCode = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE);
		if (StringUtils.isEmpty(postalCode)) {
			LOG.warn("Missing patient postal code");
			throw new ServiceException("Patient postal code required.");
		}
		if (country.equals("CAN")) {
            // make sure canadian postal code format is ok
			postalCode = postalCode.replace(" ", ""); // remove blanks if any
			
			if (postalCode.length() == 6) { // make sure there is a blank in the middle
			    postalCode = postalCode.substring(0, 3) + " " + postalCode.substring(3);
			} else {
				// wrong format 
				LOG.error("postal code format");
				throw new ServiceException("postal code format");
			}
		}
		address.setPostalCode(postalCode);

		address.setCountry(country);
		
		return address;
	}
	
	public static ContactPoint createContactPoint(Map<String, EFormValue> valueMap) {
		ContactPoint contactPoint = new ContactPoint();		
		contactPoint.setSystem(ContactPointSystem.PHONE);
		String phoneNumber = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_PHONE);
		contactPoint.setValue(phoneNumber);
		contactPoint.setUse(ContactPointUse.HOME);
		return contactPoint;
	}
	
	public static CodeableConcept createCodeableConcept(String valueSetUrl, String code) {		
		CodeableConcept codeableConcept = new CodeableConcept();
		Coding coding = new Coding();
		coding.setSystem(valueSetUrl);
		coding.setCode(code);
		codeableConcept.addCoding(coding);
		return codeableConcept;
	}
	
	public static Extension createCodeableConceptExtension(String valueSetUrl, String code, String extensionUrl) {
		Extension extension = new Extension();
		CodeableConcept codeableConcept = createCodeableConcept(valueSetUrl, code);
		extension.setValue(codeableConcept);
		extension.setUrl(extensionUrl);
		return extension;
	}
	
	public static Extension createBooleanExtension(Boolean value, String extensionUrl) {
		Extension extension = new Extension();
		BooleanType booleanValue = new BooleanType();
		booleanValue.setValue(value);
		extension.setValue(booleanValue);
		extension.setUrl(extensionUrl);
		return extension;
	}
	
	public static Extension createStringExtension(String value, String extensionUrl) {
		Extension extension = new Extension();
		StringType stringValue = new StringType();
		stringValue.setValue(value);
		extension.setValue(stringValue);
		extension.setUrl(extensionUrl);
		return extension;
	}
	
	public static Extension createDateTimeExtension(String value, String extensionUrl) {
		Extension extension = new Extension();
		DateTimeType dateTimeValue = new DateTimeType(value);
		extension.setValue(dateTimeValue);
		extension.setUrl(extensionUrl);
		return extension;
	}
	
	public static Extension createValueReferenceExtension(String referenceUrl, String extensionUrl) {
		Extension extension = new Extension();
		Reference reference = new Reference();
		reference.setReference(referenceUrl);		
		extension.setValue(reference);
		extension.setUrl(extensionUrl);
		return extension;
	}
	
	public static QuestionnaireResponse createQuestionnaireResponse(QuestionnaireResponse existingQR,
			List<String> questions, List<String> answers) {

		if ((questions != null && answers == null) || (questions == null && answers != null)) {
			LOG.warn("questions or answers is null");
			throw new ServiceException("questions or answers is null.");
		}

		if ((questions == null && answers == null) || (questions.isEmpty() && answers.isEmpty())) {
			LOG.warn("questions and answers are null or empty");
			throw new ServiceException("questions and answers are null or empty.");
		}

		if (questions.size() != answers.size()) {
			LOG.warn("questions and answers have different size.");
			throw new ServiceException("questions and answers have different size.");
		}

		QuestionnaireResponse questionnaireResponse = null;
		if (existingQR == null) {
			// create a new questionnaire response
			LOG.info("creating new questionnaire response");
			questionnaireResponse = new QuestionnaireResponse();
//			questionnaireResponse.setStatus(QuestionnaireResponseStatus.COMPLETED);
			GroupComponent group = new GroupComponent();
			questionnaireResponse.setGroup(group);
		} else {
			// use existing one
			LOG.info("use existing questionnaire response");
			questionnaireResponse = existingQR;
		}

		int index = 0;
		for (String question : questions) {
			String answer = answers.get(index);
			// question
			QuestionComponent questionComponent = new QuestionComponent();
			questionComponent.setLinkId(question);
			// answer
			QuestionAnswerComponent answerComponent = new QuestionAnswerComponent();
			StringType stringValue = new StringType(answer);
			answerComponent.setValue(stringValue);
			questionComponent.addAnswer(answerComponent);
			// add question to group
			questionnaireResponse.getGroup().addQuestion(questionComponent);
			index++;
		}
		questionnaireResponse.setStatus(QuestionnaireResponseStatus.COMPLETED);
		return questionnaireResponse;
	}

	public static QuestionnaireResponse createQuestionnaireResponse(QuestionnaireResponse existingQR,
			List<String> questions, List<String> answers, List<String> questionTexts) {
		
		if ((questions != null && answers == null) || (questions == null && answers != null)) {
			LOG.warn("questions or answers is null");
			throw new ServiceException("questions or answers is null.");
		}

		if ((questions == null && answers == null) || (questions.isEmpty() && answers.isEmpty())) {
			LOG.warn("questions and answers are null or empty");
			throw new ServiceException("questions and answers are null or empty.");
		}
		
		

		if (questions.size () != answers.size()) {
			LOG.warn("questions and answers have different size.");
			throw new ServiceException("questions and answers have different size.");
		}
		
		QuestionnaireResponse questionnaireResponse = null; 
		if (existingQR == null) {
			// create a new questionnaire response
			LOG.info("creating new questionnaire response");
			questionnaireResponse = new QuestionnaireResponse();
//			questionnaireResponse.setStatus(QuestionnaireResponseStatus.COMPLETED);
			GroupComponent group = new GroupComponent();
			questionnaireResponse.setGroup(group);
		} else {
			// use existing one
			LOG.info("use existing questionnaire response");
			questionnaireResponse = existingQR;
		}
		
		int index = 0;
		for (String question : questions) {
			String answer = answers.get(index);
			// question
			QuestionComponent questionComponent = new QuestionComponent();
			questionComponent.setLinkId(question);
			// answer
			QuestionAnswerComponent answerComponent = new QuestionAnswerComponent();
			StringType stringValue = new StringType(answer);
			answerComponent.setValue(stringValue);
			questionComponent.addAnswer(answerComponent);
			questionComponent.setText(questionTexts.get(index));
			// add question to group
			questionnaireResponse.getGroup().addQuestion(questionComponent);
			index++;
		}
		questionnaireResponse.setStatus(QuestionnaireResponseStatus.COMPLETED);
		return questionnaireResponse;
	}
}
