package org.oscarehr.integration.excelleris.eorder;

import java.util.List;

import org.hl7.fhir.instance.model.Resource;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;

public interface OrderService {
	
	/**
	 * Submit a diagnostic order in Excelleris eOrder system
	 * 
	 * @param eformData   eform data
	 * @return newly created diagnostic order 
	 */
	Resource createOrder(AuthenticationToken authenticationToken, EFormData eformData, List<EFormValue> eformValues) throws ServiceException;

}
