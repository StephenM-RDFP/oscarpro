package org.oscarehr.integration.excelleris.eorder;

import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Resource;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderGatewayImpl implements OrderGateway {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private OrderService orderService;
	
	public OrderGatewayImpl() {		
	}
	
	@Autowired
	public OrderGatewayImpl(AuthenticationService authenticationService, OrderService orderService) {
		super();
		this.authenticationService = authenticationService;
		this.orderService = orderService;
	}

	@Override
	public AuthenticationTokenAndResource createOrder(AuthenticationToken authenticationToken, EFormData eformData, List<EFormValue> eformValues) {
		
		LOG.trace("Calling OrderGatewayImpl.createOrder");
		long startTime = System.currentTimeMillis();
		long authenticationStartTime = startTime;
		
		AuthenticationTokenAndResource tokenAndResource = new AuthenticationTokenAndResource();
		// Do not reuse the token since token expiration interface is not well defined.
//		if (authenticationToken == null) {
			authenticationToken = authenticationService.authenticate();
			tokenAndResource.setAuthenticationToken(authenticationToken);
//		}
			
		long authenticationEndTime = System.currentTimeMillis();;
		
		
		Resource resource = orderService.createOrder(authenticationToken, eformData, eformValues);
		tokenAndResource.setResource(resource);
		
		long endTime = System.currentTimeMillis();
		
		LOG.info("+++++++++++++++++ Performance - Authentication : " + (authenticationEndTime - authenticationStartTime));
		LOG.info("+++++++++++++++++ Performance - Create Order : " + (endTime - authenticationEndTime));
		LOG.info("+++++++++++++++++ Performance - Overall : " + (endTime - startTime));
		
		return tokenAndResource;
	}
}
