package org.oscarehr.integration.excelleris.eorder;

public class PropertyDefaultConstants {

	public static final String EXCELLERIS_EORDER_BASE_URL_PARAM_NAME = "https://api.ontest.excelleris.com/";
	public static final String DEFAULT_EXCELLERIS_EORDER_LOGIN_URL = "https://api.ontest.excelleris.com/launchpad/eorderrest/Login";
	public static final String DEFAULT_EXCELLERIS_EORDER_ORDER_URL = "https://api.ontest.excelleris.com/launchpad/eorderrest/DiagnosticOrder";
	public static final String DEFAULT_EXCELLERIS_EORDER_BASE_URL = "https://api.ontest.excelleris.com/";
	public static final String DEFAULT_EXCELLERIS_EORDER_LAUNCHPAD_URL = "https://api.ontest.excelleris.com/launchpad";
	public static final String DEFAULT_EXCELLERIS_EORDER_USER = "cambian1";
	public static final String DEFAULT_EXCELLERIS_EORDER_USER_PASSWORD = "Cambian2020";
	public static final String DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE = "/ExcellerisEorderKeyStore.pkcs12";
	public static final String DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD = "PathCert";
	public static final String DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE = "/ExcellerisEorderTrustStore.pkcs12";
	public static final String DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD = "PathCert";
	public static final String DEFAULT_EXCELLERIS_EORDER_API_VERSION = "1.0";
	public static final int DEFAULT_EXCELLERIS_EORDER_CONNETION_TIMEOUT_MILLISECONDS = 60000;
}
