package org.oscarehr.integration.excelleris.eorder;

public interface SessionParamNameConstants {
	
	static final String EXCELLERIS_LAST_MRH_SESSION_PARAM_NAME = "EXCELLERIS_LastMRH_Session";
	static final String EXCELLERIS_MRH_SESSION_PARAM_NAME = "EXCELLERIS_MRHSession";
	static final String EXCELLERIS_F5_ST_PARAM_NAME = "EXCELLERIS_F5_ST";
	static final String EXCELLERIS_AUTHORIZATION_TOKEN = "EXCELLERIS_AUTHORIZATION_TOKEN";

}
