package org.oscarehr.integration.excelleris.eorder;

import java.util.List;

import org.hl7.fhir.instance.model.Parameters;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;

public interface OrderGateway {
	
	AuthenticationTokenAndResource createOrder(AuthenticationToken authenticationToken, EFormData eformData, List<EFormValue> eformValues);

}
