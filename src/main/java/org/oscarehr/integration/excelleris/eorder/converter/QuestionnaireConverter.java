package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.Questionnaire.QuestionComponent;
import org.oscarehr.common.dao.OrderLabTestCodeDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.api.Questionnaire;
import org.oscarehr.util.MiscUtils;

public class QuestionnaireConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();

	public static Questionnaire toApiObject(org.hl7.fhir.instance.model.Questionnaire fhirQuestionnaire,
			Map<String, EFormValue> valueMap, OrderLabTestCodeDao orderLabTestCodeDao) {
		
		if (fhirQuestionnaire == null) {
			LOG.error("Fhir Questionnaire is null");
			throw new ServiceException("Fhir Questionnaire is Null");
		}

		if (valueMap == null) {
			LOG.error("Null value map");
			throw new ServiceException("Cannot convert Fhir Questionnaire");
		}

		Questionnaire apiQuestionnaire = new Questionnaire();
		ArrayList<Question> questions = new ArrayList<Question>();
		
		for (QuestionComponent questionComponent : fhirQuestionnaire.getGroup().getQuestion()) {
			questions.add(QuestionConverter.toApiObject(questionComponent, valueMap, orderLabTestCodeDao));
		}
		
		apiQuestionnaire.setQuestions(questions);

		return apiQuestionnaire;
	}
}
