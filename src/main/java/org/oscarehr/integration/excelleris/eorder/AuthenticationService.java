package org.oscarehr.integration.excelleris.eorder;

public interface AuthenticationService {
	
	/**
	 * Authenticate with Excelleris eOrder authentication server using system user
	 */
	AuthenticationToken authenticate() throws ServiceException;

}
