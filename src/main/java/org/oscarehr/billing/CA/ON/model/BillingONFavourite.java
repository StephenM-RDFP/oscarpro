/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.billing.CA.ON.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;
import org.oscarehr.common.model.AbstractModel;

@Entity
@Table(name="billing_on_favourite")
public class BillingONFavourite extends AbstractModel<Integer> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;

	@Column(name="dx")
	private String dx = "";

	@Column(name="dx1")
	private String dx1 = "";

	@Column(name="dx2")
	private String dx2 = "";

	@Column(name="service_dx")
	private String serviceDx;

	@Column(name="provider_no")
	private String providerNo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	private int deleted;

	private String visitType = null;

	private String location = null;

	@Column(name = "sli_code")
	private String sliCode = "";

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "favourite_id", referencedColumnName = "id")
	@Where(clause = "deleted = 0")
	@NotFound(action = NotFoundAction.IGNORE)
	private List<BillingONFavouriteItem> billingItems = new ArrayList<BillingONFavouriteItem>();

	public Integer getId() {
    	return id;
    }

	public void setId(Integer id) {
    	this.id = id;
    }

	public String getName() {
    	return name;
    }

	public void setName(String name) {
    	this.name = name;
    }

	public String getDx() {
		return dx;
	}

	public void setDx(String dx) {
		this.dx = dx;
	}

	public String getDx1() {
		return dx1;
	}

	public void setDx1(String dx1) {
		this.dx1 = dx1;
	}

	public String getDx2() {
		return dx2;
	}

	public void setDx2(String dx2) {
		this.dx2 = dx2;
	}
	
	public String getServiceDx() {
    	return serviceDx;
    }

	public void setServiceDx(String serviceDx) {
    	this.serviceDx = serviceDx;
    }

	public String getProviderNo() {
    	return providerNo;
    }

	public void setProviderNo(String providerNo) {
    	this.providerNo = providerNo;
    }

	public Date getTimestamp() {
    	return timestamp;
    }

	public void setTimestamp(Date timestamp) {
    	this.timestamp = timestamp;
    }

	public int getDeleted() {
    	return deleted;
    }

	public void setDeleted(int deleted) {
    	this.deleted = deleted;
    }

	public String getVisitType() {
		return visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSliCode() {
		return sliCode;
	}

	public void setSliCode(String sliCode) {
		this.sliCode = sliCode;
	}

	public List<BillingONFavouriteItem> getBillingItems() {
		return billingItems;
	}

	public void setBillingItems(List<BillingONFavouriteItem> billingItems) {
		this.billingItems = billingItems;
	}
	
	public int getServiceCount() {
		String[] splitServiceDx = serviceDx.split("\\|");
		int count = 0;
		
		if (splitServiceDx.length % 3 == 0) { // Check for proper number of "|" in service code string
			count = splitServiceDx.length / 3;
		}
		if (!billingItems.isEmpty()) { // If joined table based BillingONFavouriteItem is not empty use that count
			count = billingItems.size();
		}
		
		return count;
	}
	
	public static final Comparator<BillingONFavourite> NAME_COMPARATOR = new Comparator<BillingONFavourite>() {
        public int compare(BillingONFavourite p1, BillingONFavourite p2) {
                return (p1.getName().compareTo(p2.getName()));
        }
};  

	@PostPersist
	public void postPersist() {
		for (BillingONFavouriteItem item : this.billingItems) {
			item.setFavouriteId(this.id);
		}
	}
}
