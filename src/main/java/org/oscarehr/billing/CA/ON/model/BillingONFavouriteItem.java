package org.oscarehr.billing.CA.ON.model;

import org.oscarehr.common.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="billing_on_favourite_item")
public class BillingONFavouriteItem extends AbstractModel<Integer> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "favourite_id")
    private Integer favouriteId;

    @Column(name = "service_code")
    private String serviceCode;

    @Column(name = "ser_num")
    private String serviceCount;

    @Column(name = "percent")
    private String favouritePercentage;
    
    private int deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(Integer favouriteId) {
        this.favouriteId = favouriteId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCount() {
        return serviceCount;
    }

    public void setServiceCount(String serviceCount) {
        this.serviceCount = serviceCount;
    }

    public String getFavouritePercentage() {
        return favouritePercentage;
    }

    public void setFavouritePercentage(String favouritePercentage) {
        this.favouritePercentage = favouritePercentage;
    }

    public Boolean isDeleted() {
        return deleted == 1;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted ? 1 : 0;
    }
}
