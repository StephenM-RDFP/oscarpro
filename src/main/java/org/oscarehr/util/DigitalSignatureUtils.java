/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.DigitalSignatureDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.common.model.UserProperty;
import oscar.OscarProperties;

public class DigitalSignatureUtils {

	private static Logger logger = MiscUtils.getLogger();

	public static final String SIGNATURE_REQUEST_ID_KEY = "signatureRequestId";

	public static String generateSignatureRequestId(String providerNo) {
		return (providerNo + System.currentTimeMillis());
	}

	public static String getTempFilePath(String signatureRequestId) {
		return (System.getProperty("java.io.tmpdir") + "/signature_" + signatureRequestId + ".jpg");
	}

	/**
	 * This method will check if digital signatures is enabled or not. It will only attempt to save it if it's enabled.
	 * 
	 * @param demographicId of the owner of this signature
	 * @throws IOException if missing or error in image when one was expected
	 */
	public static DigitalSignature storeDigitalSignatureFromTempFileToDB(LoggedInInfo loggedInInfo, String signatureRequestId, int demographicId) {
		DigitalSignature digitalSignature = null;

		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			String filename = DigitalSignatureUtils.getTempFilePath(signatureRequestId);
			digitalSignature = saveSignature(loggedInInfo, filename, demographicId);
		}

		return (digitalSignature);
	}
	
	public static DigitalSignature storeSignatureStamp(LoggedInInfo loggedInInfo, int demographicId) {
		DigitalSignature digitalSignature = null;
		
		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
			UserProperty signatureProperty = userPropertyDao.getProp(loggedInInfo.getLoggedInProviderNo(), UserProperty.PROVIDER_CONSULT_SIGNATURE);
			if (signatureProperty != null) {
				String filepath = OscarProperties.getInstance().getProperty("eform_image");
				String filename = filepath + (!filepath.endsWith("/") ? "/" : "") +  signatureProperty.getValue();
				
				digitalSignature = saveSignature(loggedInInfo, filename, demographicId);
			}
		}
		
		return digitalSignature;
	}
	
	private static DigitalSignature saveSignature(LoggedInInfo loggedInInfo, String filename, int demographicId) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(filename);
			byte[] image = new byte[1024 * 256];
			fileInputStream.read(image);

			DigitalSignature digitalSignature = new DigitalSignature();
			digitalSignature.setDateSigned(new Date());
			digitalSignature.setDemographicId(demographicId);
			digitalSignature.setFacilityId(loggedInInfo.getCurrentFacility().getId());
			digitalSignature.setProviderNo(loggedInInfo.getLoggedInProviderNo());
			digitalSignature.setSignatureImage(image);

			DigitalSignatureDao digitalSignatureDao = (DigitalSignatureDao) SpringUtils.getBean("digitalSignatureDao");
			digitalSignatureDao.persist(digitalSignature);

			return (digitalSignature);
		} catch (FileNotFoundException e) {
			logger.debug("Signature file not found. User probably didn't collect a signature.", e);
			return null;
		} catch (Exception e) {
			logger.error("UnexpectedError.", e);
			return null;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					logger.error("Unexpected error.", e);
				}
			}
		}
	}

	/**
	 * Updates a signature that already exists in the database
	 * @param loggedInInfo Logged in info of the logged in user
	 * @param signatureId The id of the signature to update
	 * @param filePath The path to the temporary file that the new signature is stored in
	 * @return The updated DigitalSignature
	 */
	public static DigitalSignature updateSignature(LoggedInInfo loggedInInfo, Integer signatureId, String filePath) {
		DigitalSignature signature = null;
		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			DigitalSignatureDao digitalSignatureDao = (DigitalSignatureDao) SpringUtils.getBean("digitalSignatureDao");

			String filename = getTempFilePath(filePath);
			try (FileInputStream fileInputStream = new FileInputStream(filename)) {
				byte[] image = new byte[1024 * 256];
				fileInputStream.read(image);

				signature = digitalSignatureDao.find(signatureId);
				signature.setSignatureImage(image);
				digitalSignatureDao.merge(signature);
			} catch (IOException e) {
				logger.error("Could not update signature " + signatureId);
			}
		}
		return signature;
	}
}
