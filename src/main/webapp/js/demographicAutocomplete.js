function setupDemoAutoCompletion(selectedDemos, docId, flagToMrp) {
    if(jQuery("#autocompletedemo" + docId) ){

        var url;
        if( jQuery("#activeOnly" + docId).is(":checked") ) {
            url = window.contextpath + "/demographic/SearchDemographic.do?jqueryJSON=true&activeOnly=" + jQuery("#activeOnly" + docId).val();
        }
        else {
            url = window.contextpath + "/demographic/SearchDemographic.do?jqueryJSON=true";
        }

        jQuery( "#autocompletedemo" + docId ).autocomplete({
            source: url,
            minLength: 2,

            focus: function( event, ui ) {
                jQuery( "#autocompletedemo" + docId ).val( ui.item.label );
                return false;
            },
            select: function(event, ui) {
                jQuery( "#autocompletedemo" + docId ).val(ui.item.label);
                jQuery( "#demofind" + docId).val(ui.item.value);
                jQuery( "#demofindName" + docId ).val(ui.item.formattedName);
                selectedDemos.push(ui.item.label);
                console.log(ui.item.providerNo);
                if(flagToMrp &&  ui.item.providerNo != undefined && ui.item.providerNo != null && ui.item.providerNo != "" && ui.item.providerNo != "null" ) {
                    addDocToList(ui.item.providerNo, ui.item.provider + " (MRP)", "" + docId);
                }
                if( ui.item.cust1 != undefined && ui.item.cust1 != null &&ui.item.cust1 != "" && ui.item.cust1 != "null" ) {
                    addDocToList(ui.item.cust1, ui.item.cust1Name + " (Alt. Provider 1)", "" + docId);
                }
                if( ui.item.cust2 != undefined && ui.item.cust2 != null &&ui.item.cust2 != "" && ui.item.cust2 != "null" ) {
                    addDocToList(ui.item.cust2, ui.item.cust2Name + " (Alt. Provider 2)", "" + docId);
                }
                if( ui.item.cust4 != undefined && ui.item.cust4 != null &&ui.item.cust4 != "" && ui.item.cust4 != "null" ) {
                    addDocToList(ui.item.cust4, ui.item.cust4Name + " (Alt. Provider 3)", "" + docId);
                }

                //enable Save button whenever a selection is made
                jQuery('#save' + docId).removeAttr('disabled');
                jQuery('#saveNext' + docId).removeAttr('disabled');

                jQuery('#msgBtn_' + docId).removeAttr('disabled');
                jQuery('#mainTickler_' + docId).removeAttr('disabled');
                jQuery('#mainEchart_' + docId).removeAttr('disabled');
                jQuery('#mainMaster_' + docId).removeAttr('disabled');
                jQuery('#mainApptHistory_' + docId).removeAttr('disabled');
                return false;
            }
        });
    }
}

