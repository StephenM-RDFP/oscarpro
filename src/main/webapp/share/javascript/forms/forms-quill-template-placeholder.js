let QuillEmbed = Quill.import('blots/embed');
class TemplatePlaceholder extends QuillEmbed {
    static create(value) {
        let node = super.create(value);

        // Set node class
        node.setAttribute('class', 'quill-placeholder quill-placeholder-' + value.subject);
        node.setAttribute('data-subject', value.subject);
        node.setAttribute('data-marker', value.marker);
        node.setAttribute('data-title', value.title);
        node.setAttribute('title', value.title);

        node.innerHTML = value.title;

        return node;
    }
    
    static value(node) {
        return {
            subject: node.getAttribute('data-subject'),
            marker: node.getAttribute('data-marker'),
            title: node.getAttribute('data-title')
        };
    }
}
TemplatePlaceholder.blotName = 'TemplatePlaceholder';
TemplatePlaceholder.tagName = 'span';
Quill.register({
    'formats/TemplatePlaceholder': TemplatePlaceholder
});

