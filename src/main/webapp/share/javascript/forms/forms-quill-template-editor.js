let QuillParchment = Quill.import('parchment');
class TemplatePlaceholderQuill {
    constructor(formId, shortCodes, demographicPlaceholderValues) {
        let quillElement = document.getElementById(formId);
        if (!quillElement) {
            console.error(`TemplatePlaceholderQuill: element \'${formId}\' does not exist`)
        }
        
        this.quillElement = quillElement;
        this.quill = new Quill(this.quillElement, {theme: 'snow', modules : { toolbar: this.getDefaultToolbarOptions()}});
        
        this.setShortCodes(shortCodes);
        this.setResolvedPlaceHolderValues(demographicPlaceholderValues);
        
        let boundFillInKeyHandler = this.fillInKeyHandler.bind(this);
        this.quill.keyboard.addBinding({key: 115}, boundFillInKeyHandler); // bind F4 key
        
        // // Bind # to placeholder lookup
        // let addPlaceholderKeyHandler = this.addPlaceholderKeyHandler.bind(this);
        // this.quill.keyboard.addBinding({key: '3', shiftKey: true}, addPlaceholderKeyHandler); // bind # key

        let boundImageHandler = this.imageHandler.bind(this);
        this.quill.getModule('toolbar').addHandler('image', boundImageHandler);
    }
    
    setShortCodes(shortCodes) {
        this.shortCodes = shortCodes || [];
        if (this.shortCodes.size > 0) {
            let boundShortCodeHandler = this.shortCodeHandler.bind(this);
            this.quill.keyboard.addBinding({key: 220}, boundShortCodeHandler); // bind slash character '\'
        }
    }
    setResolvedPlaceHolderValues(demographicPlaceholderValues) {
        this.demographicPlaceholderValues = demographicPlaceholderValues || [];
        this.updateDomWithResolvedPlaceHolderValues();
    }
    
    updateDomWithResolvedPlaceHolderValues() {
        for (let i = 0; i < this.demographicPlaceholderValues.length; i++) {
            let resolvedPlaceholder = this.demographicPlaceholderValues[i];
            let nodes = document.querySelectorAll(`[data-marker="${resolvedPlaceholder['marker']}"]`);
            for (let j = 0; j < nodes.length; j++) {
                let blot = QuillParchment.find(nodes[j]);
                if (resolvedPlaceholder['isBlock'] === 'true') {
                    // block placeholders are allowed newlines
                    blot.domNode.innerHTML = resolvedPlaceholder['value'].replace(/\\n/g, '\n')
                } else {
                    blot.domNode.innerHTML = `<span contenteditable="false">${resolvedPlaceholder['value']}</span>`;
                }
            }
        }
    }
    
    setContents(newContent) {
        this.quill.setContents(newContent);
        this.updateDomWithResolvedPlaceHolderValues();
    }
    getContents() {
        return this.quill.getContents();
    }
    getContentsWithImagePlaceholders() {
        let delta = this.quill.getContents();
        let deltaWithImagePlaceholders = { ops: [] };
        for (let i = 0; i < delta.ops.length; i++) {
            let richTextItem = delta.ops[i];
            if (richTextItem.insert['ImagePlaceholder'] != null) {
                let imagePlaceholder = richTextItem.insert.ImagePlaceholder;
                // imagePlaceholder.data = '\$\{' + richTextItem.insert.ImagePlaceholder.identifier + '\}';
                deltaWithImagePlaceholders.ops.push({'insert': { 'ImagePlaceholder': { 'identifier' : imagePlaceholder.identifier, 'data' : '\$\{' + imagePlaceholder.identifier + '\}'}}});
            } else { // no template or image, just add to formatted delta
                deltaWithImagePlaceholders.ops.push(richTextItem);
            }
        }
        return deltaWithImagePlaceholders;
    }
    getText() {
        return this.quill.getText().trim();
    }
    getTextWithTemplatePlaceholderIndexes() {
        return this.getContents().map(function (op) {
            return typeof op.insert === 'string' ? op.insert : '#'
        }).join('');
    }
    getHtml() {
        return this.quill.root.innerHTML;
    }
    
    getTemplatePlaceholders() {
        let delta = this.quill.getContents();
        return delta.ops.filter(item => (item.insert.TemplatePlaceholder != null || item.insert.TemplateBlockPlaceholder != null));
    }
    getTemplatePlaceholderMarkers() {
        let placeholders =  this.getTemplatePlaceholders();
        let placeholderMarkers = [];
        for (let i = 0; i < placeholders.length; i++) {
            if (placeholders[i].insert['TemplatePlaceholder']) {
                placeholderMarkers.push(placeholders[i].insert['TemplatePlaceholder'].marker);
            } else if (placeholders[i].insert['TemplateBlockPlaceholder']) {
                placeholderMarkers.push(placeholders[i].insert['TemplateBlockPlaceholder'].marker);
            }
        }
        return placeholderMarkers;
    }
    getDocumentImages() {
        let delta = this.quill.getContents();
        return delta.ops.filter(item => item.insert.ImagePlaceholder != null);
    }
    fetchAndApplyTemplatePlaceHoldersWithFilledValues(demographicNo, endpointUrl, csrfToken) {
        // Get list of needed placeholder keys
        let templatePlaceholderMarkers = this.getTemplatePlaceholderMarkers();

        // Ajax request to fetch data using keys
        const httpRequest = new XMLHttpRequest();
        const formData = new FormData();
        formData.append('demographicNo', demographicNo);
        formData.append('fieldsToPullListString', templatePlaceholderMarkers.join(';'));
        formData.append(csrfToken['name'], csrfToken['value']);
        
        // bind events
        let onSendRequestSuccessEvent = this.onSendRequestSuccess.bind(this);
        httpRequest.addEventListener('load', onSendRequestSuccessEvent);
        let onSendRequestErrorEvent = this.onSendRequestError.bind(this);
        httpRequest.addEventListener('error', onSendRequestErrorEvent);

        httpRequest.open('POST', endpointUrl + '?method=getTemplatePlaceHolderValues');
        httpRequest.send(formData);
    }

    onSendRequestSuccess(event) {
        let data = JSON.parse(event.target.responseText);
        if (data['success'] === true) {
            // Set data values into placeholders innerHTML
            this.setResolvedPlaceHolderValues(data['resolvedPlaceholders']);
            this.updateDomWithResolvedPlaceHolderValues();
        }
    }
    onSendRequestError(event) {
        console.log(event);
    }
    
    getFormattedHtmlAndText(headerHtml, footerHtml) {
        let delta = this.quill.getContents();
        let richTextDeltaFormatted = { ops: [] };
        for (let i = 0; i < delta.ops.length; i++) {
            let richTextItem = delta.ops[i];
            if (richTextItem.insert['TemplatePlaceholder'] != null) { // replace value and add as standard delta input
                let fieldName = richTextItem.insert.TemplatePlaceholder.marker;
                let resolvedDelta = {insert: `\$\{${fieldName}\}`};
                richTextDeltaFormatted.ops.push(resolvedDelta);
            } else if (richTextItem.insert['TemplateBlockPlaceholder'] != null) {
                let fieldName = richTextItem.insert.TemplateBlockPlaceholder.marker;
                let resolvedDelta = {insert: `\$\{${fieldName}\}`};
                richTextDeltaFormatted.ops.push(resolvedDelta);
            } else if (richTextItem.insert['ImagePlaceholder'] != null) {
                let imageIdentifier = richTextItem.insert.ImagePlaceholder.identifier;
                let resolvedDelta = {insert: `\$\{${imageIdentifier}\}`};
                richTextDeltaFormatted.ops.push(resolvedDelta);
            } else { // no template or image, just add to formatted delta
                richTextDeltaFormatted.ops.push(richTextItem);
            }
        }
        this.quill.setContents(richTextDeltaFormatted);

        if (headerHtml === null) { headerHtml = ''; }
        if (footerHtml === null) { footerHtml = ''; }
        let contentHtml = this.getHtml();
        let css = 'body { font-family: sans-serif; tab-size: 4; font-size: 13px; } p, ol, ul, pre, blockquote, h1, h2, h3, h4, h5, h6 { margin: 0; padding: 0; } @page { @top-center { content: element(header); } } #pageHeader{ position: running(header); span.tab { display: inline-block; } }';
        let formattedHtml = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>Smart Encounter Printout</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><style type="text/css">${css}</style></head><body><div class="header">${headerHtml}</div><div class="content">${contentHtml}</div><div class="footer">${footerHtml}</div></body></html>`;
        
        // let formattedHtml = htmlPrefix + this.getHtml() + htmlSuffix;
        let formattedText = this.getText();
        this.quill.setContents(delta);

        return {html: formattedHtml, text: formattedText};
    }

    insertTextAtCaret(textToInsert) {
        let caretPosition = this.quill.getSelection(true);
        this.quill.insertText(caretPosition, textToInsert);
    }

    insertPlaceholderAtCaret(placeholderToInsert) {
        let caretPosition = this.quill.getSelection(true);
        
        this.quill.insertEmbed(caretPosition.index, placeholderToInsert, Quill.sources.USER);
        if ('DIV' === placeholderToInsert.nodeName) {
            this.quill.setSelection(caretPosition.index + 1, Quill.sources.USER);
        } else {
            this.quill.insertText(caretPosition.index + 1, ' ', Quill.sources.USER);
            this.quill.setSelection(caretPosition.index + 2, Quill.sources.USER);
        }
    }

    shortCodeHandler(range, context) {
        let prefix = context.prefix.trim();
        let shortCodeString = prefix;
        if (prefix.includes(' ')) {
            shortCodeString = prefix.substring(prefix.lastIndexOf(' ')).trim();
        }
        let shortCodeText = this.shortCodes.get(shortCodeString.toLowerCase());
        if (shortCodeText) {
            this.quill.deleteText(range.index - shortCodeString.length, shortCodeString.length);
            this.quill.insertText(range.index - shortCodeString.length, shortCodeText);
        } else { // insert character
            this.quill.insertText(range.index, '\\');
        }
    };
    
    fillInKeyHandler(range, context) {
        let text = this.getTextWithTemplatePlaceholderIndexes();
        
        // get index of next fill in marker
        let indexToSelect = text.indexOf('«»', range.index);
        if (indexToSelect === range.index) {
            // selecting existing selection, add 1 to index and find next
            indexToSelect = text.indexOf('«»', range.index + 1);
        }
        // if no «» is found, look from the start
        if (indexToSelect === -1) {
            indexToSelect = text.indexOf('«»');
        }
        this.quill.setSelection(indexToSelect, 2, Quill.sources.USER);
    };

    // addPlaceholderKeyHandler(range, context) {
    //     let cursorLocation = this.quill.getBounds(range.index, 0);
    //
    //     if (context.suffix != null && context.prefix !== '') {
    //         let placeholderTooltip = new CustomizableTooltip('none', this.quill, {
    //             inputLabel: 'Template Name:',
    //             actionText: 'Add', 
    //             onAction: this.onTemplateSearchAction,
    //             searchText: context.prefix.substring(0, 1),
    //             searchMethod: onAddPlaceholder
    //         });
    //         placeholderTooltip.show();
    //         // let placeholderTooltip = new CustomizableTooltip('image' , this.quill , {
    //         //     inputLabel: 'Image:' ,
    //         //     inputPlaceholer: 'Image URL...' ,
    //         //     actionText: 'Insert',
    //         //     hideOnTyping: true,
    //         //     clearAfterHide: true
    //         // });
    //         // placeholderTooltip.position(cursorLocation);
    //         placeholderTooltip.createTooltipList();
    //     }
    //
    //
    //
    //     // document.body.appendChild(placeholderSearch);\
    //     this.quill.insertText(range.index, '#');
    // };

    imageHandler() {
        let thisQuill = this.quill;
        let thisCreateUuid = this.createUuid;
        let fileInput = this.quillElement.querySelector('input.ql-image[type=file]');
        if (fileInput == null) {
            fileInput = document.createElement('input');
            fileInput.setAttribute('type', 'file');
            fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon');
            fileInput.style.display = 'none';
            fileInput.classList.add('ql-image');
            fileInput.addEventListener('change', function () {
                if (fileInput.files != null && fileInput.files[0] != null) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        let range = thisQuill.getSelection(true);
                        let imageBlot = ImagePlaceholder.create({ data: e.target.result, identifier: 'image_' + thisCreateUuid() });
                        thisQuill.insertEmbed(range.index, imageBlot, Quill.sources.USER);
                        thisQuill.setSelection(range.index + 1, Quill.sources.USER);
                        fileInput.value = '';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            });
            this.quillElement.appendChild(fileInput);
        }
        fileInput.click();
    };
    
    getDefaultToolbarOptions() {
        return [
            ['bold', 'italic', 'underline', 'strike'], ['blockquote', 'code-block'], [{ 'header': 1 }, { 'header': 2 }],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }], [{ 'script': 'sub'}, { 'script': 'super' }],
            [{ 'direction': 'rtl' }], [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'color': [] }, { 'background': [] }], [{ 'font': [] }], [{ 'align': [] }], ['image'], ['clean']
        ];
    }

    createUuid() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }
}

