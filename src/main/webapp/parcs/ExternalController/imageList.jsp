<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<!DOCTYPE html>
<html>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="java.util.Properties" %>
<head>
    <title>image list</title>
<%
Properties oscarVariables = OscarProperties.getInstance();
String parc_ip = oscarVariables.getProperty("parcs_ip");
String parcs_username = oscarVariables.getProperty("parcs_username");
String parcs_pw = oscarVariables.getProperty("parcs_password");
String demographicNo = request.getParameter("demographic_no");
String fdid = request.getParameter("fdid");
%>

<script src="Scripts/jquery/jquery-1.8.2.js" type="text/javascript"></script>	 
<script type="text/javascript">
var demoNo = "<%=demographicNo %>";
var hostIP1 = "<%=parc_ip %>";
var globalUserName1 = "<%= parcs_username%>";
var globalUserPassword1 = "<%= parcs_pw%>";
var login_auth = "";

var fdid = "<%=fdid%>";
var savedUidList = [];
if(fdid != "0"){
	$.ajax({
		url: "../../oscarEform/attachDoc.do?method=attachPacs&requestId=" + fdid,
		type: "POST",
		async: false,
		success: function(data){
			data = JSON.parse(data);
			savedUidList = data.uidList;
		}
	});
}
</script> 
<script src="Scripts/externalCommand/ExternalCommands.js" type="text/javascript"></script>
<script src="ServiceProxy/ServiceProxy.js" type="text/javascript"></script>
<script src="ServiceProxy/AuthenticationServiceProxy.js" type="text/javascript"></script>
<script src="ServiceProxy/QueryArchiveServiceProxy.js" type="text/javascript"></script>
<script src="ServiceProxy/PatientServiceProxy.js" type="text/javascript"></script>
   
<script src="JS/logger.js" type="text/javascript"></script>
<script src="JS/viewInstances.js" type="text/javascript"></script>
<script src="JS/updatePatient.js" type="text/javascript"></script>
<script src="JS/addPatient.js" type="text/javascript"></script>
<script src="JS/updateUser.js" type="text/javascript"></script>
<script src="JS/addUser.js" type="text/javascript"></script>
<script src="JS/myMain.js" type="text/javascript"></script>

<style type="text/css">
ul{
     margin:0px;
     padding:0px;
     list_style:none;
}

li{
    white-space:nowrap;
    display:block;
	margin: 15px 10px;
    float:left;
    position: relative;
}
.clearfix:after{ 
	content:""; 
	height:0; 
	visibility:hidden; 
	display:block; 
	clear:both;
}
.clearfix{ 
	zoom:1;
}
li img,li input{
	display: block;
}
li input{
	float: left;
	height: 165px;
	line-height: 165px;
}
li img{
	width: 150px;
	height: 150px;
	margin-left: 25px;
}

.pageContent {
    bottom: 0;
    overflow: auto;
    padding: 10px;
    position: absolute;
    top: 0;
}
.pageFooter {
	bottom: 0;
    min-height: 28px;
    padding: 1px 20px;
    position: absolute;
    right: 0;
}

.btn-close, .btn-red {
    border-color: red;
    color: red;
}

.btn {
	height: 40px;
    -moz-user-select: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    line-height: 24px;
    margin-left: 20px;
    padding: 6px 30px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
	float: right;
}
</style>
<script type="text/javascript">
function saveSelectImage(){
	if(!$("input[type='checkbox']").is(":checked")){
		alert("Please select you need image first!");
		return;
	}else{
		var SOPInstanceUID_List = "";
		for(var i = 0;i < $("input[type='checkbox']").length;i ++){
			if($($("input[type='checkbox']")[i]).is(":checked")){
				SOPInstanceUID_List += $("input[type='checkbox']")[i].id + ",";
			}
		}
		window.opener.updateImageList(SOPInstanceUID_List, encodeURIComponent(login_auth));
		window.close();
	}
}

function closePage(){
	var isClose = confirm("Are you sure to close the current page?");
	if(isClose){
		window.close();
	}
}
</script>
</head>
<body style="padding: 5px;">
	<div class="pageContent">
		<ul id="imageList_li" class="clearfix">
			
		</ul>
	</div>
	<div class="pageFooter">
		<button type="button" class="btn-close btn" onclick="closePage()">colse</button>
		<button type="button" class="btn-default btn" onclick="saveSelectImage()">save</button>
	</div>
  
</body>
<script src="JS/imageList.js" type="text/javascript"></script>
</html>
