﻿// ExternalCommands.ts
/// <reference path="SharedPropertiesService.ts" />
/// <reference path="WebViewerCommandHandlerService.ts" />
/*jshint eqnull:true */
/*jslint plusplus: true */
/*jslint white: true */
/*global describe:true*/
/*jslint newcap: true*/
/*jslint nomen: true*/
/*jshint onevar: false */
/*global window : false */
/*global WebViewerCommandHandlerService : false */
/*global setTimeout : false */
/*global DebugLog : false */
/*global ParseQueryString : false */
/*global location : false */
var ExternalCommandNames;
(function (ExternalCommandNames) {
    ExternalCommandNames.LogOut = "LogOut";
    ExternalCommandNames.Close = "Close";
    ExternalCommandNames.FindPatient = "FindPatient";
    ExternalCommandNames.FindPatients = "FindPatient";
    ExternalCommandNames.ShowPatient = "ShowPatient";
    ExternalCommandNames.ShowStudy = "ShowStudy";
    ExternalCommandNames.ShowSeries = "ShowSeries";
    ExternalCommandNames.ShowInstance = "ShowInstance";
    ExternalCommandNames.GetCurrentPatient = "GetCurrentPatient";
    ExternalCommandNames.SearchImage = "SearchImage";
    ExternalCommandNames.GetImage = "GetImage";
    ExternalCommandNames.AddPatient = "AddPatient";
    ExternalCommandNames.DeletePatient = "DeletePatient";
    ExternalCommandNames.UpdatePatient = "UpdatePatient";
    ExternalCommandNames.EndAssociation = "EndAssociation";
    ExternalCommandNames.AddUser = "AddUser";
    ExternalCommandNames.UpdateUser = "UpdateUser";
    ExternalCommandNames.DeleteUser = "DeleteUser";
})(ExternalCommandNames || (ExternalCommandNames = {}));
;
//# sourceMappingURL=ExternalCommands.js.map
