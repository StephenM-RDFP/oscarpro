﻿// ExternalWEebViewercontrollerProxy.js
/// <reference path="SharedPropertiesService.ts" />
/// <reference path="ExternalCommands.ts" />
/// <reference path="WebViewerCommandHandlerService.ts" />
/// <reference path="WebViewerMessageReceiver.ts" />
/*jshint eqnull:true */
/*jslint plusplus: true */
/*jslint white: true */
/*global describe:true*/
/*jslint newcap: true*/
/*jslint nomen: true*/
/*jshint onevar: false */
/*global window : false */
/*global WebViewerCommandHandlerService : false */
/*global setTimeout : false */
/*global DebugLog : false */
/*global ParseQueryString : false */
/*global location : false */
var LogUtils;
(function (LogUtils) {
    function DebugLog(s) {
        // console.log(s);
    }
    LogUtils.DebugLog = DebugLog;
})(LogUtils || (LogUtils = {}));

var GenericActionStatus;
(function (GenericActionStatus) {
    GenericActionStatus.Success = "Success";
    GenericActionStatus.Failed = "failed";
})(GenericActionStatus || (GenericActionStatus = {}));
;

var ExternalCommandHandlerService = (function () {
    function ExternalCommandHandlerService(sharedPropertiesService, authenticationService, tabService) {
        this._externalControlEnabled = false;
        this._externalControlPort = 0;
        this._webViewerAuthenticationToken = null;
        this._externalControlAssociationToken = null;
        this._controller = null;
        this._shouldStopPolling = false;
        this._sharedPropertiesService = sharedPropertiesService;
        this._authenticationService = authenticationService;
        this._tabService = tabService;
    }
    ExternalCommandHandlerService.prototype.onUpdatePatientError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("UpdatePatientError: " + textStatus);
        this.notifyActionStatus(ExternalCommandNames.UpdatePatient, xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onDeletePatientError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("DeletePatientError: " + textStatus);
        this.notifyActionStatus(ExternalCommandNames.DeletePatient, xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onAddPatientError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("AddPatientError: " + textStatus);
        this.notifyActionStatus(ExternalCommandNames.AddPatient, xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onShowSeriesError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("FindSeriesExtError: " + textStatus);
        this.notifyActionStatus(ExternalCommandNames.ShowSeries, xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onFindPatientError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("FindPatienError: " + textStatus);
        this.notifyActionStatus(ExternalCommandNames.FindPatient, xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onFindPatientFromSeriesError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("onFindPatientFromSeriesError: " + textStatus);
        this.notifyActionStatus("FindPatientFromSeries", xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.onReceivePatientNameError = function (xhr, textStatus, ex) {
        LogUtils.DebugLog("ReceivePatientNameError: " + textStatus);
        this.notifyActionStatus("ReceivePatientName", xhr.statusText);
    };

    ExternalCommandHandlerService.prototype.encodeDate = function (date) {
        var encodedDate = date.replace(/\//g, '.');
        encodedDate = encodedDate.replace(/\:/g, '!');
        encodedDate = encodedDate.replace(/\ /g, '_');
        return encodedDate;
    };

    ExternalCommandHandlerService.prototype.sendImageInfo = function (sopInstanceUID, encodedDate, imageType, comment, toothGroups) {
        var request = this.GetExternalControlServiceRoot() + "ReceiveImageInfo/true" + "/" + sopInstanceUID + "/" + encodedDate + "/" + imageType + "/" + comment + "/" + toothGroups + "/" + this._externalControlAssociationToken;
        LogUtils.DebugLog(request);
        this.NoCacheAjax_GET(request, function () {
            LogUtils.DebugLog("Image Info Sent");
        }, $.proxy(this.onHandleRequestError, this));
    };

    ExternalCommandHandlerService.prototype.notifyImageSearchNoneFound = function () {
        var request = this.GetExternalControlServiceRoot() + 'ReceiveImageInfo/false/e/e/e/e/e/' + this._externalControlAssociationToken;
        LogUtils.DebugLog(request);
        this.NoCacheAjax_GET(request, function () {
            LogUtils.DebugLog("Notified no image info found");
        }, $.proxy(this.onHandleRequestError, this));
    };

    ExternalCommandHandlerService.prototype.notifyActionStatus = function (commandName, commandResult) {
        if (this._externalControlEnabled === true) {
            var request = this.GetExternalControlServiceRoot() + 'ReceiveGenericActionStatus/' + commandResult + '/' + this._externalControlAssociationToken;
            LogUtils.DebugLog(request);
            this.NoCacheAjax_GET(request, function () {
                LogUtils.DebugLog("Action status notification sent- status: " + commandResult);
            }, function () {
                LogUtils.DebugLog("notifyActionStatus Failed");
            });
        } else {
            var info = {
                "externalControlAssociationToken": this._externalControlAssociationToken,
                "commandName": commandName,
                "commandResult": commandResult
            };

            var s = JSON.stringify(info);

            // window.parent.postMessage(s, '*');
            if (window.opener != null) {
                window.opener.postMessage(s, '*');
            }
        }
    };

    ExternalCommandHandlerService.prototype.LogoutNotify = function (reason) {
        if (this._externalControlEnabled === true) {
            var request = this.GetExternalControlServiceRoot() + 'LogoutNotify/' + reason;
            this.NoCacheAjax_GET(request, {}, {});
        } else {
            this.notifyActionStatus(ExternalCommandNames.LogOut, reason);
        }
    };

    ExternalCommandHandlerService.prototype.IsExternalControlEnabled = function () {
        return this._externalControlEnabled;
    };

    ExternalCommandHandlerService.prototype.Initialize = function () {
        var shouldRequest;
        var polling;
        var token;
        var shouldRequest;

        shouldRequest = this._sharedPropertiesService.GetExternalControlMode();
        polling = this._sharedPropertiesService.GetPolling();
        console.log("polling: ", polling);
        token = this._sharedPropertiesService.GetToken();
        if (token != '') {
            this._webViewerAuthenticationToken = token;
        }

        if (shouldRequest) {
            this._externalControlPort = this._sharedPropertiesService.GetPort();
            this._shouldStopPolling = !polling;

            if (polling) {
                this.EstablishExternalControl();
            } else {
                this._controller = new WebViewerCommandHandlerService(this._authenticationService, this._webViewerAuthenticationToken, window);
            }
        } else {
            this._externalControlEnabled = false;
            LogUtils.DebugLog("Normal Mode");
        }
    };

    ExternalCommandHandlerService.prototype.GetExternalControlServiceRoot = function () {
        return "http://localhost:" + this._externalControlPort + "/ExternalCommandQueueService/";
    };

    ExternalCommandHandlerService.prototype.EstablishExternalControl = function () {
        LogUtils.DebugLog("Establishing External Control...");
        this.NoCacheAjax_GET(this.GetExternalControlServiceRoot() + "RequestExternalControl", $.proxy(this.onRequestExternalControlSuccess, this), $.proxy(this.onHandleRequestError, this));
    };

    ExternalCommandHandlerService.prototype.onRequestExternalControlSuccess = function (data) {
        var _externalControlAssociationStatus = data;
        if (_externalControlAssociationStatus.Accepted) {
            this._externalControlEnabled = true;
            this._externalControlAssociationToken = _externalControlAssociationStatus.Token;
            LogUtils.DebugLog("External Control Established - token: " + this._externalControlAssociationToken);
            this.NoCacheAjax_GET(this.GetExternalControlServiceRoot() + "GetViewerAuthenticationToken/" + this._externalControlAssociationToken, $.proxy(this.onGetViewerAuthenticationTokenSuccess, this), $.proxy(this.onHandleRequestError, this));
        } else {
            LogUtils.DebugLog("External Control Request Denied");
        }
    };

    ExternalCommandHandlerService.prototype.onGetViewerAuthenticationTokenSuccess = function (data) {
        this._webViewerAuthenticationToken = data;
        this._controller = new WebViewerCommandHandlerService(this._authenticationService, this._webViewerAuthenticationToken, window);
        this.PollForCommands();
    };

    ExternalCommandHandlerService.prototype.onHandleRequestError = function (jqXHR, textStatus, errorThrown) {
        this._externalControlEnabled = false;
        LogUtils.DebugLog("External Control Error: " + textStatus + " - " + errorThrown);
    };

    ExternalCommandHandlerService.prototype.NoCacheAjaxParams_POST = function (serviceUrl, params, successFunction, errorFunction) {
        var s = JSON.stringify(params);

        $.ajax({
            url: serviceUrl,
            crossDomain: false,
            type: "POST",
            data: s,
            contentType: "application/json",
            dataType: "json",
            cache: false,
            success: successFunction,
            error: errorFunction
        });
    };

    ExternalCommandHandlerService.prototype.NoCacheAjax_GET = function (url, successFunction, errorFunction) {
        $.ajax({
            url: url,
            crossDomain: true,
            type: 'GET',
            dataType: "json",
            cache: false,
            success: successFunction,
            error: errorFunction
        });
    };

    ExternalCommandHandlerService.prototype.NoCacheAjax_POST = function (url, args, successFunction) {
        var s = JSON.stringify(args);

        $.ajax({
            url: url,
            crossDomain: true,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            cache: false,
            data: JSON.stringify(args),
            success: successFunction
        });
    };

    ExternalCommandHandlerService.prototype.PollForCommands = function () {
        if (this._shouldStopPolling) {
            return;
        }

        var request = this.GetExternalControlServiceRoot() + 'HasCommands/' + this._externalControlAssociationToken;
        this.NoCacheAjax_GET(request, $.proxy(this.HasCommandsRequestSuccess, this), $.proxy(this.onHandleRequestError, this));
    };

    ExternalCommandHandlerService.prototype.HasCommandsRequestSuccess = function (data) {
        var hasCommands = data;
        if (hasCommands) {
            var request = this.GetExternalControlServiceRoot() + 'GetCommands/' + this._externalControlAssociationToken;
            LogUtils.DebugLog(request);
            this.NoCacheAjax_GET(request, $.proxy(this.onGetCommandsRequestSuccess, this), $.proxy(this.onHandleRequestError, this));
        } else {
            setTimeout($.proxy(this.PollForCommands, this), 500);
        }
    };

    ExternalCommandHandlerService.prototype.onGetCommandsRequestSuccess = function (data) {
        var commands = data;
        for (var i = 0; i < commands.length; ++i) {
            this.ProcessCommand(commands[i]);
        }
        this.PollForCommands();
    };

    ExternalCommandHandlerService.prototype.GetInvalidArgumentsErrorString = function (functionName) {
        return functionName + ": Invalid Arguments";
    };

    ExternalCommandHandlerService.prototype.MyReceivePatientInfo = function (commandName, studies) {
        var localThis = this;
        var study = studies[0], name = null, birthDate = null, comments = null, patientId = null, sex = null, ethnicGroup = null, url = null, patientInfo = null, info = null;

        if (study.hasOwnProperty("Patient")) {
            name = study.Patient.Name;
            birthDate = study.Patient.BirthDate;
            comments = study.Patient.Comments;
            patientId = study.Patient.ID;
            sex = study.Patient.Sex;
            ethnicGroup = study.Patient.EthnicGroup;
        } else {
            if (study.hasOwnProperty("Name")) {
                name = study.Name;
            }

            if (study.hasOwnProperty("BirthDate")) {
                birthDate = study.BirthDate;
            }

            if (study.hasOwnProperty("Comments")) {
                comments = study.Comments;
            }

            if (study.hasOwnProperty("ID")) {
                patientId = study.ID;
            }

            if (study.hasOwnProperty("Sex")) {
                sex = study.Sex;
            }

            if (study.hasOwnProperty("EthnicGroup")) {
                ethnicGroup = study.EthnicGroup;
            }
        }

        if (name != null) {
            patientInfo = {
                "PatientId": patientId,
                "Name": name,
                "BirthDate": birthDate,
                "Sex": sex,
                "EthnicGroup": ethnicGroup,
                "Comments": comments
            }, info = {
                "externalControlAssociationToken": this._externalControlAssociationToken,
                "patientInfo": patientInfo
            }, url = null;

            if (this._externalControlEnabled === true) {
                url = this.GetExternalControlServiceRoot() + 'ReceivePatientInfo';

                this.NoCacheAjaxParams_POST(url, info, function (data, textStatus, xhr) {
                    LogUtils.DebugLog("Patient Name sent");
                    localThis.notifyActionStatus(commandName, GenericActionStatus.Success);
                }, function (xhr, textStatus, ex) {
                    LogUtils.DebugLog("ReceivePatientInfo: Error");
                    localThis.onReceivePatientNameError(xhr, textStatus, ex);
                });
            } else {
                info.commandName = commandName;
                info.commandResult = GenericActionStatus.Success;
                var s = JSON.stringify(info);
                window.opener.postMessage(s, '*');
            }
        } else {
            this.notifyActionStatus(commandName, GenericActionStatus.Success);
        }
    };

    ExternalCommandHandlerService.prototype.ProcessCommand = function (command) {
        var _this = this;
        var errorMessage = null, options = null, patientID = null, patientInfo = null;

        if (command.Name === ExternalCommandNames.LogOut) {
            _this._controller.LogOut();
        } else if (command.Name === ExternalCommandNames.Close) {
            _this._controller.Close();
        } else if (command.Name === ExternalCommandNames.FindPatient) {
            patientID = command.Args[0];
            options = command.Args[1];
            _this._controller.FindPatient(patientID, options, this.onFindPatientError, function (studies) {
                if (studies.data.length > 0) {
                    _this.MyReceivePatientInfo(command.Name, studies.data);
                } else {
                    _this.notifyActionStatus(command.Name, "PatientId does not exist: " + patientID);
                }
            });
        } else if (command.Name === ExternalCommandNames.ShowPatient) {
            if (command.Args == null || command.Args.length !== 1) {
                errorMessage = _this.GetInvalidArgumentsErrorString(command.Name);
                LogUtils.DebugLog(errorMessage);
                _this.notifyActionStatus(command.Name, errorMessage);
                return;
            }

            patientID = command.Args[0];
            _this._controller.FindSeriesExt(patientID, null, null, _this.onFindPatientError, function (series) {
                for (var i = 0; i < series.data.length; i++) {
                    var instanceData = series.data[i];
                    _this._controller.ShowSeriesInstanceExt(series.data, instanceData, null);
                    if (_this._controller.isDental() && series.data.length > 1)
                        break;
                }
                _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
            });
        } else if (command.Name === ExternalCommandNames.ShowStudy) {
            if (command.Args == null || command.Args.length !== 1) {
                errorMessage = _this.GetInvalidArgumentsErrorString(command.Name);
                LogUtils.DebugLog(errorMessage);
                _this.notifyActionStatus(command.Name, errorMessage);
                return;
            }

            var studyInstanceUID = command.Args[0];
            _this._controller.FindSeriesExt(null, studyInstanceUID, null, _this.onFindPatientError, function (series) {
                for (var i = 0; i < series.data.length; i++) {
                    var instanceData = series.data[i];
                    _this._controller.ShowSeriesInstanceExt(series.data, instanceData, null);
                }
                _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
            });
        } else if (command.Name === ExternalCommandNames.ShowSeries) {
            if (command.Args == null || command.Args.length !== 1) {
                errorMessage = _this.GetInvalidArgumentsErrorString(command.Name);
                LogUtils.DebugLog(errorMessage);
                _this.notifyActionStatus(command.Name, errorMessage);
                return;
            }

            var seriesInstanceUID = command.Args[0];
            _this._controller.FindSeriesExt(null, null, seriesInstanceUID, _this.onShowSeriesError, function (series) {
                var instanceData = series.data[0];
                _this._controller.ShowSeriesInstanceExt(series.data, instanceData, function () {
                    _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
                });
            });
        } else if (command.Name === ExternalCommandNames.GetCurrentPatient) {
            options = "All";

            var controller = null;
            var medicalViewer = null;
            var selectedItem = null;

            var tab = this._tabService.get_allTabs()[this._tabService.activeTab];
            if (tab == null) {
                _this.notifyActionStatus(command.Name, "There are no loaded instances.");
                return;
            }
            controller = this._tabService.get_tabData(tab.id, TabDataKeys.ViewController);
            if (controller != null) {
                medicalViewer = controller.getViewer();
            }

            if (medicalViewer != null) {
                selectedItem = medicalViewer.layout.get_selectedItems().get_item(0);
            }

            if (selectedItem == null) {
                _this.notifyActionStatus(command.Name, "There are no selected instances.");
                return;
            }

            var seriesInstanceUid = selectedItem.get_seriesInstanceUID();

            _this._controller.FindPatientFromSeries(seriesInstanceUid, options, _this.onFindPatientFromSeriesError, function (studies) {
                if (studies.data.length > 0) {
                    _this.MyReceivePatientInfo(command.Name, studies.data);
                } else {
                    _this.notifyActionStatus(command.Name, "Current patient does not exist");
                }
            });
        } else if (command.Name === ExternalCommandNames.GetImage) {
            var sopInstanceUID = command.Args[0];
            if (sopInstanceUID === "") {
                var request = _this.GetExternalControlServiceRoot() + 'ReceiveImageURL?url=' + encodeURIComponent("") + "&token=" + this._externalControlAssociationToken;
                LogUtils.DebugLog(request);
                _this.NoCacheAjax_POST(request, null, function () {
                    LogUtils.DebugLog("Empty Image URL Sent");
                });
            } else {
                _this._controller.GetInstanceImageURL(sopInstanceUID, function (url) {
                    if (_this._externalControlEnabled === true) {
                        var request = _this.GetExternalControlServiceRoot() + 'ReceiveImageURL?url=' + encodeURIComponent(url) + "&token=" + _this._externalControlAssociationToken;
                        LogUtils.DebugLog(request);
                        _this.NoCacheAjax_POST(request, null, function () {
                            LogUtils.DebugLog("Image URL Sent");
                        });
                    } else {
                        var info = {
                            "externalControlAssociationToken": _this._externalControlAssociationToken,
                            "url": url,
                            "sopInstanceUID": sopInstanceUID,
                            "commandName": command.Name,
                            "commandResult": GenericActionStatus.Success
                        };

                        var s = JSON.stringify(info);
                        window.opener.postMessage(s, '*');
                    }
                });
            }
        } else if (command.Name === ExternalCommandNames.AddPatient) {
            if (command.Args == null) {
                errorMessage = _this.GetInvalidArgumentsErrorString(command.Name);
                LogUtils.DebugLog(errorMessage);
                _this.notifyActionStatus(command.Name, errorMessage);
                return;
            }

            patientInfo = {};
            patientInfo.PatientId = command.Args[0];
            patientInfo.Name = command.Args[1];
            patientInfo.Sex = command.Args[2];
            patientInfo.BirthDate = command.Args[3];
            patientInfo.EthnicGroup = command.Args[4];
            patientInfo.Comments = command.Args[5];

            _this._controller.AddPatient(patientInfo, _this.onAddPatientError, function (data) {
                var patientAdded = data.data.AddPatientResult;
                if (patientAdded) {
                    _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
                } else {
                    _this.notifyActionStatus(command.Name, "PatientId Already Exists: " + patientInfo.PatientId);
                }
            });
        } else if (command.Name === ExternalCommandNames.DeletePatient) {
            if (command.Args == null || command.Args.length !== 1) {
                errorMessage = _this.GetInvalidArgumentsErrorString(command.Name);
                LogUtils.DebugLog(errorMessage);
                _this.notifyActionStatus(command.Name, errorMessage);
                return;
            }

            patientID = command.Args[0];
            options = "All";
            _this._controller.FindPatient(patientID, options, _this.onFindPatientError, function (studies) {
                if (studies.data.length > 0) {
                    _this._controller.DeletePatient(patientID, _this.onDeletePatientError, function (isSuccess) {
                        if (isSuccess) {
                            _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
                        } else {
                            _this.notifyActionStatus(command.Name, "Failed to delete patient: " + patientID);
                        }
                    });
                } else {
                    _this.notifyActionStatus(command.Name, "PatientId does not exist: " + patientID);
                }
            });
        } else if (command.Name === ExternalCommandNames.UpdatePatient) {
            patientInfo = {};
            patientInfo.PatientId = command.Args[0];
            patientInfo.Name = command.Args[1];
            patientInfo.Sex = command.Args[2];
            patientInfo.BirthDate = command.Args[3];
            patientInfo.EthnicGroup = command.Args[4];
            patientInfo.Comments = command.Args[5];

            _this._controller.UpdatePatient(patientInfo, _this.onUpdatePatientError, function (data) {
                _this.notifyActionStatus(command.Name, GenericActionStatus.Success);
            });
        } else if (command.Name === ExternalCommandNames.EndAssociation) {
            _this._shouldStopPolling = true;
        }
    };
    ExternalCommandHandlerService.$inject = ['sharedPropertiesService', 'authenticationService', 'tabService'];
    return ExternalCommandHandlerService;
})();

services.service('externalCommandHandlerService', ExternalCommandHandlerService);
//# sourceMappingURL=ExternalCommandHandlerService.js.map
