﻿/// <reference path="../app.ts" />
var SharedPropertiesService = (function () {
    function SharedPropertiesService() {
        this._externalControlMode = false;
        this._authenticationToken = '';
        this._port = 0;
        this._polling = true;
    }
    SharedPropertiesService.prototype.GetExternalControlMode = function () {
        return this._externalControlMode;
    };

    SharedPropertiesService.prototype.SetExternalControlMode = function (externalControlMode) {
        this._externalControlMode = externalControlMode;
    };

    SharedPropertiesService.prototype.GetToken = function () {
        return this._authenticationToken;
    };

    SharedPropertiesService.prototype.SetToken = function (token) {
        this._authenticationToken = token;
    };

    SharedPropertiesService.prototype.GetPort = function () {
        return this._port;
    };

    SharedPropertiesService.prototype.SetPort = function (port) {
        this._port = port;
    };

    SharedPropertiesService.prototype.GetPolling = function () {
        return this._polling;
    };

    SharedPropertiesService.prototype.SetPolling = function (polling) {
        this._polling = polling;
    };
    return SharedPropertiesService;
})();

services.service('sharedPropertiesService', SharedPropertiesService);
//# sourceMappingURL=SharedPropertiesService.js.map
