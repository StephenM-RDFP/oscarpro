var controller = new MainPageController();
var logger = new Logger();
patientID = demoNo;

//Maged added
var VieweImagesViewerSettings   =  "location=no,resizable=1, left=0,top=0,width="+ (screen.width-200) +',height='+ (screen.height-150);
var GetStudiesViewerSettings    = "toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,left=10000, top=10000, width=50, height=50, visible=none";
var functionToCallAfterLoad     = '';
var intervalVar = null;
var timeoutBeforeshowVeiwer = 5000;
var loadingMode = 0;        //0 login, 1 load sieres, 2 display images

var currPatienNum = 0;
var patientList = null;
var seriesResultsList = null;
var studyResultsNum = 0;
//login

loadingMode = 2;
controller.Authenticate(controller.onAuthenticationError, loginSuccess);


function loginSuccess(authentication){
	if (IsJsonString(authentication)) {
        var result = JSON.parse(authentication);
        var errorString = result.Message;
        logger.LogMessage("Login: ", "Error: " + errorString);
        Logout(false);
    }else {
        //var userName = "UserName:" + $("#idText_loginLogout_userName").val();
        //var password = "Password:" + $("#idText_loginLogout_password").val();
    	
    	login_auth = authentication;

        controller.RemoteLogOut = false;
        logger.LogMessage("Login", "Success", globalUserName, globalUserPassword);

        controller.AuthenticationToken = authentication;
        controller.AuthenticationProxy = new AuthenticationServiceProxy(hostIP+"MedicalViewerService19/AuthenticationService.svc");
        controller.AuthenticationProxy.SetAuthenticationCookie(authentication);

        controller.QueryProxy = new QueryArchiveServiceProxy(hostIP + "MedicalViewerService19/ObjectQueryService.svc", controller.AuthenticationProxy);

        controller.PatientProxy = new PatientServiceProxy(hostIP + "MedicalViewerService19/PatientService.svc", controller.AuthenticationProxy);

        //controller.RunViewer();
		
		var url = hostIP + "MedicalViewer19/#/login/autologin/" + "0/" + encodeURIComponent(controller.AuthenticationToken);

		controller.ViewerWindow = window.open(url, "MedicalViewer19", GetStudiesViewerSettings);
		
		if (!controller.ViewerWindow) {
			logger.LogMessage("RunViewer", "Error: Viewer could not be opened.");
		}
		else {
			if (loadingMode == 0) {
				controller.ShowSelected('updatePatients', 1);
			}
			else {
				justInitializeViewInstances(controller.ViewerWindow);

				if (loadingMode == 0 || loadingMode == 1)
					intervalVar = setInterval(showCurrStudy2, 2000);
				else
					intervalVar = setInterval(showCurrStudy2, timeoutBeforeshowVeiwer);
			}
		}
		
		if (controller.QueryProxy) {
			var queryParams = {};
			queryParams.options = {};
			queryParams.options.PatientsOptions = {};
			queryParams.options.PatientsOptions.PatientID = patientID;
			controller.QueryProxy.FindPatientsMax(queryParams, maxPatientQueryResults, onUpdatePatientsError, onUpdatePatientsIdSuccess);
		}
    }
}

function showCurrStudy2() {
    clearInterval(intervalVar);
    intervalVar = null;
    //window[functionToCallAfterLoad]();
    functionToCallAfterLoad = '';
    //EnableButtons(true);
}

function onUpdatePatientsIdSuccess(patientResults) {
	patientList = new Array();
	patientList = patientResults;
    if (patientResults) {
        for (var i = 0; i < patientResults.length; i++) {
			if (controller.QueryProxy) {
				currPatienNum ++;
				
				var queryParams = {};
				queryParams.options = {};
				queryParams.options.PatientsOptions = {};
				queryParams.options.PatientsOptions.PatientID = patientResults[i].ID;

				controller.QueryProxy.FindStudiesMax(queryParams, maxStudyQueryResults, onUpdateStudiesError, onUpdateStudiesUidSuccess);
			}
        }
    }
}

function onUpdateStudiesUidSuccess(studyResults) {
	studyResults.sort(function (a, b) {
        var c = new Date(a.Date);
        var d = new Date(b.Date);
        return d - c;
    });
	
	studyResultsList = studyResults;
	if (studyResults) {
        for (var i = 0; i < studyResults.length; i++) {
        	studyResultsNum = i;
            studies[i] = studyResults[i].InstanceUID;
			
			var queryParams = {};
            queryParams.options = {};
            queryParams.options.StudiesOptions = {};
            queryParams.options.StudiesOptions.StudyInstanceUID = studyResults[i].InstanceUID;

            controller.QueryProxy.FindSeriesMax(queryParams, maxSeriesQueryResults, onUpdateSeriesError, onUpdateSeriesUidSuccess);
        }
    }
}

function onUpdateSeriesUidSuccess(seriesResults){
	if (seriesResults && seriesResults.length > 0) {
        for (var i = 0; i < seriesResults.length; i++) {
			if (controller.QueryProxy) {
				var queryParams = {};
				queryParams.options = {};
				queryParams.options.SeriesOptions = {};
				queryParams.options.SeriesOptions.SeriesInstanceUID = seriesResults[i].InstanceUID;

				controller.QueryProxy.FindInstancesMax(queryParams, maxInstanceQueryResults, onUpdateInstancesError, onUpdateInstancesUidSuccess);
			}
        }
    }
}

function onUpdateInstancesUidSuccess(instanceResults) {
    
	var imageList = document.getElementById("imageList_li");
    if (instanceResults && instanceResults.length > 0) {
		var li_html = "";
        for (var i = 0; i < instanceResults.length; i++) {
			//li_html += instanceResults[i].SOPInstanceUID + "\n" + instanceResults[i].ImageURL;
        	if($.inArray(instanceResults[i].SOPInstanceUID, savedUidList) > -1){
        		li_html = "<li><input type='checkbox' id='" + instanceResults[i].SOPInstanceUID + "' checked><label>" + studyResultsList[studyResultsNum].Date + "</label><img src='" + instanceResults[i].ImageURL + instanceResults[i].PagesInfo.ImageUri + "'/></li>";
        	}else{
        		li_html = "<li><input type='checkbox' id='" + instanceResults[i].SOPInstanceUID + "'><label>" + studyResultsList[studyResultsNum].Date + "</label><img src='" + instanceResults[i].ImageURL + instanceResults[i].PagesInfo.ImageUri + "'/></li>";
        	}
			//get image url
			imageList.innerHTML = imageList.innerHTML + li_html;
        }
		//closeViewerWindow();
    }
	
	if(currPatienNum == patientList.length){
		closeViewerWindow();
	}
}

function closeViewerWindow() {
    if (ViewerWindow != null && !ViewerWindow.closed)
        ViewerWindow.close();

    ViewerWindow = null;
}
