<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.encounter" rights="w" reverse="true">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin,_admin.encounter");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
	String curUser_no = (String) session.getAttribute("user");
%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.dao.SmartEncounterTemplateDao" %>
<%@ page import="oscar.form.model.SmartEncounterTemplate" %>
<%@ page import="oscar.form.model.SmartEncounterShortCode" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oscar.form.dao.SmartEncounterShortCodeDao" %>
<%@ page import="java.util.List" %>
<%@ page import="oscar.form.model.SmartEncounterTemplatePlaceholder" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.form.model.SmartEncounterTemplateImage" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%

	SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
	List<SmartEncounterTemplate> templateList = smartEncounterTemplateDao.findAllActiveOrderByName();
	SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
	List<SmartEncounterShortCode> shortCodeList = smartEncounterShortCodeDao.findAllOrderByName();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	List<SmartEncounterTemplatePlaceholder> templatePlaceholderList = Arrays.asList(SmartEncounterTemplatePlaceholder.values());

%>

<html:html locale="true">
	<head>
		<meta charset="utf-8">
		<title>Smart Template Form Settings</title>

		<script type="text/javascript" src="<%=request.getContextPath()%>/js/quill/quill.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-block-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-image-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>
	
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/quill/quill.snow.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main-kai.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/fonts-min.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/autocomplete.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/demographicProviderAutocomplete.css"/>
	</head>
	<script type="text/javascript">
		function init() {
			// resize window
			window.resizeTo(1150, 800);
		}

		<%
		List<String> jsTemplates = new ArrayList<String>();
		for (SmartEncounterTemplate template : templateList) {
			List<String> imageJson = new ArrayList<String>();
			for (SmartEncounterTemplateImage smartEncounterTemplateImage : template.getPlaceholderImageList()) {
				imageJson.add("'" + smartEncounterTemplateImage.getName() + "' : '" + smartEncounterTemplateImage.getValue() + "'");
			}
			String deltaText = template.getTemplate();
			// replace image placeholders with proper urls
			for (SmartEncounterTemplateImage placeHolderImage : template.getPlaceholderImageList()) {
				deltaText = deltaText.replaceAll("\\$\\{" + placeHolderImage.getName() + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + placeHolderImage.getValue());
			}
			jsTemplates.add("[" + template.getId() + ", { 'name':'" + template.getName() + "', 'template': " + deltaText + ", 'createDate': " + template.getCreateDate().getTime() + "}]");
		}
		%>
		let templatesMap = new Map([<%=StringUtils.join(jsTemplates, ",")%>]);
		<%
		List<String> jsPlaceholder = new ArrayList<String>();
		for (SmartEncounterTemplatePlaceholder templatePlaceholder : templatePlaceholderList) {
			jsPlaceholder.add("['" + templatePlaceholder.getSubjectAndField() + "', " 
				+ "\"" + templatePlaceholder.toJavascriptCreateMethod() + "\"]");
		}
		%>
		let placeholderMap = new Map([<%=StringUtils.join(jsPlaceholder, ",")%>]);
		<%
		List<String> jsShortCodes = new ArrayList<String>();
		for (SmartEncounterShortCode shortCode : shortCodeList) {
			jsShortCodes.add("[" + shortCode.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(shortCode.getName()) + "', 'text':'" + Encode.forJavaScriptBlock(shortCode.getText()) + "', 'createDate': " + shortCode.getCreateDate().getTime() + "}]");
		}
		%>
		let shortCodesMap = new Map([<%=StringUtils.join(jsShortCodes, ",")%>]);

	</script>
	<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12" style="display: flow-root;">
				<h3 style="display: inline;">Smart Template Form Settings</h3>
				<input type="button" class="btn btn-default pull-right" style="display: none;" id="backButton" onclick="onClickBack()" value="Back">
			</div>
			<div class="col-sm-12">
				<html:form styleId="smartEncounterTemplateForm" action="/form/ManageSmartEncounterTemplateAction" styleClass="well">
					<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
					<input type="hidden" id="method" name="method" value="">
					<input type="hidden" id="id" name="id" value="">
					<input type="hidden" id="template" name="template" value="">
					<input type="hidden" id="createDateLong" name="createDateLong" value="">
					<div id="listTemplatesAndShortCodes">
						<div style="display: flow-root;">
							<h4 style="display: inline;">Templates:</h4>
							&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-default pull-right" type="button" value="Add New" onclick="addNewTemplate()"/>
						</div>
						<div class="panel panel-default">
							<div class="panel-body" style="max-height: 350px; overflow: scroll;">
								<table class="table">
									<tr>
										<th>Name</th>
										<th>Created</th>
										<th>Last Edited</th>
										<th>Options</th>
									</tr>
									<% for (SmartEncounterTemplate template : templateList) { %>
									<tr>
										<td><%=template.getName()%></td>
										<td><%=sdf.format(template.getCreateDate())%></td>
										<td><%=sdf.format(template.getEditedDate())%></td>
										<td><a href="javascript:void(0);" onclick="onClickEditTemplate(<%=template.getId()%>)">Edit</a> |
											<a href="javascript:void(0);" onclick="onClickDeleteTemplate(<%=template.getId()%>)">Delete</a></td>
									</tr>
									<% } %>
								</table>
							</div>
						</div>
						<div style="display: flow-root;">
							<h4 style="display: inline;">Short Codes:</h4>
							&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-default pull-right" type="button" value="Add New" onclick="addNewShortCode()"/>
						</div>
						<div class="panel panel-default">
							<div class="panel-body" style="max-height: 350px; overflow: scroll;">
								<table class="table">
									<tr>
										<th class="col-xs-1">Name</th>
										<th class="col-xs-4">Text</th>
										<th class="col-xs-2">Created</th>
										<th class="col-xs-2">Last Edited</th>
										<th class="col-xs-1">Options</th>
									</tr>
									<% for (SmartEncounterShortCode shortCode : shortCodeList) { %>
									<tr id="shortCodeRowId<%=shortCode.getId()%>">
										<td><%=Encode.forHtmlAttribute(shortCode.getName())%></td>
										<td><%=shortCode.getText()%></td>
										<td><%=sdf.format(shortCode.getCreateDate())%></td>
										<td><%=sdf.format(shortCode.getEditedDate())%></td>
										<td><a href="javascript:void(0);" onclick="onClickEditShortCode(<%=shortCode.getId()%>)">Edit</a> | 
											<a href="javascript:void(0);" onclick="onClickDeleteShortCode(<%=shortCode.getId()%>);">Delete</a></td>
									</tr>
									<% } %>
								</table>
							</div>
						</div>
					</div>
					<div id="addEditTemplate" style="display: none;">
						<h4>Edit Template:</h4>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-3">
										<div class="form-group">
											<div class="form-group">
												<input type="text" class="form-control" id="templateName" name="templateName" placeholder="Template Name"/>
											</div>
										</div>
									</div>
								</div>
								<div class="row center-row-content" style="overflow: visible; padding-bottom: 100px;">
									<div class="col-xs-9" style="width: 8in;">
										<div id="quillEditorTemplate" style="background-color: white;"></div>
									</div>
									<div class="col-xs-2" style="min-width: 220px;">
										<input type="text" class="input-sm" style="width: 70%;" id="placeholderFilter" placeholder="filter placeholders" onkeyup="updatePlaceholderFilter(this)"/>
										<input type="button" class="input-sm" onclick="clearPlaceholderFilter()" value="clear" />
										<div class="btn-group-vertical" role="group" id="placeholderList" style="width: 100%; overflow-y: scroll; max-height: 700px;">
											<input type="button" class="btn btn-default" onclick="addFillInMarkerAtCaret()" value="Add Fill In Marker (&laquo;&raquo;)"/>
											<%
												for (SmartEncounterTemplatePlaceholder templatePlaceholder : templatePlaceholderList) {
											%>
											<input type="button" class="btn btn-default quill-placeholder-<%=templatePlaceholder.getSubject()%>"
												   onclick="addTemplatePlaceholderAtCaret('<%=templatePlaceholder.getSubjectAndField()%>')"
												   value="<%=templatePlaceholder.getTitle()%>"/>
											<% } %>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="row form-inline">
											<div class="col-xs-6">
												<input type="button" class="btn btn-primary" value="Save" onclick="onClickTemplateSave()"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="addEditShortCode" style="display: none;">
						<h4 style="display: inline;">Edit Short Code:</h4>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-3">
										<div class="form-group">
											<div class="form-group">
												<input type="text" class="form-control" id="shortCodeName" name="shortCodeName" placeholder="Short Code Name"/>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12" style="width: 8in;">
										<textarea class="form-control" id="shortCodeText" name="shortCodeText" rows="5" cols="80" style="padding-bottom: 10px;"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="row form-inline">
											<div class="col-xs-6">
												<input type="button" class="btn btn-primary" value="Save" onclick="onClickShortCodeSave()"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>

	<script>
		let quillEditor = new TemplatePlaceholderQuill('quillEditorTemplate', null);

		let methodElement = document.getElementById('method');
		let listTemplatesAndShortCodesElement = document.getElementById('listTemplatesAndShortCodes');
		let addEditTemplateElement = document.getElementById('addEditTemplate');
		let addEditShortCodeElement = document.getElementById('addEditShortCode');
		function changeView(view) {
			listTemplatesAndShortCodesElement.style.display = 'none';
			addEditTemplateElement.style.display = 'none';
			addEditShortCodeElement.style.display = 'none';
			document.getElementById(view).style.display = 'block';
			document.getElementById('backButton').style.display = 'block';
		}

		function onClickTemplateSave() {
			methodElement.value = 'saveTemplate';
			setInputElementsToQuillValues();
			document.getElementById('smartEncounterTemplateForm').submit();
		}

		function addNewTemplate() {
            quillEditor.setContents([]); // clear editor
			changeView('addEditTemplate');
		}
		
		function onClickEditTemplate(templateId) {
			let template = templatesMap.get(templateId);

			document.getElementById('templateName').value = template['name'];
			document.getElementById('id').value = templateId;
			document.getElementById('createDateLong').value = template['createDate'];
			quillEditor.setContents(template['template']);
			changeView('addEditTemplate');
		}

		function onClickDeleteTemplate(templateId) {
			let template = templatesMap.get(templateId);
			if (confirm('Are you sure you want to delete template \'' + template['name'] +  '\'?')) {
				methodElement.value = 'deleteTemplate';
				document.getElementById('id').value = templateId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function setInputElementsToQuillValues() {
			let imagesToSaveArray = quillEditor.getDocumentImages();
			let formElement = document.getElementById('smartEncounterTemplateForm');
			for (let i = 0; i < imagesToSaveArray.length; i++) {
				let imageDataToSave = imagesToSaveArray[i].insert.ImagePlaceholder;
				let fileInput = document.createElement('input');
				fileInput.setAttribute('type', 'hidden');
				fileInput.setAttribute('name', 'images');
				fileInput.setAttribute('value', imageDataToSave.identifier + ';' + imageDataToSave.data);
				formElement.appendChild(fileInput);
			}
			document.getElementById('template').value = JSON.stringify(quillEditor.getContentsWithImagePlaceholders());
		}

		function addTemplatePlaceholderAtCaret(placeholderName) {
			let placeholder = placeholderMap.get(placeholderName);
			if (placeholder) {
				quillEditor.insertPlaceholderAtCaret(eval(placeholder));
			}
		}
		function addFillInMarkerAtCaret() {
			quillEditor.insertTextAtCaret(String.fromCharCode(171) + String.fromCharCode(187));
		}

		function onClickEditShortCode(shortCodeId) {
			let shortCode = shortCodesMap.get(shortCodeId);
			document.getElementById('shortCodeName').value = shortCode['name'];
			document.getElementById('id').value = shortCodeId;
			document.getElementById('shortCodeText').value = shortCode['text'];
			document.getElementById('createDateLong').value = shortCode['createDate'];
			changeView('addEditShortCode');
		}
		
		function onClickDeleteShortCode(shortCodeId) {
			let shortCode = shortCodesMap.get(shortCodeId);
			if (confirm('Are you sure you want to delete template \'' + shortCode['name'] +  '\'?')) {
				methodElement.value = 'deleteShortCode';
				document.getElementById('id').value = shortCodeId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function onClickShortCodeSave() {
			methodElement.value = 'saveShortCode';
			document.getElementById('smartEncounterTemplateForm').submit();
		}
		
	
		function updatePlaceholderFilter(filterInputElement) {
			let filter = filterInputElement.value.toUpperCase();
			let list = document.getElementById('placeholderList');
			for (let i = 0; i < list.children.length; i++) {
				let value = list.children[i].value;
				if (value.toUpperCase().includes(filter)) {
					list.children[i].style.display = 'block';
				} else {
					list.children[i].style.display = 'none';
				}
			}
		}
        function clearPlaceholderFilter() {
		    let element = document.getElementById('placeholderFilter');
            element.value = '';
            updatePlaceholderFilter(element);
        }
		
		function addNewShortCode() {
			changeView('addEditShortCode');
		}
		
		function onClickBack() {
			listTemplatesAndShortCodesElement.style.display = 'block';
			addEditTemplateElement.style.display = 'none';
			addEditShortCodeElement.style.display = 'none';
			document.getElementById('backButton').style.display = 'none';
			document.getElementById('templateName').value = '';
			document.getElementById('shortCodeName').value = '';
			document.getElementById('shortCodeText').value = '';
			document.getElementById('template').value = '';
			document.getElementById('createDateLong').value = '';
			document.getElementById('id').value = '';
		}
	</script>
	</body>
</html:html>
