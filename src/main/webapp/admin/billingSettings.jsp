<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    List<String> billingSettingsKeys = Arrays.asList("enable_3rd_party_billing_footer","3rd_party_billing_footer_text", "enable_footer_patient_id", "auto_populate_refer");

    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for(String key : billingSettingsKeys) {
            List<Property> property = propertyDao.findGlobalByName(key);
            if (property.isEmpty()) {
                Property newProperty = new Property();
                newProperty.setName(key);
                String newValue = request.getParameter(key);
                newProperty.setValue(newValue);
                propertyDao.persist(newProperty);
            } else {
                for (Property p : property) {
                    String newValue = request.getParameter(key);
                    p.setValue(newValue);
                    propertyDao.merge(p);
                }
            }
        }
    }
%>

<html:html locale="true">
    <head>
        <title>Billing Settings</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            function hasScrollbar(element_id)
            {
                var elem = document.getElementById(element_id);
                if (elem.clientHeight < elem.scrollHeight) {
                    document.getElementById("warning_text").style.visibility = "visible";
                } else {
                    document.getElementById("warning_text").style.visibility = "hidden";
                }
            }
        </script>
    </head>

    <%
        for(String key : billingSettingsKeys) {
            List<Property> properties = propertyDao.findGlobalByName(key);
            if (!properties.isEmpty() && properties.get(0).getName() != null && properties.get(0).getValue() != null) {
                dataBean.setProperty(properties.get(0).getName(), properties.get(0).getValue());
            }
        }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <div id="warning_text" style="color: #e26e6e; visibility: hidden;"><b>Warning: Footer text size is too large! This may cause undesired formatting!</b></div>
    <h4>Manage OSCAR Billing Settings</h4>
    <form name="billingSettingsForm" method="post" action="billingSettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>Enable Custom 3rd Party Billing Footer: </td>
                <td>
                    <input id="enable_3rd_party_billing_footer-true" type="radio" value="true" name="enable_3rd_party_billing_footer"
                            <%=(dataBean.getProperty("enable_3rd_party_billing_footer", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_3rd_party_billing_footer-false" type="radio" value="false" name="enable_3rd_party_billing_footer"
                            <%=(dataBean.getProperty("enable_3rd_party_billing_footer", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Patient ID Number on footer: </td>
                <td>
                    <input id="enable_footer_patient_id-true" type="radio" value="true" name="enable_footer_patient_id"
                            <%=(dataBean.getProperty("enable_footer_patient_id", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_footer_patient_id-false" type="radio" value="false" name="enable_footer_patient_id"
                            <%=(dataBean.getProperty("enable_footer_patient_id", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <textarea id="3rd_party_billing_footer_text" name="3rd_party_billing_footer_text" onKeyUp='hasScrollbar("3rd_party_billing_footer_text");' rows="4" cols="400" maxlength="2000" style="overflow: auto; min-width:572px; max-width:572px; min-height:100px; max-height:100px;"><%=Encode.forHtmlContent(dataBean.getProperty("3rd_party_billing_footer_text", ""))%></textarea>
                </td>
            </tr>
            <tr>
                <td>Auto-populate Referring Physician on Billing Form for All Providers?: </td>
                <td>
                    <input id="auto_populate_refer-true" type="radio" value="true" name="auto_populate_refer"
                            <%=(dataBean.getProperty("auto_populate_refer", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="auto_populate_refer-false" type="radio" value="false" name="auto_populate_refer"
                            <%=(dataBean.getProperty("auto_populate_refer", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>

        <input type="button" onclick="document.forms['billingSettingsForm'].dboperation.value='Save'; document.forms['billingSettingsForm'].submit();" name="saveBillingSettings" value="Save"/>
    </form>
    </body>
</html:html>