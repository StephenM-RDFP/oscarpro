<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="oscar.util.*, oscar.eform.data.*"%>
<%@page
	import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question"%>
<%@page
	import="org.oscarehr.integration.excelleris.eorder.api.QuestionType"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Choice"%>
<%@page import="org.oscarehr.common.dao.EOrderDao" %>
<%@page import="org.oscarehr.common.model.EOrder" %>
<%@page import="org.oscarehr.common.model.EFormValue" %>
<%@page import="org.oscarehr.common.dao.EFormValueDao" %>
<%@page import="org.oscarehr.common.dao.OrderLabTestCodeDao" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.converter.EFormValueHelper" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.converter.QuestionnaireConverter" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.ServiceException" %>
<%@page import="org.hl7.fhir.instance.formats.JsonParser" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="java.util.*,org.apache.log4j.Logger"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>


<head>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/moment.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap.min.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap-datetimepicker.min.js" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker-standalone.css" />
<link href="<%=request.getContextPath()%>/css/excelleris/eOrderQuestionnaire.css" rel="stylesheet" type="text/css"/>

<script> 


const QuestionTypeEnum = {
        CHOICE: 'CHOICE',
        STRING: 'STRING',
        TEXT: 'TEXT',
        DECIMAL: 'DECIMAL',
        INTEGER: 'INTEGER',
        DATE: 'DATE',
        DATETIME: 'DATETIME',
        TIME: 'TIME'
    }

function displayQuestionnaire() {           
    $('#questionnaireContatiner').show();
    updateQuestionDisplay(1);
    updateQuestionnaireNavigation(1);
    displaySavedAnswers();
    $('.time').datetimepicker({
		format : "LT",
	});
    
    $('.date').datetimepicker({
        format: 'L'
    });
    
    $('.datetime').datetimepicker({
        format: 'L LT'
    });
}

function updateQuestionDisplay(questionNum) {
    $('.question').hide();
    $('#questionnaireContainer').attr("current_question", questionNum);
    updateQuestionnaireNavigation(questionNum);
    $('.question[sequence=' + questionNum + ']').show();
}

function displayNextQuestion() {
    setQuestionAnswer();
    var currentQuestionNum = getCurrentQuestionNum();
    if(isQuestionAnswered(currentQuestionNum)){
        $('#answerRequiredMessage').hide();
        updateQuestionDisplay(getCurrentQuestionNum() + 1);
    } else {
        $('#answerRequiredMessage').show();
    }
}

function displayPreviousQuestion() {
     setQuestionAnswer();
     var currentQuestionNum = getCurrentQuestionNum();
     $('#answerRequiredMessage').hide();
     updateQuestionDisplay(getCurrentQuestionNum() - 1);
}

function isQuestionAnswered(questionNum) {
    var answerField = $('.question[sequence=' + questionNum + '] .qAnswer');
    if(answerField.val() == '') {
        return false;
    }

    return true;
}

function getCurrentQuestionNum() {
    return parseInt($('#questionnaireContainer').attr("current_question"));
}

function setQuestionAnswer() {
    var questionNum = getCurrentQuestionNum();
    var answerField = $(".question[sequence=" + questionNum + "] .qAnswer");
    var answer = "";
    var questionType = $(".question[sequence=" + questionNum + "]").attr('answerType');
    
    if(questionType === QuestionTypeEnum.CHOICE) {
        if($("input[name=question_" + questionNum +"]:checked").length > 0) {
    		answer = $("input[name=question_" + questionNum +"]:checked").val();
        }
    	
    } else {
    	answer = $("#question_" + questionNum).val();
    	if(questionType === QuestionTypeEnum.INTEGER) {
    		if(!/^-?\d*$/.test(answer)) {
    			 $('#invalidInt').show();
    			return;
    		}else {
    			 $('#invalidInt').hide();
    		}
    	}
    	
    }
    
    answerField.val(answer);
    var parentAnswerFieldId = "QR_q" + questionNum;
    opener.document.getElementById(parentAnswerFieldId).value = answer;
    $('#answerRequiredMessage').hide();
}

function displaySavedAnswers() {
	var i;
	for(i = 1; i <= $('.question').length; i++) {
		var questionType = $(".question[sequence=" + i + "]").attr('answerType');
		var value = opener.document.getElementById('QR_q' + i).value;
		if(value === '') {
			continue;
		}
		if (questionType === QuestionTypeEnum.CHOICE) {	    	
	    	$(".question[sequence=" + i + "] input[value='" + value + "']").attr('checked', 'checked');
	    } else if(questionType === QuestionTypeEnum.STRING){
	    	$("input[name=question_" + i +"]").val(value);	
	    }
	}
}

function updateQuestionnaireNavigation(currentQuestionNum) {
    if(currentQuestionNum <= 1) {
        $('#previousQuestion').hide();
    } else {
        $('#previousQuestion').show();
    }

    if($('.question').length < 2 || currentQuestionNum >= $('.question').length) {
        $('#nextQuestion').hide();
    } else {
        $('#nextQuestion').show();
    } 
    
    if(currentQuestionNum >= $('.question').length){
    	$('#finishAndSubmit').show();
    } else {
    	$('#finishAndSubmit').hide()
    }
}

function finishAndSubmit() {
	setQuestionAnswer();
	if(validQuestionnaire()){
		opener.submitEform();
		self.close();
	}
}


function validQuestionnaire() {
        var i;
        for(i = 1; i <= $('.question').length; i++) {
            if(!isQuestionAnswered(i)){
                $('#answerRequiredMessage').show();
                return false;
            }
        }
        $('#answerRequiredMessage').hide();
        return true;
}
	
</script>
</head>



<%
	Logger logger = MiscUtils.getLogger();
   
    String fdid = (String) request.getParameter("fdid");
   
    EOrderDao eOrderDao=(EOrderDao)SpringUtils.getBean(EOrderDao.class);
    EOrder eOrder = eOrderDao.findByExtEFormDataId(Integer.parseInt(fdid));
    Questionnaire questionnaire = null;
    if(eOrder != null && eOrder.getQuestionnaire() != null) {
    	// Convert to api questionnaire
	
    	JsonParser parser = new JsonParser();
//				ByteArrayOutputStream writer = new ByteArrayOutputStream();
		org.hl7.fhir.instance.model.Questionnaire fHirQuestionnaire = null;
			try {
				fHirQuestionnaire = (org.hl7.fhir.instance.model.Questionnaire) parser.parse(eOrder.getQuestionnaire());
			} catch (Exception e) {
				logger.error("Error converting Parameters to JSON.", e);
				throw new ServiceException("Error converting Parameters to JSON.");
			}
		
    	
    	OrderLabTestCodeDao orderLabTestCodeDao = (OrderLabTestCodeDao) SpringUtils
				.getBean(OrderLabTestCodeDao.class);
		// get eform values for the current eform data
		EFormValueDao eformValueDao = (EFormValueDao)SpringUtils.getBean("EFormValueDao");
		List<EFormValue> eformValues = eformValueDao.findByFormDataId(new Integer(fdid));					
    	Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		questionnaire = QuestionnaireConverter.toApiObject(fHirQuestionnaire, valueMap,
				orderLabTestCodeDao);
    }
    
    
	if (questionnaire != null) {
	
%>

	
	<body onload="displayQuestionnaire()">
	
	<div id="questionnaireContainer" current_question='1'>
		<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='<%= fdid %>'></input>
<%
		int seq = 1;
		for (Question question : questionnaire.getQuestions()) {
%>
		<div class='question' answerType='<%= question.getType() %>'
			sequence='<%= seq %>'
			hardCodedTestFieldId=' <%= question.getHardCodedTestFieldId() %>'
			otherTestFieldId='<%= question.getOtherTestFieldId() %>'>
			<div class="instruction">
				<bean:message
					key="eform.instruction.submit_questionnaire.answerQuestionnaire_1" />
				<b> <%= question.getTestName() %>
				</b>
				<bean:message
					key="eform.instruction.submit_questionnaire.answerQuestionnaire_2" />
			</div>

			<div class="questionContainer">

				<p><%=question.getQuestionText()%></p>
				<%
					if (question.getType().equals(QuestionType.CHOICE)) {

								for (Choice choice : question.getChoices()) {
				%>
				<input type='radio' id='<%=choice.getCode()%>'
					name='question_<%=seq%>' value='<%=choice.getCode()%>'> <label
					for='<%=choice.getCode()%>'><%=choice.getText()%> </label><br>
				<%
					}
							} else if (question.getType().equals(QuestionType.STRING)) {
				%>
				<input type='text' maxlength='120' id='question_<%=seq%>'
					name='question_<%=seq%>' value=''>
				<%
					} else if (question.getType().equals(QuestionType.TEXT)) {
				%>
				<textarea name='question_<%=seq%>' rows='4' cols='60'
					style="font-family: sans-serif; font-style: normal; font-weight: normal; font-size: 12px; text-align: left;"></textarea>
				<%
					} else if (question.getType().equals(QuestionType.DECIMAL)) {
				%>
				<input type="number" id='question_<%=seq%>'
					name='question_<%=seq%>'>
				<%
					} else if (question.getType().equals(QuestionType.INTEGER)) {
				%>
				<input type="text" id='question_<%=seq%>' name='question_<%=seq%>'>
				<%
					} else if (question.getType().equals(QuestionType.TIME)) {
				%>

				<div class='input-group time'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%=seq%>' class="form-control" /> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-time"></span>
					</span>
				</div>

				<%
					} else if (question.getType().equals(QuestionType.DATE)) {
				%>


				<div class='input-group date'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%=seq%>' class="form-control" /> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

				<%
					} else if (question.getType().equals(QuestionType.DATETIME)) {
				%>


				<div class='input-group datetime'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%=seq%>' class="form-control" /> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

				<%
					}
				%>

				<input class='qAnswer' type='hidden' id='QR_q<%=seq%>'
					name='QR_q<%=seq%>' value=''>
			</div>
		</div>
		<%
			seq++;
				}
		%>
		<div id='invalidInt' style="display: none; color: red; margin-left:20px;"><bean:message
					key="eform.instruction.submit_questionnaire.invalidInt" /></div>
    <div id='answerRequiredMessage' style="display: none; color: red; margin-left:20px;"><bean:message
					key="eform.instruction.submit_questionnaire.answerRequired" /></div>
    <div id='questionnaireNavigation' style="margin-top: 10px; text-align: center;">
       
         <input value="Previous"  name="previousQuestion" id="previousQuestion" type="button" style="display: none;" onclick="displayPreviousQuestion()">

          <input value="Next"  name="nextQuestion" id="nextQuestion" type="button" onclick="displayNextQuestion()" style="display: none;">
          <input value="Finish" name="Done" id="finishQuestionnaire" type="button" onclick="finishQuestionnaire()" style="display: none;">
          <input value="eOrder"  name="finishAndSubmit" id="finishAndSubmit" type="button" onclick="finishAndSubmit()" style="display: none;">
    </div>
</div>
<%
	}
%>

</body>
