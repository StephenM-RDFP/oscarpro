<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.sql.*, oscar.eform.data.*"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="oscar.eform.EFormCsrfUtil" %>
<%


String questionnaireString = "";
  if (request.getAttribute("questionnaire") != null) {
  	Questionnaire questionnaire = (Questionnaire) request.getAttribute("questionnaire");
  	
  	questionnaireString =  "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + request.getAttribute("M_existingFdid") + "'></input>";
   
  	int seq = 1;
  	for (Question question : questionnaire.getQuestions()) {	
  		questionnaireString += "<input class='qAnswer' type='hidden' id='" + "QR_q" + seq + "' name='" + "QR_q" + seq + "' value=''>";			  
  		seq++;
  	}
  }  	  	
  	  	



    String provider_no = (String) session.getValue("user");
    String eform_data_id = (String) request.getParameter("fdid");
    		
	String id = request.getParameter("fid");
	String messageOnFailure = "No eform or appointment is available";
  if (id == null) {  // form exists in patient
      id = request.getParameter("fdid");
      String appointmentNo = request.getParameter("appointment");
      String eformLink = request.getParameter("eform_link");

      EForm eForm = new EForm(id);
      eForm.setContextPath(request.getContextPath());
      eForm.setOscarOPEN(request.getRequestURI());
      if ( appointmentNo != null ) eForm.setAppointmentNo(appointmentNo);
      if ( eformLink != null ) eForm.setEformLink(eformLink);

      String parentAjaxId = request.getParameter("parentAjaxId");
      if( parentAjaxId != null ) eForm.setAction(parentAjaxId);
	  String logData = "fdid=" + request.getParameter("fdid") + "\nFormName=" + eForm.getFormName();
	  if (request.getParameter("appointment") != null) { logData += "\nappointment_no=" + request.getParameter("appointment"); }
	  LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.READ, "eForm",
			  request.getParameter("fdid"), eForm.getDemographicNo(), logData);
	  String htmlDocument = eForm.getFormHtml();
	  htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());
	  
	  // Pass additional data into the eForm - add here as needed
      htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
      htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
      htmlDocument = htmlDocument.replace("${questionnaire}", "");
      htmlDocument = htmlDocument.replace("${existingFdid}", "");
	  out.print(htmlDocument);
  } else {  //if form is viewed from admin screen
      EForm eForm = new EForm(id, "-1"); //form cannot be submitted, demographic_no "-1" indicate this specialty
      eForm.setContextPath(request.getContextPath());
      eForm.setupInputFields();
      eForm.setOscarOPEN(request.getRequestURI());
      eForm.setImagePath();
	  String logData = "fdid=" + request.getParameter("fdid") + "\nid=" + id;
	  if (request.getParameter("appointment") != null) { logData += "\nappointment_no=" + request.getParameter("appointment"); }
	  LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.READ, "eForm",
			  request.getParameter("fdid"), eForm.getDemographicNo(), logData);
	  String htmlDocument = eForm.getFormHtml();
	  htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());
	  
	  // Pass additional data into the eForm - add here as needed
      htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
      htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
      htmlDocument = htmlDocument.replace("${questionnaire}", questionnaireString);
      String existingFdid = (String) request.getAttribute("M_existingFdid") != null ? (String) request.getAttribute("M_existingFdid") : "";
      htmlDocument = htmlDocument.replace("${existingFdid}", existingFdid);
      out.print(htmlDocument);
  }
%>
<%
String iframeResize = (String) session.getAttribute("useIframeResizing");
if(iframeResize !=null && "true".equalsIgnoreCase(iframeResize)){ %>
<script src="<%=request.getContextPath() %>/library/pym.js"></script>
<script>
    var pymChild = new pym.Child({ polling: 500 });
</script>
<%}%>