<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="oscar.util.*, oscar.eform.data.*"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.QuestionType" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Choice" %>
<%@page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="java.util.*"%>
<%@ page import="oscar.eform.EFormCsrfUtil" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
  if (request.getAttribute("page_errors") != null) {
%>

<script language=javascript type='text/javascript'>


function hideDiv() {
    if (document.getElementById) { // DOM3 = IE5, NS6
        document.getElementById('hideshow').style.display = 'none';
    }
    else {
        if (document.layers) { // Netscape 4
            document.hideshow.display = 'none';
        }
        else { // IE 4
            document.all.hideshow.style.display = 'none';
        }
    }
}
</script>
<div id="hideshow" style="position: relative; z-index: 999;"><a
	href="javascript:hideDiv()">Hide Errors</a> <font
	style="font-size: 10; font-color: darkred;"> <html:errors /> </font></div>
<% } %>

<%
  if (request.getAttribute("page_messages") != null) {
%>
<script language=javascript type='text/javascript'>
function hideDivMsg() {
    if (document.getElementById) { // DOM3 = IE5, NS6
        document.getElementById('hideshowMsg').style.display = 'none';
    }
    else {
        if (document.layers) { // Netscape 4
            document.hideshowMsg.display = 'none';
        }
        else { // IE 4
            document.all.hideshowMsg.style.display = 'none';
        }
    }
}
</script>
<div id="hideshowMsg" style="position: relative; z-index: 999;"><a
	href="javascript:hideDivMsg()">Hide Messages</a>
	<html:messages id="message" message="true">
	<p>
		<bean:write name="message" filter="false"/>
	</p>
	</html:messages>	
</div>
<% } %>



<%

String questionnaireString = "";
  if (request.getAttribute("questionnaire") != null) {
  	Questionnaire questionnaire = (Questionnaire) request.getAttribute("questionnaire");
  	
  	questionnaireString =  "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + request.getAttribute("M_existingFdid") + "'></input>";
   
  	int seq = 1;
  	for (Question question : questionnaire.getQuestions()) {	
  		questionnaireString += "<input class='qAnswer' type='hidden' id='" + "QR_q" + seq + "' name='" + "QR_q" + seq + "' value=''>";			  
  		seq++;
  	}
  }  	  	
  	  	
%>

<%
  String provider_no = (String) session.getValue("user");
  String demographic_no = request.getParameter("demographic_no");
  String appointment_no = request.getParameter("appointment");
  String fid = request.getParameter("fid");
  String eform_link = request.getParameter("eform_link");
  String source = request.getParameter("source");
  String eform_data_id = (String) request.getParameter("fdid");
  if (StringUtils.empty(eform_data_id)) {
	  eform_data_id = (String)request.getAttribute("fdid");
  } 

  EForm thisEForm = null;
  if (fid == null || demographic_no == null) {
      //if the info is in the request attribute
      thisEForm = (EForm) request.getAttribute("curform");
  } else {
      //if the info is in the request parameter
      thisEForm = new EForm(fid, demographic_no);
      thisEForm.setProviderNo(provider_no);  //needs provider for the action
  }
  
  if (thisEForm != null) {
      if (appointment_no != null) thisEForm.setAppointmentNo(appointment_no);
      if (eform_link != null) thisEForm.setEformLink(eform_link);
      thisEForm.setContextPath(request.getContextPath());
      thisEForm.setupInputFields();
      thisEForm.setImagePath();
      thisEForm.setDatabaseAPs();
      thisEForm.setOscarOPEN(request.getRequestURI());
      thisEForm.setAction();
      thisEForm.setSource(source);
      String htmlDocument = thisEForm.getFormHtml();
      htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());
      
      // Pass additional data into the eForm - add here as needed
      htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
      htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
      htmlDocument = htmlDocument.replace("${questionnaire}", questionnaireString);
      String existingFdid = (String) request.getAttribute("M_existingFdid") != null ? (String) request.getAttribute("M_existingFdid") : "";
      htmlDocument = htmlDocument.replace("${existingFdid}", existingFdid);
      out.print(htmlDocument);
  }
%>

<%
String iframeResize = (String) session.getAttribute("useIframeResizing");
if(iframeResize !=null && "true".equalsIgnoreCase(iframeResize)){ %>
<script src="<%=request.getContextPath() %>/library/pym.js"></script>
<script>
    var pymChild = new pym.Child({ polling: 500 });
</script>
<%}%>