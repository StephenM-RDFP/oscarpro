<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.form.dao.SmartEncounterProviderPreferenceDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.model.SmartEncounterProviderPreference" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName2$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_form" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_form");%>
</security:oscarSec>
<% if (!authed) { return; } %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
	
	// get providers header and footer
	SmartEncounterProviderPreferenceDao smartEncounterProviderPreferenceDao = SpringUtils.getBean(SmartEncounterProviderPreferenceDao.class);
	SmartEncounterProviderPreference smartEncounterProviderPreference = smartEncounterProviderPreferenceDao.findByProviderNo(loggedInInfo.getLoggedInProviderNo());
	
%>
<html>
<head>
<title>Smart Encounter Header Form</title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/quill/quill.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>
    
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/quill/quill.snow.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main-kai.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/fonts-min.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/autocomplete.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/demographicProviderAutocomplete.css"/>
    <script type="text/javascript">

        <% if (Boolean.parseBoolean(request.getParameter("success"))) { %>
			window.close();
        <% } %>
        function init() {
            // resize window
            window.resizeTo(1150, 800);
        }

        let headerHtml = <%=smartEncounterProviderPreference != null ? "'" + Encode.forJavaScriptBlock(smartEncounterProviderPreference.getHeaderHtmlText()) + "'" : "null" %>;
        let headerDelta = <%=smartEncounterProviderPreference != null ? smartEncounterProviderPreference.getHeaderDeltaText() : "null" %>;
        let footerHtml = <%=smartEncounterProviderPreference != null ? "'" + Encode.forJavaScriptBlock(smartEncounterProviderPreference.getFooterHtmlText()) + "'" : "null" %>;
        let footerDelta = <%=smartEncounterProviderPreference != null ? smartEncounterProviderPreference.getFooterDeltaText() : "null" %>;
    </script>
</head>
<body onload="init()">
    <html:form styleId="formSmartEncounter" action="/form/FormSmartEncounterAction" styleClass="container" style="min-width: 1000px">
		<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>">
		<input type="hidden" id="method" name="method" value="saveProviderHeaderFooter"/>
		<input type="hidden" id="headerHtmlText" name="headerHtmlText" value=""/>
		<input type="hidden" id="headerDeltaText" name="headerDeltaText" value=""/>
		<input type="hidden" id="footerHtmlText" name="footerHtmlText" value=""/>
		<input type="hidden" id="footerDeltaText" name="footerDeltaText" value=""/>
		<div class="row">
			<div class="col-xs-12">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-form navbar-left">
							<a href="#" class="navbar-brand" style="padding: 6px">Smart Encounter Header and Footer Stamp Settings</a>
						</div>
					</div>
				</nav>
			</div>
		</div>
        <div class="row center-row-content">
            <div class="col-xs-9" style="width: 8in;">
                <div>
                    <label>Header:</label>
				</div>
                <div id="quillHeaderEditor"></div>
            </div>
        </div>
		<div class="row center-row-content">
			<div class="col-xs-9" style="width: 8in;">
				<div>
					<label>Footer:</label>
				</div>
				<div id="quillFooterEditor"></div>
			</div>
		</div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row form-inline">
                            <div class="col-xs-6">
                                <input type="button" class="btn btn-primary" value="Save" onclick="onClickSave()"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </html:form>
    <script type="text/javascript">
        let methodElement = document.getElementById('method');
        let quillHeaderEditor = new TemplatePlaceholderQuill('quillHeaderEditor', null);
        if (headerDelta != null && headerDelta.ops.length > 0) {
            quillHeaderEditor.setContents(headerDelta);
        }
        let quillFooterEditor = new TemplatePlaceholderQuill('quillFooterEditor', null);
        if (footerDelta != null && footerDelta.ops.length > 0) {
            quillFooterEditor.setContents(footerDelta);
        }
        
        
        function setInputElementsToQuillValues() {
            document.getElementById('headerDeltaText').value = JSON.stringify(quillHeaderEditor.getContents());
            document.getElementById('headerHtmlText').value = quillHeaderEditor.getHtml();
            document.getElementById('footerDeltaText').value = JSON.stringify(quillFooterEditor.getContents());
            document.getElementById('footerHtmlText').value = quillFooterEditor.getHtml();
        }
        
        function onClickSave() {
            setInputElementsToQuillValues();
            document.getElementById('formSmartEncounter').submit();
        }
    </script>
</body>
</html>
