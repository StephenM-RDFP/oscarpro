<%
    int rfNum = request.getParameter("rfNum") != null ? Integer.parseInt(request.getParameter("rfNum")) : 0;
%>

<tr id="rf_<%=rfNum%>">
    <td>
        <a class="delete_link"  href="javascript:void(0)" onclick="deleteRiskFactor('<%=rfNum%>'); return false;">[x]</a>&nbsp; <%=rfNum%>
    </td>
    <td>
        <input type="text" name="rf_issues<%=rfNum%>" size="20" maxlength="40" style="width: 100%" >
    </td>
    <td>
        <input type="text" name="rf_plan<%=rfNum%>" size="60" maxlength="85" style="width: 100%" >
    </td>
</tr>