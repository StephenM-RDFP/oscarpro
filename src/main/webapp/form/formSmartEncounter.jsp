<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName2$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_form" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_form");%>
</security:oscarSec>
<% if (!authed) { return; } %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ page import="oscar.form.*"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="java.util.Properties" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="oscar.form.dao.SmartEncounterTemplateDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.model.SmartEncounterTemplate" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="oscar.form.model.SmartEncounterShortCode" %>
<%@ page import="oscar.form.dao.SmartEncounterShortCodeDao" %>
<%@ page import="org.oscarehr.common.dao.ProfessionalSpecialistDao" %>
<%@ page import="org.oscarehr.common.model.ProfessionalSpecialist" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.model.DemographicExt" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="oscar.form.dao.SmartEncounterProviderPreferenceDao" %>
<%@ page import="oscar.form.model.SmartEncounterProviderPreference" %>
<%@ page import="oscar.form.model.SmartEncounterTemplateImage" %>
<%@ page import="org.oscarehr.common.dao.SystemPreferencesDao" %>
<%
    Logger logger = MiscUtils.getLogger();
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    int formId = Integer.parseInt(request.getParameter("formId"));
    int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
    String appointmentNo = StringUtils.trimToEmpty(request.getParameter("appointmentNo"));
    String errorMessage = request.getParameter("error_message");
    FrmRecord rec = (new FrmRecordFactory()).factory("SmartEncounter");
    Properties formProperties = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request), demographicNo, formId);
    String deltaText = formProperties.getProperty("deltaText", "");
    // replace image placeholders with proper urls
	for (Object o : formProperties.keySet()) {
		String key = (String) o;
		if (key.startsWith("image_")) {
			deltaText = deltaText.replaceAll("\\$\\{" + key + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + formProperties.getProperty(key));
		}
	}

    SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
    List<SmartEncounterTemplate> templateList = smartEncounterTemplateDao.findAllActiveOrderByName();
    SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
    List<SmartEncounterShortCode> shortCodeList = smartEncounterShortCodeDao.findAll(null, null);

    // get Family Doctor and Referral Doctor
    ProfessionalSpecialist familyDoctor = null;
    ProfessionalSpecialist referralDoctor = null;
    ProfessionalSpecialistDao professionalSpecialistDao = SpringUtils.getBean(ProfessionalSpecialistDao.class);
    DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
    DemographicExt familyDoctorExt = demographicExtDao.getDemographicExt(demographicNo, ProfessionalSpecialist.DemographicRelationship.FAMILY_DOCTOR.getDemographicExtKeyVal());
    DemographicExt referralDoctorExt = demographicExtDao.getDemographicExt(demographicNo, ProfessionalSpecialist.DemographicRelationship.REFERRAL_DOCTOR.getDemographicExtKeyVal());
    if (familyDoctorExt != null) {
        try {
            familyDoctor = professionalSpecialistDao.find(Integer.valueOf(familyDoctorExt.getValue()));
        } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + familyDoctorExt.getValue()); }
    }
    if (referralDoctorExt != null) {
        try {
            referralDoctor = professionalSpecialistDao.find(Integer.valueOf(referralDoctorExt.getValue()));
        } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + referralDoctorExt.getValue()); }
    }
    
    // get providers header and footer
	SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
	SmartEncounterProviderPreference smartEncounterProviderPreference = null;
	if (systemPreferencesDao.isPreferenceValueEquals("smart_template_provider_header", "true")) {
		SmartEncounterProviderPreferenceDao smartEncounterProviderPreferenceDao = SpringUtils.getBean(SmartEncounterProviderPreferenceDao.class);
		smartEncounterProviderPreference = smartEncounterProviderPreferenceDao.findByProviderNo(loggedInInfo.getLoggedInProviderNo());
	}
%>
<html>
<head>
<meta charset="utf-8">
<title>Smart Encounter Form</title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/quill/quill.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-block-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-image-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>
    
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/quill/quill.snow.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main-kai.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/fonts-min.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/autocomplete.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/demographicProviderAutocomplete.css"/>
    <script type="text/javascript">
        function init() {
            // resize window
            window.resizeTo(1150, 800);
        }
        
        let printoutHeaderHtml = <%=smartEncounterProviderPreference != null ? "'" + Encode.forJavaScriptBlock(smartEncounterProviderPreference.getHeaderHtmlText()) + "'" : "null" %>;
        let printoutFooterHtml = <%=smartEncounterProviderPreference != null ? "'" + Encode.forJavaScriptBlock(smartEncounterProviderPreference.getFooterHtmlText()) + "'" : "null" %>;
        
        <%
        List<String> jsTemplates = new ArrayList<String>();
        for (SmartEncounterTemplate template : templateList) {
            List<String> imageJson = new ArrayList<String>();
            for (SmartEncounterTemplateImage smartEncounterTemplateImage : template.getPlaceholderImageList()) {
                imageJson.add("'" + smartEncounterTemplateImage.getName() + "' : '" + smartEncounterTemplateImage.getValue() + "'");
            }
            String templateDeltaText = template.getTemplate();
			// replace image placeholders with proper urls
			for (SmartEncounterTemplateImage placeHolderImage : template.getPlaceholderImageList()) {
			    templateDeltaText = templateDeltaText.replaceAll("\\$\\{" + placeHolderImage.getName() + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + placeHolderImage.getValue());
			}
            jsTemplates.add("['" + template.getId() + "', { 'name':'" + template.getName() + "', 'template': " + templateDeltaText + "}]");
        }
        %>
        let templatesMap = new Map([<%=StringUtils.join(jsTemplates, ",")%>]);
        <%
        List<String> jsShortCodes = new ArrayList<String>();
        for (SmartEncounterShortCode shortCode : shortCodeList) {
            jsShortCodes.add("['" + shortCode.getName().toLowerCase() + "', '" + Encode.forJavaScriptBlock(shortCode.getText()) + "']");
        }
        %>
        let shortCodesMap = new Map([<%=StringUtils.join(jsShortCodes, ",")%>]);
        
        let demographicPlaceholderValues = <%=formProperties.getProperty("resolvedPlaceholders", "[]")%>
    
    </script>
</head>
<body onload="init()">
    <html:form styleId="formSmartEncounter" action="/form/FormSmartEncounterAction" styleClass="container" style="min-width: 1000px">
        <div class="row">
            <div class="col-xs-12">
                <input type="hidden" id="method" name="method" value="">
                <input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>">
                <input type="hidden" name="formId" value="<%=formId%>">
                <input type="hidden" name="demographicNo" value="<%=demographicNo%>">
                <input type="hidden" name="appointmentNo" value="<%=appointmentNo%>">
                <input type="hidden" id="templateUsed" name="templateUsed" value="<%=Encode.forHtmlContent(formProperties.getProperty("templateUsed", ""))%>">
                <input type="hidden" id="deltaText" name="deltaText" value="<%=Encode.forHtmlContent(deltaText)%>">
                <input type="hidden" id="htmlText" name="htmlText" value="<%=Encode.forHtmlContent(formProperties.getProperty("htmlText", ""))%>">
                <input type="hidden" id="plainText" name="plainText" value="<%=formProperties.getProperty("plainText", "")%>">
                <input type="hidden" id="fieldsToPullListString" name="fieldsToPullListString" value="">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-form navbar-left">
                            <a href="#" class="navbar-brand" style="padding: 6px">Smart Encounter</a>
                            <div class="form-group">
                                <select id="template-select" class="form-control" name="templateSelect" onchange="setTemplate(this.value)">
                                    <option value="">Select Template:</option>
                                    <% for (SmartEncounterTemplate template : templateList) { %>
                                    <option value="<%=template.getId()%>"><%=template.getName()%></option>
                                    <% } %>
                                </select>
                            </div>
                        </div>
                        <div class="navbar-right" style="padding: 8px">
                            Referring: <%=referralDoctor != null ? referralDoctor.getFormattedName() : "(none)"%><br/>
                            Family: <%=familyDoctor != null ? familyDoctor.getFormattedName() : "(none)"%>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row center-row-content" style="margin-bottom: 130px;">
            <div class="col-xs-9" style="width: 8in;">
                <div class="row">
                    <div class="col-xs-12">
                        <label>Document Name:</label>
                        <input type="text" class="form-control input-sm" id="documentName" name="documentName" value="<%=Encode.forHtmlContent(formProperties.getProperty("documentName", ""))%>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="display: inherit;">
                        <div id="quillEditor"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2" style="min-width: 220px;">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <% if (errorMessage != null) { %>
                        <div id="error-messages" class="alert alert-danger">
                            <%=errorMessage%>
                        </div>
                        <% } %><br/>
                        <div class="btn-group-vertical" role="group" style="width: 100%;">
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('label');" value="Patient Block"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('social_family_historyc');" value="Social History"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('medical_history');" value="Medical History"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('ongoingconcerns');" value="Ongoing Concerns"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('reminders');" value="Reminders"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('allergies_des');" value="Allergies"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('druglist_trade');" value="Prescriptions"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('other_medications_history'); " value="Other Medications"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('family_history_json'); " value="Family History"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('first_last_name');" value="Patient Name"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('ageComplex');" value="Patient Age"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('label');" value="Patient Label"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('sex');" value="Patient Gender"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('current_user');" value="Current User"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('doctor');" value="Doctor (MRP)"/>
                            <input type="button" class="btn btn-default" onclick="printCommonMacro('recent_note')" value="RecentNote"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row form-inline">
                            <div class="col-xs-6">
                                <label for="faxRecipientsSearch">Fax Recipients:
                                    <input type="text" class="form-control input-sm" style="width: 180px" id="faxRecipientsSearch" name="faxRecipientsSearch"/>
                                </label>
                                <ul id="faxRecipientsList"></ul>
                            </div>
                            <div class="col-xs-6">
                                <input type="button" class="btn btn-primary" value="Save" onclick="onClickSave()"/>
                                <input type="button" class="btn btn-primary" value="Fax and Print" onclick="onClickFaxAndPrint()" <%=formId == 0 ? "disabled" : ""%>/>
                                <input type="button" class="btn btn-primary" value="Print" onclick="onClickPrint()" <%=formId == 0 ? "disabled" : ""%>/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </html:form>
    <script type="text/javascript">
        let csrfToken = { name: '<csrf:tokenname/>', value: '<csrf:tokenvalue/>'};
        let methodElement = document.getElementById('method');
        let existingDelta = <%=deltaText.isEmpty() ? "{ops: [] }" : "JSON.parse('" + Encode.forJavaScriptBlock(deltaText) + "')"%>;
        let quillEditor = new TemplatePlaceholderQuill('quillEditor', shortCodesMap, demographicPlaceholderValues);
        if (existingDelta.ops.length > 0) {
            quillEditor.setContents(existingDelta);
        }

        function setTemplate(templateId) {
            let template = templatesMap.get(templateId);
            if (quillEditor.getText()) {
                if (confirm('Warning: Selecting a new template will overwrite the existing note content. Continue with new template?')) {
                    quillEditor.setContents(template['template']);
                    document.getElementById('documentName').value = template['name'];
                    document.getElementById('templateUsed').value = templateId;
                    quillEditor.fetchAndApplyTemplatePlaceHoldersWithFilledValues(<%=demographicNo%>, '<%=request.getContextPath()%>/form/FormSmartEncounterAction.do', csrfToken);
                }
            } else {
                quillEditor.setContents(template['template']);
                document.getElementById('documentName').value = template['name'];
                document.getElementById('templateUsed').value = templateId;
                quillEditor.fetchAndApplyTemplatePlaceHoldersWithFilledValues(<%=demographicNo%>, '<%=request.getContextPath()%>/form/FormSmartEncounterAction.do', csrfToken);
            }
            document.getElementById('template-select').value = '';
        }
        
        function setInputElementsToQuillValues() {
            let fieldsToPullArray = quillEditor.getTemplatePlaceholderMarkers();
            let imagesToSaveArray = quillEditor.getDocumentImages();
            let formElement = document.getElementById('formSmartEncounter');
            for (let i = 0; i < imagesToSaveArray.length; i++) {
                let imageDataToSave = imagesToSaveArray[i].insert.ImagePlaceholder;
                let fileInput = document.createElement('input');
                fileInput.setAttribute('type', 'hidden');
                fileInput.setAttribute('name', 'images');
                fileInput.setAttribute('value', imageDataToSave.identifier + ';' + imageDataToSave.data);
                formElement.appendChild(fileInput);
            }
            
            document.getElementById('deltaText').value = JSON.stringify(quillEditor.getContentsWithImagePlaceholders());
            document.getElementById('fieldsToPullListString').value = fieldsToPullArray.join(';');
            let formattedData = quillEditor.getFormattedHtmlAndText(printoutHeaderHtml, printoutFooterHtml);
            document.getElementById('htmlText').value = formattedData.html;
            document.getElementById('plainText').value = formattedData.text;
        }
        
        function onClickSave() {
            methodElement.value = 'save';
            setInputElementsToQuillValues();
            document.getElementById('formSmartEncounter').submit();
        }
        function onClickFaxAndPrint() {
            if (document.getElementsByName('faxRecipient').length === 0) {
                window.alert('Cannot Fax: No recipients selected')
            } else {
                methodElement.value = 'faxAndPrint';
                setInputElementsToQuillValues();
                document.getElementById('formSmartEncounter').submit();
            }
        }
        function onClickPrint() {
            methodElement.value = 'print';
            setInputElementsToQuillValues();
            document.getElementById('formSmartEncounter').submit();
            quillEditor.updateDomWithResolvedPlaceHolderValues();
        }

        // Fax Recipient autocomplete
        function setupReferralDoctorAutoCompletion() {
            jQuery('#faxRecipientsSearch').autocomplete({
                source: '<%=request.getContextPath()%>/professionalSpecialist/Search.do',
                minLength: 2,
                focus: function(event, data) {
                    jQuery('#faxRecipientsSearch').val('');
                    return false;
                },
                select: function(event, data) {
                    jQuery('#faxRecipientsSearch').val('');
                    let faxRecipientListElement = jQuery('#faxRecipientsList');
                    faxRecipientListElement.append('<li>' + data.item.label + ' (' + data.item.value + ') <a onclick="removeFaxRecipient(this)">remove</a><input type="hidden" class="fax-recipient" name="faxRecipient" value="' + data.item.value + '"/></li>');
                    return false;
                }
            });
        }
        jQuery(setupReferralDoctorAutoCompletion());

        function removeFaxRecipient(thisElement) {
            $(thisElement).parent().remove()
        }
        
        // Common Macros, using the APCache.js
        let lastUsedCommonMacroKey = null;
        function printCommonMacro(key) {
            if (key) {
                cache.lookup(key);
            }
        }

        let cache = createCache({
            defaultCacheResponseHandler: function(type) {
                if (checkKeyResponse(type)) {
                    quillEditor.insertTextAtCaret(cache.get(type).replace(/<br>/g, '\n'));
                }

            },
            cacheResponseErrorHandler: function(xhr, error) {
                alert("Please contact an administrator, an error has occurred.");
            },
            serviceUrlContext: '<%=request.getContextPath()%>/eform/'
        });

        function checkKeyResponse(response) {
            if (cache.isEmpty(response)) {
                alert("The requested value has no content.");
                return false;
            }
            return true;
        }
    </script>
</body>
</html>
