function historyRow(historyObj) {
    var row = "";
    if (historyObj != null) {
        row =  "<tr>" +
            "<td>&nbsp;</td>" +
            "<td>" + historyObj.dateTime +"</td>" +
            "<td>" + historyObj.action +"</td>" +
            "<td>" + historyObj.data +"</td>" +
            "<td>" + historyObj.provider +"</td>" +
            "<td>" + historyObj.file +"</td>" +
            "</tr>";
    }
    return row;
}

function serviceHistory(services) {
    var rows = "";
    for (var i in services) {
        rows += "<tr>" +
            "<th>" + services[i].service + "</th>" +
            "<th colspan='5'>&nbsp;</th>" +
            "</tr>";
        
        var serviceHistory = services[i].history;
        for (var j in serviceHistory) {
            rows += (historyRow(serviceHistory[j]));
        }
    }
    return rows;
}

function updateHistory(filter) {
    $("#loading").show();
    $("#historyBody").html("");
    
    var billingNo = $("#billing_no").val();
    var url = "?filter=" + filter + "&billingNo=" + billingNo + " #history";
    $.ajax({
        data : {
            billingNo: billingNo,
            filter: filter,
            method: 'getFilteredInvoiceHistory'
        },
        dataType : 'json',
        type: 'post',
        url : '../../InvoiceHistory.do',
        success : function(history) {
            if (filter.startsWith("service_code")) {
                $("#historyBody").append(serviceHistory(history));
            } else {
                for (var i in history) {
                    $("#historyBody").append(historyRow(history[i]));
                }
            }

            $("#loading").hide();
        },
        error : function(data) {
            console.log(data);
            $("#historyBody").html('<tr>Error retrieving history</tr>');
            $("#loading").hide();
        }
    });
    
    
}