
function init(rules) {
    $('input[type=checkbox]').bootstrapToggle();

    $("input[type=checkbox].categoryToggle").bind("change", function() {
        if ($(this).attr("id")) {
            toggleCodes($(this).attr("id"));
        }
    });

    if (rules != null) {
        $('.code').each(function(i) {
            let code = $(this).attr('id');
           
            if (rules[code] == true){
                let category =  $(this).attr('category');
                let categoryToggle = category ? $("#" + category) : null;
                if (categoryToggle != null && !categoryToggle[0].checked) {
                    categoryToggle.prop('checked', rules[code]).change();
                }
            }
        });
    }
}

function save() {
    $("#billingRuleSettings").submit();
}

function toggleCodes(category) {
    $('input[category='+category+']').each(function(i) {
        $(this).attr('value', $('#'+category)[0].checked.toString());
    });
}