function checkServiceCode() {
    let b = true;
    let serviceCode = document.forms[1].service_code.value;
    if(serviceCode.length==0){
        b = false;
        alert ("You must type in a service code with letters/digits.");
    } else if (document.forms[0].service_code_select && document.forms[0].service_code_select.options.length) {
        b = Array.from(document.forms[0].service_code_select.options).filter(o => o.value == serviceCode).length === 0;
        if (!b) {
            alert("Code already exists");
        }
    }
    return b;
}

function checkAllFields() {
    let b = true;
    b = checkServiceCode();
    if(document.forms[1].value.value.length>0) {
        if(!isNumber(document.forms[1].value.value)){
            b = false;
            alert ("You must type in a number in the field fee");
        }
    } else if(document.forms[1].value.value.length==0) {
        b = false;
        alert ("You must type in a number in the field fee");
    }

    if(document.forms[1].billingservice_date.value.length<10) {
        b = false;
        alert ("You need to select a date from the calendar.");
    }
    return b;
}

function isNumber(s){
    let i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        let c = s.charAt(i);
        if (c == ".") continue;
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function onDelete() {
    let ret = checkServiceCode();
    if(ret==true) {
        ret = confirm("Are you sure you want to Delete?");
    }
    return ret;
}

function onSave() {
    //document.forms[1].submit.value="Save";
    let ret = checkServiceCode();
    if(ret==true) {
        ret = checkAllFields();
    }
    if(ret==true) {
        ret = confirm("Are you sure you want to save?");
    }
    return ret;
}

function onSearch() {
    //document.forms[1].submit.value="Search";
    let ret = checkServiceCode();
    return ret;
}

function setFlag(){
    if(document.getElementById("gstCheck").checked == true){
        document.getElementById("gstFlag").value = "1";
    } else {
        document.getElementById("gstFlag").value = "0";
    }
}

function upCaseCtrl(ctrl) {
    ctrl.value = ctrl.value.toUpperCase();
}

