package org.oscarehr.integration.excelleris.eorder;

import org.apache.http.client.HttpClient; 

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

public class HttpClientHelperTest {
	
	@Test
	public void testCreateHttpClient() throws Exception {
		
		Map<String, String> cookieMap = new HashMap<>();
		HttpClient httpClient = HttpClientHelper.createHttpClient(cookieMap);
		
		assertNotNull("should not be null", httpClient);	
	}
}