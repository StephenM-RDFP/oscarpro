package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.Questionnaire.AnswerFormat;
import org.hl7.fhir.instance.model.Questionnaire.QuestionComponent;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.OrderLabTestCodeDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCode;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.api.QuestionType;

public class QuestionConverterTest {
	
	private OrderLabTestCodeDao orderLabTestCodeDao;
	
	@Before
	public void setUp() {
		this.orderLabTestCodeDao = new MockOrderLabTestCodeDao();
	}

	@Test(expected = ServiceException.class)
	public void testToApiObject_nullQuestion() {
		QuestionConverter.toApiObject(null, new HashMap<String, EFormValue>(), orderLabTestCodeDao);
	}

	@Test(expected = ServiceException.class)
	public void testToApiObject_nullValueMap() {
		QuestionConverter.toApiObject(new QuestionComponent(), null, orderLabTestCodeDao);
	}

	@Test
	public void testToApiObject_hardCodedTest() {

		QuestionComponent fhirQuestion = new QuestionComponent();
		fhirQuestion.setType(AnswerFormat.CHOICE);
		fhirQuestion.setLinkId(
				"GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:");
		fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");

		Coding coding1 = new Coding();
		coding1.setCode("Chronic kidney failure");
		coding1.setDisplay("Chronic kidney failure");

		Coding coding2 = new Coding();
		coding2.setCode("Iron overload");
		coding2.setDisplay("Iron overload");

		List<Coding> options = new ArrayList<Coding>();
		options.add(coding1);
		options.add(coding2);

		fhirQuestion.addOption(coding1);
		fhirQuestion.addOption(coding2);

		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("T_Iron__24h_Urine");
		eformValue.setVarValue("TR10259-0U");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Question question = QuestionConverter.toApiObject(fhirQuestion, valueMap, this.orderLabTestCodeDao);

		assertEquals("Iron and Ferritin together require one of the following diagnosis:", question.getQuestionText());
		assertEquals(QuestionType.CHOICE, question.getType());
		assertEquals(
				"GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:",
				question.getLinkId());
		assertTrue(question.getOtherTestFieldId() == null);
		assertEquals("T_Iron__24h_Urine", question.getHardCodedTestFieldId());
		assertEquals("Iron 24h Urine", question.getTestName());
		assertEquals("TR10259-0U", question.getTestCode());
		assertEquals("Iron and Ferritin together require one of the following diagnosis:", question.getItemId());
		assertEquals(2, question.getChoices().size());
		assertEquals("Iron overload", question.getChoices().get(1).getCode());
		assertEquals("Iron overload", question.getChoices().get(1).getText());
		assertEquals("Chronic kidney failure", question.getChoices().get(0).getCode());
		assertEquals("Chronic kidney failure", question.getChoices().get(0).getText());
	}

	@Test
	public void testToApiObject_otherTest() {

		QuestionComponent fhirQuestion = new QuestionComponent();
		fhirQuestion.setType(AnswerFormat.CHOICE);
		fhirQuestion.setLinkId(
				"GUI|TestCode|TR10259-0|Global|1|Iron and Ferritin together require one of the following diagnosis:");
		fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");


		Coding coding1 = new Coding();
		coding1.setCode("Chronic kidney failure");
		coding1.setDisplay("Chronic kidney failure");

		Coding coding2 = new Coding();
		coding2.setCode("Iron overload");
		coding2.setDisplay("Iron overload");

		List<Coding> options = new ArrayList<Coding>();
		options.add(coding1);
		options.add(coding2);

		fhirQuestion.addOption(coding1);
		fhirQuestion.addOption(coding2);
		Question question = QuestionConverter.toApiObject(fhirQuestion, new HashMap<String, EFormValue>(),
				this.orderLabTestCodeDao);

		assertEquals("Iron and Ferritin together require one of the following diagnosis:", question.getQuestionText());
		assertEquals(QuestionType.CHOICE, question.getType());
		assertEquals(
				"GUI|TestCode|TR10259-0|Global|1|Iron and Ferritin together require one of the following diagnosis:",
				question.getLinkId());
		assertTrue(question.getHardCodedTestFieldId() == null);
		assertEquals("Iron", question.getOtherTestFieldId());
		assertEquals("Iron", question.getTestName());
		assertEquals("TR10259-0", question.getTestCode());
		assertEquals("Iron and Ferritin together require one of the following diagnosis:", question.getItemId());
		assertEquals(2, question.getChoices().size());
		assertEquals("Iron overload", question.getChoices().get(1).getCode());
		assertEquals("Iron overload", question.getChoices().get(1).getText());
		assertEquals("Chronic kidney failure", question.getChoices().get(0).getCode());
		assertEquals("Chronic kidney failure", question.getChoices().get(0).getText());
	}

	public class MockOrderLabTestCodeDao extends OrderLabTestCodeDao {

		List<OrderLabTestCode> orderLabTestCodes;

		public MockOrderLabTestCodeDao() {


			orderLabTestCodes = new ArrayList<OrderLabTestCode>();

			OrderLabTestCode labTestCode = new OrderLabTestCode();
			labTestCode.setId(1);
			labTestCode.setSearchable(true);
			labTestCode.setTestOnForm(false);
			labTestCode.setRequireAppointment(false);
			labTestCode.setPatientPay(false);
			labTestCode.setTestName("Iron");
			labTestCode.setLabTestCode("TR10259-0");
			labTestCode.setBaseBCOTC("TR10259-0");
			orderLabTestCodes.add(labTestCode);

			OrderLabTestCode labTestCode2 = new OrderLabTestCode();
			labTestCode2.setId(2);
			labTestCode2.setSearchable(true);
			labTestCode2.setTestOnForm(false);
			labTestCode2.setRequireAppointment(false);
			labTestCode2.setPatientPay(false);
			labTestCode2.setTestName("Blood");
			labTestCode2.setLabTestCode("TR10222-0");
			labTestCode2.setBaseBCOTC("TR10222-0");
			orderLabTestCodes.add(labTestCode2);

			OrderLabTestCode labTestCode3 = new OrderLabTestCode();
			labTestCode3.setId(3);
			labTestCode3.setSearchable(true);
			labTestCode3.setTestOnForm(false);
			labTestCode3.setRequireAppointment(false);
			labTestCode3.setPatientPay(false);
			labTestCode3.setTestName("Iron 24h Urine");
			labTestCode3.setLabTestCode("TR10259-0U");
			labTestCode3.setBaseBCOTC("TR10259-0U");
			orderLabTestCodes.add(labTestCode3);

		}



		public OrderLabTestCode findByTestCode(String testCode) {

			for (OrderLabTestCode orderLabTestCode : this.orderLabTestCodes) {
				if (orderLabTestCode.getLabTestCode().equals(testCode)) {
					return orderLabTestCode;
				}

			}

			return null;

		}

	}

}
