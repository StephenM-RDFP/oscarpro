package org.oscarehr.integration.excelleris.eorder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.model.Resource;
import org.junit.Test;

public class OrderResourceHelperTest {
	

	
	@Test
	public void errorTest() throws Exception {
        
		// prepare test data
		JsonParser parser = new JsonParser();		
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-1.json");
		Resource resource = parser.parse(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getErrors());
		assertTrue(helper.getErrors().isEmpty());
	}
	
	@Test
	public void orderIdTest() throws Exception {
        
		// prepare test data
		JsonParser parser = new JsonParser();		
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");
		Resource resource = parser.parse(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getOrderId());
	}
	
	@Test
	public void pdfLabRequisitionFormTest() throws Exception {
        
		// prepare test data
		JsonParser parser = new JsonParser();		
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");
		Resource resource = parser.parse(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getPdfLabRequisitionForm());
	}
	
	@Test
	public void patientIdTest() throws Exception {
        
		// prepare test data
		JsonParser parser = new JsonParser();		
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");
		Resource resource = parser.parse(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getPatientId());
	}
}
