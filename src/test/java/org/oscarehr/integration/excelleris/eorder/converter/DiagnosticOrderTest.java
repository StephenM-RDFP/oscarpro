package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.model.BooleanType;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.DateTimeType;
import org.hl7.fhir.instance.model.DiagnosticOrder;
import org.hl7.fhir.instance.model.DiagnosticOrder.DiagnosticOrderItemComponent;
import org.hl7.fhir.instance.model.DiagnosticOrder.DiagnosticOrderStatus;
import org.hl7.fhir.instance.model.Extension;
import org.hl7.fhir.instance.model.Questionnaire;
import org.hl7.fhir.instance.model.QuestionnaireResponse;
import org.hl7.fhir.instance.model.QuestionnaireResponse.QuestionComponent;
import org.hl7.fhir.instance.model.Reference;
import org.hl7.fhir.instance.model.StringType;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;

public class DiagnosticOrderTest {
	
	

	@Test
	public void TestToFhirObject() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
		eformValues
				.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1113"));
		eformValues.add(EFormValueHelper.createEFormValue(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID,
				"DAV77092A"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
		eformValues.add(EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, "/encounterURL"));
		eformValues.add(EFormValueHelper.createEFormValue("T_CBC", "CBC"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);


		DiagnosticOrder order = DiagnosticOrderConverter.toFhirObject(valueMap, new QuestionnaireResponse(), null);

		assertEquals(DiagnosticOrderStatus.REQUESTED, order.getStatus());
//		pregnant
		Extension pregnantExtension = order.getExtension().get(0);
		BooleanType pregnantValue = (BooleanType) pregnantExtension.getValue();
		assertTrue(pregnantExtension.getUrl().endsWith("diagnosticorder-subjectpregnancy"));
		assertEquals(true, pregnantValue.getValue());

		Extension specimanCollectionExtension = order.getExtension().get(1);
		DateTimeType specimanCollectionValue = (DateTimeType) specimanCollectionExtension.getValue();
		assertTrue(specimanCollectionExtension.getUrl().endsWith("diagnosticorder-collectiondatetime"));
		assertEquals(new DateTimeType("2020-02-11T01:13:01+01:00").getValue(), specimanCollectionValue.getValue());

		Extension additionalInfoExtension = order.getExtension().get(2);
		StringType additionalInfoValue = (StringType) additionalInfoExtension.getValue();
		assertTrue(additionalInfoExtension.getUrl().endsWith("diagnosticorder-subjectdiagnosis"));
		assertEquals("pos", additionalInfoValue.getValue());

		Extension codeableConceptExtension = order.getExtension().get(3);
		CodeableConcept codeableConcept = (CodeableConcept) codeableConceptExtension.getValue();

		assertTrue(codeableConceptExtension.getUrl().endsWith("diagnosticorder-classification"));
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("Diagnosis", codeableConcept.getCoding().get(0).getCode());
		assertTrue(codeableConcept.getCoding().get(0).getSystem().endsWith("ValueSet/diagnosis-code"));

		assertTrue(order.getSubject().getReference().endsWith("/Patient/PAT1113"));

		assertEquals("/encounterURL", order.getEncounter().getReference());

		assertTrue(order.getOrderer().getReference().endsWith("/Practitioner/DAV77092A"));
//		copy to provider
		Extension copyToProviderExtension = order.getExtension().get(4);
		Reference copyToProviderReference = (Reference) copyToProviderExtension.getValue();
		assertTrue(copyToProviderExtension.getUrl().endsWith("diagnosticorder-copytorecipient"));
		assertTrue(copyToProviderReference.getReference().endsWith("/Practitioner/DAV77092A"));

		assertEquals(1, order.getItem().size());
		DiagnosticOrderItemComponent labTest = order.getItem().get(0);
		assertEquals(DiagnosticOrderStatus.REQUESTED, labTest.getStatus());

		Extension labtestExtension = labTest.getExtension().get(0);
		CodeableConcept labtestExtensionCC = (CodeableConcept) labtestExtension.getValue();

		assertTrue(labtestExtension.getUrl().endsWith("diagnosticorder-item-payertype"));
		assertEquals(1, labtestExtensionCC.getCoding().size());
		assertEquals("Patient", labtestExtensionCC.getCoding().get(0).getCode());
		assertTrue(labtestExtensionCC.getCoding().get(0).getSystem().endsWith("ValueSet/payer-type"));

		Extension labtestExtension2 = labTest.getExtension().get(1);
		CodeableConcept labtestExtensionCC2 = (CodeableConcept) labtestExtension2.getValue();

		assertTrue(labtestExtension2.getUrl().endsWith("diagnosticorder-item-tiebreaker"));
		assertEquals(1, labtestExtensionCC2.getCoding().size());
		assertEquals("0", labtestExtensionCC2.getCoding().get(0).getCode());
		assertTrue(labtestExtensionCC2.getCoding().get(0).getSystem()
				.endsWith("ValueSet/diagnosticorder-item-tiebreaker"));

		assertTrue(labTest.getCode().getCoding().get(0).getSystem().endsWith("ValueSet/ordercode"));

	}

	
	@Test
	public void TestToFhirObject_otherTestAndQuestionnaire() throws Exception {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();

		String JSONQuestionnaire = "{\"resourceType\":\"Questionnaire\",\"version\":\"1.0\",\"group\":{\"question\":[{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-questionItem\",\"valueString\":\"e45fb235-083d-4eb0-a9f7-98d31aa36fe7\"}],\"linkId\":\"GUI|TestCode|TR10259-0|Global|1|Iron and Ferritin together require one of the following diagnosis:\",\"text\":\"Iron and Ferritin together require one of the following diagnosis:\",\"type\":\"choice\",\"required\":true,\"option\":[{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Chronic inflammation\",\"display\":\"Chronic inflammation\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Chronic kidney failure\",\"display\":\"Chronic kidney failure\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Iron overload\",\"display\":\"Iron overload\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Iron excess\",\"display\":\"Iron excess\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Hemochromatosis\",\"display\":\"Hemochromatosis\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"Hemosidoris\",\"display\":\"Hemosidoris\"},{\"extension\":[{\"url\":\"https://api.excelleris.com/1.0/eorder/questionnaire-question-option-description\"}],\"code\":\"None of the above. Note: The Iron test will be removed from the requisition.\",\"display\":\"None of the above. Note: The Iron test will be removed from the requisition.\"}]}]}}";
		JsonParser parser = new JsonParser();
		Questionnaire questionnaire = null;
		try {
			questionnaire = (Questionnaire) parser.parse(JSONQuestionnaire);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1113"));
		eformValues.add(EFormValueHelper.createEFormValue(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID,
				"DAV77092A"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
		eformValues.add(EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, "/encounterURL"));
		eformValues.add(EFormValueHelper.createEFormValue("T_other_tests_1", "Iron"));
//		eformValues.add(EFormValueHelper.createEFormValue("TR10259-0", "TR10259-0"));
		eformValues.add(EFormValueHelper.createEFormValue("QR_q1", "Chronic kidney failure"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse();
		DiagnosticOrder order = DiagnosticOrderConverter.toFhirObject(valueMap, questionnaireResponse,
				questionnaire);

		assertEquals(DiagnosticOrderStatus.REQUESTED, order.getStatus());
//		pregnant
		Extension pregnantExtension = order.getExtension().get(0);
		BooleanType pregnantValue = (BooleanType) pregnantExtension.getValue();
		assertTrue(pregnantExtension.getUrl().endsWith("diagnosticorder-subjectpregnancy"));
		assertEquals(true, pregnantValue.getValue());

		Extension specimanCollectionExtension = order.getExtension().get(1);
		DateTimeType specimanCollectionValue = (DateTimeType) specimanCollectionExtension.getValue();
		assertTrue(specimanCollectionExtension.getUrl().endsWith("diagnosticorder-collectiondatetime"));
		assertEquals(new DateTimeType("2020-02-11T01:13:01+01:00").getValue(), specimanCollectionValue.getValue());

		Extension additionalInfoExtension = order.getExtension().get(2);
		StringType additionalInfoValue = (StringType) additionalInfoExtension.getValue();
		assertTrue(additionalInfoExtension.getUrl().endsWith("diagnosticorder-subjectdiagnosis"));
		assertEquals("pos", additionalInfoValue.getValue());

		Extension codeableConceptExtension = order.getExtension().get(3);
		CodeableConcept codeableConcept = (CodeableConcept) codeableConceptExtension.getValue();

		assertTrue(codeableConceptExtension.getUrl().endsWith("diagnosticorder-classification"));
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("Diagnosis", codeableConcept.getCoding().get(0).getCode());
		assertTrue(codeableConcept.getCoding().get(0).getSystem().endsWith("ValueSet/diagnosis-code"));

		assertTrue(order.getSubject().getReference().endsWith("/Patient/PAT1113"));

		assertEquals("/encounterURL", order.getEncounter().getReference());

		assertTrue(order.getOrderer().getReference().endsWith("/Practitioner/DAV77092A"));
//		copy to provider
		Extension copyToProviderExtension = order.getExtension().get(4);
		Reference copyToProviderReference = (Reference) copyToProviderExtension.getValue();
		assertTrue(copyToProviderExtension.getUrl().endsWith("diagnosticorder-copytorecipient"));
		assertTrue(copyToProviderReference.getReference().endsWith("/Practitioner/DAV77092A"));

		// assertEquals(1, order.getItem().size());
		DiagnosticOrderItemComponent labTest = order.getItem().get(0);
		assertEquals(DiagnosticOrderStatus.REQUESTED, labTest.getStatus());

		Extension labtestExtension = labTest.getExtension().get(0);
		CodeableConcept labtestExtensionCC = (CodeableConcept) labtestExtension.getValue();

		assertTrue(labtestExtension.getUrl().endsWith("diagnosticorder-item-payertype"));
		assertEquals(1, labtestExtensionCC.getCoding().size());
		assertEquals("Patient", labtestExtensionCC.getCoding().get(0).getCode());
		assertTrue(labtestExtensionCC.getCoding().get(0).getSystem().endsWith("ValueSet/payer-type"));

		Extension labtestExtension2 = labTest.getExtension().get(1);
		CodeableConcept labtestExtensionCC2 = (CodeableConcept) labtestExtension2.getValue();

		assertTrue(labtestExtension2.getUrl().endsWith("diagnosticorder-item-tiebreaker"));
		assertEquals(1, labtestExtensionCC2.getCoding().size());
		assertEquals("0", labtestExtensionCC2.getCoding().get(0).getCode());
		assertTrue(labtestExtensionCC2.getCoding().get(0).getSystem()
				.endsWith("ValueSet/diagnosticorder-item-tiebreaker"));

		assertTrue(labTest.getCode().getCoding().get(0).getSystem().endsWith("ValueSet/ordercode"));

		assertEquals(1, questionnaireResponse.getGroup().getQuestion().size());
		QuestionComponent questionComponent = questionnaireResponse.getGroup().getQuestion().get(0);
		assertEquals("Chronic kidney failure",
				questionComponent.getAnswer().get(0).getValueStringType().asStringValue());

	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_noEncounterUrl() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1113"));
		eformValues.add(EFormValueHelper.createEFormValue(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID,
				"DAV77092A"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
//		eformValues.add(EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, "/encounterURL"));
		eformValues.add(EFormValueHelper.createEFormValue("T_CBC", "CBC"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		DiagnosticOrderConverter.toFhirObject(valueMap, new QuestionnaireResponse(), null);

	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_noOrderinProviderId() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1113"));
//		eformValues.add(EFormValueHelper.createEFormValue(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID,
//				"DAV77092A"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
		eformValues.add(EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, "/encounterURL"));
		eformValues.add(EFormValueHelper.createEFormValue("T_CBC", "CBC"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		DiagnosticOrderConverter.toFhirObject(valueMap, new QuestionnaireResponse(), null);

	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_noLabTest() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1113"));
		eformValues.add(EFormValueHelper.createEFormValue(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID,
				"DAV77092A"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
		eformValues.add(EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, "/encounterURL"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		DiagnosticOrderConverter.toFhirObject(valueMap, new QuestionnaireResponse(), null);

	}

}