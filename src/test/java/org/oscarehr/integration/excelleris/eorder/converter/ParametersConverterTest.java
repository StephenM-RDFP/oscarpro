package org.oscarehr.integration.excelleris.eorder.converter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.model.Parameters;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.util.MiscUtils;

public class ParametersConverterTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	@Test
	public void testToFhirObject() {	
		
		List<EFormValue> eformValues = new ArrayList<>();
		
		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_LAST_NAME);
		eformValue.setVarValue("Doe");
		// gender
//		eformValue = new EFormValue();
//		eformValues.add(eformValue);
//		eformValue.setVarName(PatientPropertySet.PATIENT_GENDER_FEMALE);
//		eformValue.setVarValue("Female");
		// dob
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_DOB);
		eformValue.setVarValue("1985-01-01");
		// hin
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PAITENT_HEALTH_INSURANCE_NUMBER);
		eformValue.setVarValue("1234");
		
		
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("1526201");

		// Tests on the form
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_free_psa");
		//eformValue.setVarValue("T_hepatitis_a");
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_psa_uninsured");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_gc");
		//eformValue.setVarValue("T_hepatitis_a");
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_gc_src");
		eformValue.setVarValue("CERVICAL");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_chlamydia");
		//eformValue.setVarValue("T_hepatitis_a");
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_chlamydia_src");
		eformValue.setVarValue("URINE");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_therapeutic_drug_monitor");
		//eformValue.setVarValue("T_hepatitis_a");
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_therapeutic_drug_mon_src_1");
		eformValue.setVarValue("Amitriptyline/Nortriptylin");

        // Other Tests	
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_other_tests_1");
		eformValue.setVarValue("1, 25 Dihydroxyvitamin D");
		
        // diagnosis/notes
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EO_Additional_Clinic_Info");
		eformValue.setVarValue("a string with spaces.");
		
        // diagnosis/notes
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EO_pregnant");
		
		// neonatal 
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_neonatal_bilirubin");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EO_child_age_days");
		eformValue.setVarValue("5");
		
		// province 
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("province");
		eformValue.setVarValue("ON");
		
		// postal 
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("postal");
		eformValue.setVarValue("M2M1A1");

		// gc
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_gc");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_gc_src");
		eformValue.setVarValue("URINE");
		
		// gc
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_wound");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_wound_src");
		eformValue.setVarValue("ARM");
		
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_LINE);
		eformValue.setVarValue("22 Main St.");

		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_CITY);
		eformValue.setVarValue("Vancouver");

		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
		eformValue.setVarValue("BC");

		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_PHONE);
		eformValue.setVarValue("1-123-123-1234");

		Parameters parameters = ParametersConverter.toFhirObject(EFormValueHelper.getValueMap(eformValues), null);
		
		// convert parameters to JSON
		JsonParser parser = new JsonParser();
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		try {
			parser.compose(writer, parameters);
			
			String requestString = new String(writer.toByteArray(), "UTF-8");
			
			LOG.info("requestString : " + requestString);
		} catch (Exception e) {
			LOG.error("Error converting Parameters to JSON.", e);
			throw new ServiceException("Error converting Parameters to JSON.");
		}
		

	}

}
