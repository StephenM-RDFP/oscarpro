package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.Questionnaire.AnswerFormat;
import org.hl7.fhir.instance.model.Questionnaire.QuestionComponent;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.OrderLabTestCodeDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCode;
import org.oscarehr.integration.excelleris.eorder.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Questionnaire;

public class QuestionnaireConverterTest {
	
	private OrderLabTestCodeDao orderLabTestCodeDao;

	@Before
	public void setUp() {
		this.orderLabTestCodeDao = new MockOrderLabTestCodeDao();
	}

	@Test(expected = ServiceException.class)
	public void testToApiObject_nullQuestionnaire() {
		QuestionnaireConverter.toApiObject(null, new HashMap<String, EFormValue>(), orderLabTestCodeDao);
	}

	@Test(expected = ServiceException.class)
	public void testToApiObject_nullValueMap() {
		QuestionnaireConverter.toApiObject(new org.hl7.fhir.instance.model.Questionnaire(), null, orderLabTestCodeDao);
	}

	@Test
	public void testToApiObject() {
		org.hl7.fhir.instance.model.Questionnaire fhirQuestionnaire = new org.hl7.fhir.instance.model.Questionnaire();

		QuestionComponent fhirQuestion = new QuestionComponent();
		fhirQuestion.setType(AnswerFormat.CHOICE);
		fhirQuestion.setLinkId(
				"GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:");
		fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");

		Coding coding1 = new Coding();
		coding1.setCode("Chronic kidney failure");
		coding1.setDisplay("Chronic kidney failure");

		Coding coding2 = new Coding();
		coding2.setCode("Iron overload");
		coding2.setDisplay("Iron overload");

		List<Coding> options = new ArrayList<Coding>();
		options.add(coding1);
		options.add(coding2);

		fhirQuestion.addOption(coding1);
		fhirQuestion.addOption(coding2);

		fhirQuestionnaire.getGroup().addQuestion(fhirQuestion);

		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("T_Iron__24h_Urine");
		eformValue.setVarValue("TR10259-0U");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Questionnaire questionnaire = QuestionnaireConverter.toApiObject(fhirQuestionnaire, valueMap,
				this.orderLabTestCodeDao);
		assertEquals(1, questionnaire.getQuestions().size());
	}


	public class MockOrderLabTestCodeDao extends OrderLabTestCodeDao {

		public MockOrderLabTestCodeDao() {
		}

		public OrderLabTestCode findByTestCode(String testCode) {
			return null;
		}
	}

}
