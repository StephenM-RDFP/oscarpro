package org.oscarehr.integration.excelleris.eorder;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AuthenticationServiceImplTest {
	
	private AuthenticationServiceImpl authenticationServiceImpl;
	
	@Before
    public void setUp() {
        this.authenticationServiceImpl = new AuthenticationServiceImpl();
    }
	
	@Test
	public void testAuthenticate() {
		
		// execute test
		AuthenticationToken token = authenticationServiceImpl.authenticate();

		// verify result
		assertNotNull("should not be null", token);	
	}
}
