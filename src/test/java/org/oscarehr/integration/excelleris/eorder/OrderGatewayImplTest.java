package org.oscarehr.integration.excelleris.eorder;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.EOrder;
import org.oscarehr.integration.excelleris.eorder.converter.PatientPropertySet;
import org.oscarehr.integration.excelleris.eorder.converter.PractitionerPropertySet;

public class OrderGatewayImplTest {
	
	private OrderGatewayImpl orderGatewayImpl;
	private OrderService orderService;
	private AuthenticationService authenticationService;
	
	@Before
    public void setUp() {
		this.orderService = new OrderServiceImpl(new MockEOrderDao());
        this.authenticationService = new AuthenticationServiceImpl();
        this.orderGatewayImpl = new OrderGatewayImpl(authenticationService, orderService);
    }
	
	@Test
	public void testCreateOrder() {
		EFormData eformData = new EFormData();
		eformData.setId(1);
		List<EFormValue> eformValues = new ArrayList<>();
		
		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_LAST_NAME);
		eformValue.setVarValue("Doe");
		// date of birth
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_DOB);
		eformValue.setVarValue("2000-01-01");
		// address
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_LINE);
		eformValue.setVarValue("2000-01-01");
		// city
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_CITY);
		eformValue.setVarValue("Surrey");
		// province
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
		eformValue.setVarValue("BC");
		// postal code
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE);
		eformValue.setVarValue("V1V 1C1");
		// phone
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_PHONE);
		eformValue.setVarValue("604-222-2222");
		
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// lifelabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("DAV77092A");

		// Tests
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_hepatitis_a");
		eformValue.setVarValue("T_hepatitis_a");
		
		AuthenticationTokenAndResource result = orderGatewayImpl.createOrder(null, eformData, eformValues);
		System.out.println (result);
		
		assertNotNull("should not be null", result);	
	}

	public class MockEOrderDao extends EOrderDao {

		public MockEOrderDao() {
		}

		public EOrder findByExtEFormDataId(Integer fdid) {
			return new EOrder();
		}
	}
}
