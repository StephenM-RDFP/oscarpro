package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.util.MiscUtils;

public class QuestionTextHelperTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	@Test
	public void testGetQuestionLinkId() {
		String linkId = QuestionTextHelper.getQuestionLinkId("Test Code", "Question");
		
		String expectedLinkId = "GUI|TestCode|Test Code|Global|1|Question";
		assertEquals(linkId, expectedLinkId);
	}

	@Test
	public void testGetSourceQuestionLinkId() {
		String qlId = QuestionTextHelper.getSourceQuestionLinkId("Test Code", "QLID");

		String expectedLinkId = "GUI|TestCode|Test Code|Global|1|Source|QLID";
		assertEquals(qlId, expectedLinkId);
	}
}
