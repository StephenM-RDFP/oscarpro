CREATE TABLE IF NOT EXISTS `hl7MeasurementAllowList` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT IGNORE INTO `hl7MeasurementAllowList` (`keyword`)
VALUES
	('negative'),
	('non reactive'),
	('non-reactive'),
	('nonreactive'),
	('positive'),
	('reactive');