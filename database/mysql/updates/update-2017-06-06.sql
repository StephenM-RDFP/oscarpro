ALTER TABLE provider ADD thirdPartyOnly BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE providerArchive ADD thirdPartyOnly BOOLEAN DEFAULT FALSE NOT NULL;