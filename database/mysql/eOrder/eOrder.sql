--
-- create table for storing eOrders 
--
/*DROP TABLE IF EXISTS `excelleris_eorder`;*/
CREATE TABLE `excelleris_eorder` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `eform_data_id` int(8) NOT NULL,
  `ext_order_id` varchar(255) DEFAULT NULL,
  `generated_pdf` mediumtext,
  `state` varchar(32) NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `idx_eorder_eform_data_id` (`eform_data_id`),
  KEY `idx_eorder_ext_order_id` (`ext_order_id`),
  KEY `idx_eorder_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Alter provider table structure to include Excelleris/LifeLab identities to support eOrder
--
ALTER TABLE `provider`
	ADD COLUMN `excelleris_id` VARCHAR(40)  AFTER `practitionerNoType`,
	ADD COLUMN `lifelabs_id`   VARCHAR(40)  AFTER `excelleris_id`;

	
--
-- update existing data in provider table to set the Excelleris/LifeLabs ids. These are
-- required for eOrder integration to work. May be these should be in a separate file tbd
--
	
--UPDATE `provider` set `excelleris_id` = WHERE

--UPDATE `provider` set `lifelabs_id` = WHERE



	
	
--
-- create table for storing 'copy-to' providers	
--
CREATE TABLE `excelleris_copy_to_providers` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `ontario_lifelabs_id` varchar(40),
  `rover_id` varchar(40),
  `on_provincial_govt_id` varchar(40),
  `pathnet_code` varchar(20),
  `salutation` varchar(5),
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `middle_name` varchar(25),
  `address1` varchar(100),
  `address2` varchar(100),
  `city` varchar(50),
  `state_or_province` varchar(20),
  `zip_or_postal` varchar(20),
  `country` varchar(30),
  `region` varchar(5),
  `specialty` varchar(4),
  `deleted` boolean,
  `visible` boolean,

  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `idx_eorder_cc_providers_lifelabs_id` (`ontario_lifelabs_id`),
  KEY `idx_eorder_cc_providers_last_name` (`last_name`),
  KEY `idx_eorder_cc_providers_first_name` (`first_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;